﻿pageEnums = {
    UserAccounts: 1,
    HWData: 2,
    InactiveUser: 3,
    Password: 4,
    Browser: 5,
    AuditLog: 6,
    Image: 7,
    Gtin: 8,
    ConfigurationSetting: 9,
    Country: 10,
    Software: 11,
    Message: 12,
    EventMessage: 13,
    BlockIp: 14,
    Contact:15
};
customGrid = {
    pageEnums: pageEnums,
    [pageEnums.UserAccounts]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.HWData]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.InactiveUser]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Password]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Browser]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.AuditLog]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Image]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Gtin]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.ConfigurationSetting]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Country]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Software]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.Message]: {
        defaultOptions: null,
        savedOptions: null
    },
    [pageEnums.EventMessage]: {
        defaultOptions: null,
        savedOptions: null
    },
     [pageEnums.BlockIp]: {
         defaultOptions: null,
         savedOptions: null
    },
    [pageEnums.Contact]: {
        defaultOptions: null,
        savedOptions: null
    },
    currentPage: pageEnums.UserAccounts,
    loadGrid: loadGrid,
    setGridOptions: setGridOptions
};

$(document).ready(function (e) {
    /* Grid Tooltip Fuctions */
})

/*Local storage for grid option settings*/

/* Get startdate/enddate values */
function getDate() {
    return {
        startDate: $("#startDate").val(),
        endDate: $("#endDate").val()
    };
}

function test(e) {
    return {
       
    };
}

/*
    Get note text value from edit popup for seding to kendo grid update method.
    'textParam' key used to pass value.
    Return - textParam
*/
function getData(e) {
    return {
        textParam: $("#note").val()
    };
}

/*
    Fetch active grid id from document.
    Return - id
*/
function getGrid() {
    var id = $("div[aria-expanded='true']").attr('id');
    if (id == undefined) {
        var id = $("div[data-role='grid']").attr('id');
    }
    else {
        var id = $('#' + id).children().first().attr('id');
    }
    return id;
}

/*
    Save user grid options on click event.
*/
$(document).on("click", "#save", function (e) {
    e.preventDefault();
    display_error('Saving', true);
    setGridOptions(true);
});

/*
    Load user grid options on click event.
*/
$(document).on("click", "#rOption", function (e) {
    display_error('Resetting', false);
    loadGrid(true);
    clearSavedOptions();
});

/*
    Set grid options on initial load.
*/
function grid_initialState(e) {
    console.log(e)
    setGridOptions();
    setGridComponents();
}


/*
    Setting grid components.
*/
function setGridComponents() {
    switch (customGrid.currentPage) {
        case customGrid.pageEnums.UserAccounts:
            userPage.setComponents();
            break;
    }
}

/*
    Setting grid options.
*/

function setGridOptions(savedOptions) {
    var gid = getGrid();

    var grid = $("#" + gid).data("kendoGrid");

    if (grid._data.length > 0) {
        $("#save").show();
        $("#rOption").show();
    }
    else {
        $("#save").hide();
        $("#rOption").hide();
    }
    if (grid._data) {
        utility.disableSaveReset(grid._data.length <= 0);
    }
    if (savedOptions) {
        customGrid[customGrid.currentPage].savedOptions = kendo.stringify(grid.getOptions());
    }
    else {
        if (!customGrid[customGrid.currentPage].defaultOptions) {
            customGrid[customGrid.currentPage].defaultOptions = kendo.stringify(grid.getOptions());
        }
    }
}


function loadGrid(reset) {
    var gridOptions = "";
    if (reset) {
        gridOptions = JSON.parse(customGrid[customGrid.currentPage].defaultOptions);
    }
    else {
        gridOptions = JSON.parse(customGrid[customGrid.currentPage].savedOptions);
    }
    if (gridOptions) {
        var gid = getGrid();
        var grid = $("#" + gid).data("kendoGrid");
        //preserve the toolbar option for auditlog
        if (customGrid.currentPage == customGrid.pageEnums.AuditLog) {
            gridOptions.toolbar = [
                        {
                                template: '<button class="k-button k-button-icontext k-grid-excel" type="button"><span class="k-icon k-i-excel"></span>Export to Excel</button>'
                        }
            ];
        }
        grid.setOptions(gridOptions);
        if (reset) {
            grid.dataSource.filter({});
            grid.dataSource.sort({});
        }
    }
}

function clearSavedOptions() {
    customGrid[customGrid.currentPage].savedOptions = null;
}

//Delete operation in Kendo grid
function destroy(data) {
    var s = _modelData[0].DisplayName;
    $.ajax({
        url: "/" + gridName + "/" + model + "Delete",
        type: "POST",
        dataType: 'json',
        data: { data: mainData },
        success: function (response) {
            display_error(response.message, response.status)
            $('#' + model + "Grid").data('kendoGrid').dataSource.read();
        },
        error: function (error) {
            display_error("Error", false)
        }
    });

}
var gridName = null;
var model = null;
var _modelData = [];
var mainData = {};
function afterDelete(e) {
    gridName = e.sender.element[0].id.replace("Grid", "");
    model = gridName;
    _modelData = [];
    _modelData.push(e.model);
    $('#' + model + "Grid").data('kendoGrid').dataSource.read();
    if (gridName == gridModel.Country)
        gridName = "User";
    if (gridName == gridModel.ConfigurationSetting)
        gridName = "Settings";
    if (gridName == gridModel.BrowserDetails || gridName == gridModel.BlockIpAddress)
        gridName = "Software"
    if (gridName == gridModel.EventMessage) {
        modelId = e.model.Id;
        gridName = "Message";
    }
    switch (model) {
        case gridModel.Message:
            mainData = {
                Description: e.model["Description"], LanguageId: e.model["LanguageId"], MessageId: e.model["MessageId"],
                MessageTitle: e.model["MessageTitle"], id: e.model["id"]
            }
            break;
        case gridModel.BrowserDetails:
            mainData = {
                Id: e.model["Id"], SearchPattern: e.model["SearchPattern"], BrowserName: e.model["BrowserName"],
            }
            break;
        case gridModel.EventMessage:
            mainData = {
                Id: e.model["Id"], StartDate: e.model["StartDate"], EndDate: e.model["EndDate"],
                EventStart: e.model["EventStart"], EventEnd: e.model["EventEnd"], TimeZone: e.model["TimeZone"],
                MessageId: e.model["MessageId"]
            }
            break;
        case gridModel.ConfigurationSetting:
            mainData = {
                Id: e.model["Id"], DisplayName: e.model["DisplayName"], OS: e.model["OS"],
                OS_Top_Version: e.model["OS_Top_Version"], OS_32_64Bit: e.model["OS_32_64Bit"],
                Browser: e.model["Browser"], Browser_Bottom_Version: e.model["Browser_Bottom_Version"], Browser_Top_Version: e.model["Browser_Top_Version"], Browser_Bottom_Version: e.model["Browser_Bottom_Version"],
                Browser_32_64Bit: e.model["Browser_32_64Bit"]
            };
            break;
        case gridModel.BlockIpAddress:
            mainData = {
                Id: e.model["Id"], IPaddress: e.model["IPaddress"]
            }
            break;
        default:
            break;
    }

    $.dialog({
        "body": "Delete this data permanently ?",
        "title": "Are you sure ?",
        "show": true,
        "modal": true,
        "height": 50,
        "closeOnMaskClick": false,
        "allowScrolling": true,
        "footer": "<button name='saveButton' data-dialog-action=\"hide\" onClick='destroy(\"" + e.model.id + "\")'>Ok</button> <button name='cancelButton' data-dialog-action=\"hide\">Cancel</button>"
    });
}
gridModel = {
    Country: "Country",
    ConfigurationSetting: "ConfigurationSetting",
    BrowserDetails: "BrowserDetails",
    BlockIpAddress: "BlockIpAddress",
    EventMessage: "EventMessage",
    Message:"Message"
}
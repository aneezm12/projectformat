/*
    Main script that handles common functionalities.
*/

/* Preloader delay timer */
function preloader() {
    if ($('.preloader').length) {
        $('.preloader').delay(2000).fadeOut(500);
    }
}

/* Set height for main content area */
function right_col_height() {
    if ($(window).height() < 830) {
        $(".right_col").css("min-height", 890);
    }
    else {
        $(".right_col").css("min-height", $(window).height() - 59);
    }
}

/* Sidebar Menu handle click events */
function init_sidebar() {
    right_col_height();
    $("#sidebar-menu").find("a").on("click", function (b) {
        var c = $(this).parent();
        c.is(".active") ? c.removeClass("active active-sm") : (c.parent().is(".child_menu") ? $("body").is(".nav-sm") && $("#sidebar-menu").find("li").removeClass("active active-sm") : $("#sidebar-menu").find("li").removeClass("active active-sm"), c.addClass("active"))
    }),
     $("#menu_toggle").on("click", function () {
         $("body").hasClass("nav-md") ? ($("#sidebar-menu").find("li.active ul").hide(), $("#sidebar-menu").find("li.active").addClass("active-sm").removeClass("active")) : ($("#sidebar-menu").find("li.active-sm ul").show(), $("#sidebar-menu").find("li.active-sm").addClass("active").removeClass("active-sm")), $("body").toggleClass("nav-md nav-sm")
     })
}

/* Update connt area on window resize */
$(window).resize(function () {
    right_col_height();
});

/* Load Sidebar */
$(document).ready(function () {
    init_sidebar();
});

/* Call preloader image on load */
$(window).on('load', function () {
        preloader();
});


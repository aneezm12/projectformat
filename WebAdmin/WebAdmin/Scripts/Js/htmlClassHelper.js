﻿function setReadOnly(inputId) {
    var session = $("#SessionUserRole").data('value');
    if (session == "Marketing") 
        $(inputId).attr("disabled", true);
    else 
        $(inputId).attr("disabled", false);
}

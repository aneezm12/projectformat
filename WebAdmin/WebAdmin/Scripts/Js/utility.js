﻿//file to keep all the utility methods
utility = {
    clearValidationMessage: clearValidationMessage,
    clearAllControls: clearAllControls,
    disableSaveReset: disableSaveReset
}
function clearValidationMessage(allElement, element) {
    if (allElement) {
        $(".highlight").removeClass("highlight");
        $(".field-validation-valid.text-danger").text('');
        $(".field-validation-valid").text(''); //added this to clear the validation mssg after Form submissn
    }
    else if (element) {
        element.classList.remove('highlight');
        var validationElement = $("[data-valmsg-for='" + element.name + "']");
        validationElement.text('');

    }
}
function setReadOnly() {
    var session = $("#SessionUserRole").data('value');
    var args = Array.prototype.slice.call(arguments);
    args.forEach(function (inputId) {
        if (session == "Marketing") {
            $(inputId).attr("disabled", true)
        }
        else {
            $(inputId).attr("disabled", false)
        }
    });
}
function clearAllControls() {
    //clear all textboxes and radio buttons
    $.each($('input'), function (index, input) {
        if ((input.type != 'button') && (input.type != 'hidden')) {
            if (input.type == 'radio') {
                input.checked = false;
            }
            else {
                input.value = '';
            }
        }
    });

    //clear all drop downs
    $.each($('select'), function (index, select) {
        select.selectedIndex = 0;
    });

    //clear iframes - this is the text area which is formed from kendo control
    var iframeElement = $('iframe');
    if (iframeElement) {
        iframeElement.contents().find('body').html('');
    }

    //clear all kendo date pickers
    $.each($('.k-datepicker input'), function (index, datePicker) {
        var datePickerId = datePicker.id;
        $('#' + datePickerId).data("kendoDatePicker").min(new Date());
        $('#' + datePickerId).data("kendoDatePicker").max(new Date(2099, 11, 31));
    });

    //clear all kendo date time pickers
    $.each($('.k-datetimepicker input'), function (index, dateTimePicker) {
        var dateTimePickerId = dateTimePicker.id;
        $('#' + dateTimePickerId).data("kendoDateTimePicker").min(new Date());
        $('#' + dateTimePickerId).data("kendoDateTimePicker").max(new Date(2099, 11, 31));
    });
}

//disable save,reset options
function disableSaveReset(disable) {
    if (disable) {
        $('#custonGridOptions').addClass("disabledContent");
    }
    else {
        $('#custonGridOptions').removeClass("disabledContent");
    }
}

function display_error(message, errorstatus) {

    //Toaster
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    if (errorstatus && message != '' && message != null) {
        toastr.success(message)
    } else if (!errorstatus && message != '' && message != null) {
        toastr.error(message)

    }

}
﻿//Event Message Object
eventMessage = {
    tabs: {
        messageList:1,
        addMessage:2
    },
    settingsChangeType: {
        startDate: 1,
        endDate: 2,
        startDateTime: 3,
        endDateTime: 4
    },
    eventStartDate: null,
    eventStartDateTime: null,
    eventEndDate: null,
    eventEndDateTime: null,
    eventStartControl: $('#EventMessage_EventStart'),
    eventEndControl: $('#EventMessage_EventEnd'),
    eventMessageInputsUpdated: false,
    isEditor: false,
    eventSettingsChanged: eventSettingsChanged
}

//ready hook
$(document).ready(function () {
   
    document.cookie = "TimeZoneClient=" + Intl.DateTimeFormat().resolvedOptions().timeZone;
    eventMessage.eventStartControl.attr('disabled', true);
    eventMessage.eventEndControl.attr('disabled', true);
    $("#eventmessage_form").kendoValidator().data("kendoValidator");
});

//setting up the event message controls to variables for reducing redundancy
function setEventMessageInputs() {
    if (eventMessage.isEditor) {
        eventMessage.eventStartDate = $("#StartDate").data("kendoDatePicker");
        eventMessage.eventStartDateTime = $("#EventStart").data("kendoDateTimePicker");
        eventMessage.eventEndDate = $("#EndDate").data("kendoDatePicker");
        eventMessage.eventEndDateTime = $("#EventEnd").data("kendoDateTimePicker");
    }
    else {
        eventMessage.eventStartDate = $("#EventMessage_StartDate").data("kendoDatePicker");
        eventMessage.eventStartDateTime = $("#EventMessage_EventStart").data("kendoDateTimePicker");
        eventMessage.eventEndDate = $("#EventMessage_EndDate").data("kendoDatePicker");
        eventMessage.eventEndDateTime = $("#EventMessage_EventEnd").data("kendoDateTimePicker");
    } 
}

//this will be fired when some date change happens
function eventSettingsChanged(changedType) {
    var startDate, endDate, startDateTime, endDateTime;

    //set the date inputs if its not set
    if (!eventMessage.eventMessageInputsUpdated) {
        setEventMessageInputs();
        eventMessage.eventMessageInputsUpdated = true;
    }

    //get the values
    startDate = eventMessage.eventStartDate.value();
    endDate = eventMessage.eventEndDate.value();
    startDateTime = eventMessage.eventStartDateTime.value();
    endDateTime = eventMessage.eventEndDateTime.value();
    //set date/date time according to the changed type
    switch (changedType) {
        case eventMessage.settingsChangeType.startDate:
            if (eventMessage.isEditor) {
                eventMessage.eventEndDateTime.min(startDate);
            }
            else {
                eventMessage.eventStartDateTime.enable(true);
            }
            //case when the user sets greater start date than end date
            if (endDate && (endDate.getTime() < startDate.getTime())) {
                eventMessage.eventEndDate.value(null);
                eventMessage.eventEndDateTime.value(null);
                eventMessage.eventStartDateTime.value(null);
                eventMessage.eventStartDateTime.max(new Date(2099, 11, 31));
            }
            eventMessage.eventEndDate.min(startDate);
            eventMessage.eventStartDateTime.min(startDate);
            break;
        case eventMessage.settingsChangeType.endDate:
            var adjustedEndDateHours = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 23, 30);
            if (!eventMessage.isEditor) {
                eventMessage.eventEndDateTime.enable(true);
            }
            eventMessage.eventStartDateTime.max(adjustedEndDateHours);
            eventMessage.eventEndDateTime.max(adjustedEndDateHours);
            break;
        case eventMessage.settingsChangeType.startDateTime:
            if (startDateTime && endDateTime) {
                adjustEndDateTime(startDateTime, endDateTime, null, false);
            }
            else {
                eventMessage.eventEndDateTime.min(startDateTime);
            }
            break;
        case eventMessage.settingsChangeType.endDateTime:
            if (startDateTime && endDateTime) {
                adjustEndDateTime(startDateTime, endDateTime);
            }
            break;
    }
}

//adjust the Event End Date Time if the Event Start Date, Event End Date are same
function adjustEndDateTime(startDateTime, endDateTime, skipEndValueSetting, clearEndDateTime) {
    var adjustedEndDatetime;
    if (startDateTime.toLocaleDateString() == endDateTime.toLocaleDateString()) {
        adjustedEndDatetime = new Date(startDateTime.getFullYear(), startDateTime.getMonth(), startDateTime.getDate(), startDateTime.getHours(), (startDateTime.getMinutes() + 30));
        eventMessage.eventEndDateTime.min(adjustedEndDatetime);
        if (clearEndDateTime) {
            eventMessage.eventEndDateTime.value(null);
            eventMessage.eventEndDateTime.min(new Date());
            eventMessage.eventEndDateTime.max(new Date(2099, 11, 31));
        }
        else if ((!skipEndValueSetting) && (endDateTime.getTime() <= adjustedEndDatetime.getTime())) {
            eventMessage.eventEndDateTime.value(adjustedEndDatetime);
        }
    }
    else {
        if (clearEndDateTime || (startDateTime.getTime() > endDateTime.getTime())) {
            eventMessage.eventEndDateTime.value(null);
        }
        eventMessage.eventEndDateTime.min(startDateTime);
    }
}

//date or datetime change events
//method to capture start date change
function startChange() {
    eventSettingsChanged(eventMessage.settingsChangeType.startDate);
}
//method to capture end date change
function endChange() {
    eventSettingsChanged(eventMessage.settingsChangeType.endDate);
}
//method to capture start datetime change
function eventStartChange() {
    eventSettingsChanged(eventMessage.settingsChangeType.startDateTime);
}
//method to capture end date time change
function eventEndChange() {
    eventSettingsChanged(eventMessage.settingsChangeType.endDateTime);
}

//set calendar
function updateCalender() {
    //set the date inputs if its not set
    if (!eventMessage.eventMessageInputsUpdated) {
        setEventMessageInputs();
        eventMessage.eventMessageInputsUpdated = true;
    }
    utility.clearAllControls();
    eventMessage.eventEndDateTime.enable(false);
    eventMessage.eventStartDateTime.enable(false);
    eventMessage.eventStartDate.min(new Date()); // Setting defaults
    eventMessage.eventStartDate.max(new Date(2099, 11, 31));
    eventMessage.eventStartDateTime.min(new Date());
    eventMessage.eventStartDateTime.max(new Date(2099, 11, 31));
    eventMessage.eventEndDate.min(new Date()); // Setting defaults
    eventMessage.eventEndDate.max(new Date(2099, 11, 31));
    eventMessage.eventEndDateTime.min(new Date());
    eventMessage.eventEndDateTime.max(new Date(2099, 11, 31));

    var combo = $("#EventMessage_MessageId").data("kendoComboBox");
    combo.dataSource.read();
    combo.value("");
}

//edit date/datetime
function onEdit(e) {
    eventMessage.isEditor = true;
    eventMessage.eventMessageInputsUpdated = false;
    //set the date inputs if its not set
    if (!eventMessage.eventMessageInputsUpdated) {
        setEventMessageInputs();
        eventMessage.eventMessageInputsUpdated = true;
    }
    eventMessage.eventStartDate.min(new Date());
    eventMessage.eventEndDate.min(new Date());
    eventMessage.eventStartDateTime.min(eventMessage.eventStartDate.value());
    eventMessage.eventStartDateTime.max(eventMessage.eventEndDate.value());
    adjustEndDateTime(eventMessage.eventStartDateTime.value(), eventMessage.eventEndDateTime.value(), true);
    adjustedEndDatetime = new Date(eventMessage.eventEndDate.value().getFullYear(),
        eventMessage.eventEndDate.value().getMonth(), eventMessage.eventEndDate.value().getDate(), 23, 30);
    eventMessage.eventEndDateTime.max(adjustedEndDatetime);
}

//save event message
function saveMessage(url) {
    serviceWrap.submitForm(url, function () {
        var grid = $("#EventMessageGrid").data("kendoGrid");
        grid.dataSource.read();
        updateCalender();
    });
}

//tab changed to Add Event Message
function tabChanged(activeTab) {
    switch (activeTab) {
        case eventMessage.tabs.addMessage:
            eventMessage.isEditor = false;
            eventMessage.eventMessageInputsUpdated = false;
            break;
    }
}

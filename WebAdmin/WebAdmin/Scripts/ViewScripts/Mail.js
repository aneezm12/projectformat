﻿//Get description from message template on dropdown event change
$(window).load(function () {
    // executes when complete page is fully loaded, including all frames, objects and images
    $($('#msg_content').data().kendoEditor.body).attr('contenteditable', false);
    $('.k-editor-toolbar').hide();
    document.getElementById("txtMailSubject").value = "";

});
var headingTitle;
var messageContent;
var MailSubject;
function onSelect(e) {
    const heading = e.dataItem.Text;
    headingTitle = heading;
    debugger;
    window.$.ajax({
        url: "/Mail/GetSubject",
        type: 'POST',
        data: { heading: heading },
        dataType: "json",
        success: function (data) {
            //$("#txtMailSubject").data("kendoEditor").value(data);
            document.getElementById('txtMailSubject').value = data;

            MailSubject = data;
        }
    });
    window.$.ajax({
        url: "/Mail/GetDescription",
        type: 'POST',
        data: { heading: heading },
        dataType: "json",
        success: function (data) {
            $("#msg_content").data("kendoEditor").value(data);
            msgcontent = data;
        }
    });

}
//send messages to selected recievers
//global variables
var mailDetailsList = [];
var selectedMailsDetails = [];//selected Users
//if (document.getElementById("msg_content") != null)
//    var messageContent = document.getElementById('msg_content').value;
debugger;
function sendMessagesBySelected() {
    page_loader(true);
    var dateType = /(\d{4})([\/-])(\d{1,2})\2(\d{1,2})/;
    var isMatch = dateType.test(messageContent);
    debugger;

    if (!isMatch) {
        messageContent = messageDescriptionReplace(messageContent);
    }
    if (messageContent.toLowerCase().indexOf("&nbsp;") != -1) {
        messageContent = messageContent.replace("&nbsp;", '');
    }

    window.$.ajax({
        url: "/Mail/SendMail",
        type: "POST",
        dataType: 'json',
        data: { messageContent: messageContent, mailList: selectedMailsDetails, heading: headingTitle },

        // ReSharper disable once CyclomaticComplexity
        success: function (response) {
            window.$("#msg_content").data("kendoEditor").value(null);
            const combo = window.$("#message").data("kendoComboBox");
            combo.value("");

            if (response.mail === 0) {
                $('#dialog_mail').data("kendoWindow").close();
                display_error("Mail Sent Successfully !", response.status);
            }
            else {
                $('#dialog_mail').data("kendoWindow").close();
                display_error("Mail Sent Failed !", response.status);
            }
            page_loader(false);
        },
        error: function (error) {
            $('#dialog_mail').data("kendoWindow").close();
            display_error("Mail Sent Failed !", response.status);
            page_loader(false);
        }
    });

    document.getElementById("txtMailSubject").value = "";

}
//replace description 
function messageDescriptionReplace(messageContent) {
    if (messageContent.toLowerCase().indexOf("<startdate>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("<startdate>", '');
    } if (messageContent.toLowerCase().indexOf("</startdate>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("</startdate>", '');

    }
    if (messageContent.toLowerCase().indexOf("<starttime>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("<starttime>", '');
    }
    if (messageContent.toLowerCase().indexOf("</starttime>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("</starttime>", '');

    }
    if (messageContent.toLowerCase().indexOf("<enddate>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("<enddate>", '');
    }
    if (messageContent.toLowerCase().indexOf("</enddate>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("</enddate>", '');
    }
    if (messageContent.toLowerCase().indexOf("<endtime>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("<endtime>", '');
    }
    if (messageContent.toLowerCase().indexOf("</endtime>".toLowerCase()) !== -1) {
        messageContent = messageContent.replace("</endtime>", '');
    }
    return messageContent;
}
//handling receivers list changing
function handleChange(checkbox) {
    var nameandEmail = checkbox.value.split(',')
    var index = mailDetailsList.findIndex(std => std.Username === nameandEmail[0] && std.EmailAddress === nameandEmail[1])
    if (checkbox.checked == true) {
        mailDetailsList[index].IsChecked = true;
    } else {
        mailDetailsList[index].IsChecked = false;
    }
    selectedMailsDetails = [];
    selectedMailsDetails = mailDetailsList.filter(x => x.IsChecked != false);
}
//method for sending message
function sendMessage() {
    debugger;
    if (document.getElementById("msg_content") != null)
        messageContent = document.getElementById('msg_content').value;

    if (messageContent !== "") {
        const grid = window.$("#UserGrid").data("kendoGrid")._data;
        const length = window.$("#UserGrid").data("kendoGrid")._data.length;

        const mailList = [];
        for (let i = 0; i < length; i++) {
            const userDetailsViewModel = {
                "Username": grid[i].Username,
                "EmailAddress": grid[i].EmailAddress,
                "IsChecked": true,
                "Id": grid[i].Id
            };
            mailList.push(userDetailsViewModel);
        }
        $('#dialog_mail').data("kendoWindow").close();
        var userDetails = [];
        mailList.forEach(function (element) {
            userDetails.push(
                `<li style="list-style-type:none;"></b><input type="checkbox" onchange=handleChange(this); checked value=${element.Username},${element.EmailAddress}>${element.Username
                }&nbsp[Email :&nbsp${element.EmailAddress}]<br>`);
        });
        mailDetailsList = mailList;
        selectedMailsDetails = mailList;
        if (mailDetailsList.length > 0) {
            $.dialog({
                "body": userDetails,
                "title": "Do you want to sent mail to these following persons ?",
                "show": true,
                "modal": true,
                "closeOnMaskClick": false,
                "allowScrolling": true,
                "footer": "<button class='saveButton' data-dialog-action=\"hide\" onClick='sendMessagesBySelected()'>Sent</button> <button class='cancelButton' data-dialog-action=\"hide\">Close</button>"
            });
        }
    }
    else {
        document.getElementById("msg_content").focus();
        $('#err_span').text('Required *');
        event.preventDefault();
        return false;
    }
    var combo = $("#message").data("kendoComboBox");
    combo.value("");
    $('#msg_content').data("kendoEditor").value(null);

}
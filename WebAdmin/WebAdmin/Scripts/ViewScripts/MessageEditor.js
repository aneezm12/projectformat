﻿//on language change
function OnChange(e) {
    var title = $("#MessageTitle").val();
    var data = this.dataItem(e.item);
    var language = data.Value;
    $("#dubpicatemssg").text(" ");
    $.ajax({
        url: "/Message/CheckExistence",
        type: 'POST',
       data: { language: language, title: title },
        dataType: "json",
        success: function (data) {
            $("#dubpicatemssg").text(" ");
            $(".k-grid-update").attr("disabled", false);
            if (data!= 0) {
                $("#dubpicatemssg").text("Message Already Exists in this Language");
                $(".k-grid-update").attr("disabled", true);
            }
        }
    });
}
//set tages in the description editor
function setTagsInEditor(tag) {
    var editor = $("#Description").data("kendoEditor");
    var content = editor.value();
    switch (tag) {
        case "Startdate":
            if (!content.includes("&lt;Startdate&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Startdate" + "&gt;" });
            }
            break;
        case "Enddate":
            if (!content.includes("&lt;Enddate&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Enddate" + "&gt;" });
            }
            break;
        case "Starttime":
            if (!content.includes("&lt;Starttime&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Starttime" + "&gt;" });
            }
            break;
        case "Endtime":
            if (!content.includes("&lt;Endtime&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Endtime" + "&gt;" });
            }
            break;
        default: break;

    }
}


﻿

//method to load grid
function loadGridData(data) {
    $('#content-area').html(data);
    customGrid.loadGrid();
    page_loader(false);
}

//method to show grid options
function showGridOptions(show) {
    if (show) {
        $('#custonGridOptions').show();
    }
    else {
        $('#custonGridOptions').hide();
    }
}

//show/hide mail option
function showMailOption(show) {
    var userRole = $("#SessionUserRole").data('value');
    if (show && userRole != "Marketing") {
        $('#showDialogBtn').show();
    }
    else {
        $('#showDialogBtn').hide();
    }
}

//hide mail window
function hideMailWindow() {
    $('#dialog_mail').data("kendoWindow").close();
}

//initiate mail editor
function initiateEditorMail() {
    $("#err_span").html('');
    $("#msg_content").data("kendoEditor").value(null);
    var combo = $("#message").data("kendoComboBox");
    combo.value("");
    var cmbMessageInMail = $("#message").data("kendoComboBox")
    if (cmbMessageInMail != null) {
        cmbMessageInMail.dataSource.read();
    }
}

// navigate sitemap menu
function navigate(url, key) {
    debugger
    var customGridOption = undefined;
    var isShowMailOption = false;
    switch (key) {
        case "userdata":
            customGridOption = customGrid.pageEnums.UserAccounts;
            isShowMailOption = true;
            break;
        case "hardwaredata":
            customGridOption = customGrid.pageEnums.HWData;
            break;
        case "inactive":
            customGridOption = customGrid.pageEnums.InactiveUser;
            break;
        case "reset":
            customGridOption = customGrid.pageEnums.Password;
            break;
        case "browser":
            customGridOption = customGrid.pageEnums.Browser;
            break;
        case "auditlog":
            customGridOption = customGrid.pageEnums.AuditLog;
            break;
        case "gtin":
            customGridOption = customGrid.pageEnums.Gtin;
            break;
        case "imgresolution":
            customGridOption = customGrid.pageEnums.Image;
            break;
        case "sversions":
            customGridOption = customGrid.pageEnums.ConfigurationSetting;
            break;
        case "country":
            customGridOption = customGrid.pageEnums.Country;
            break;
        case "software":
            customGridOption = customGrid.pageEnums.Software;
            break;
        case "Messages":
            customGridOption = customGrid.pageEnums.Message;
            break;
        case "EventMessages":
            customGridOption = customGrid.pageEnums.EventMessage;
            break;
        case "blockIP":
            customGridOption = customGrid.pageEnums.BlockIp;
            break;
        case "contact":
            customGridOption = customGrid.pageEnums.Contact;
            break;
        default:
            break;
    }
    hideMailWindow();
    showGridOptions(true);
    customGrid.currentPage = customGridOption;
    showMailOption(isShowMailOption);
    page_loader(true);
    serviceWrap.getPage(url,
        function (data) { loadGridData(data); },
        function () { page_loader(false); }
    );
}
﻿//date or datetime change events
//method to capture start date change
function startEditorChange() {
    eventMessage.eventSettingsChanged(eventMessage.settingsChangeType.startDate);
}
//method to capture end date change
function endEditorChange() {
    eventMessage.eventSettingsChanged(eventMessage.settingsChangeType.endDate);
}
//method to capture start datetime change
function eventEditorStartChange(e) {
    eventMessage.eventSettingsChanged(eventMessage.settingsChangeType.startDateTime);
    
}
//method to capture end date time change
function eventEditorEndChange() {
    eventMessage.eventSettingsChanged(eventMessage.settingsChangeType.endDateTime);
}

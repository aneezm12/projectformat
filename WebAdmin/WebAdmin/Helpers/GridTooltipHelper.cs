﻿using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public static class GridTooltipHelper
    {
        public static TooltipBuilder GridTooltip<T>(this HtmlHelper helper, IGridViewModel<T> gmodel, string jsFunctions, string js) where T : class
        {
            return helper.Kendo().Tooltip()
              .For("#" + gmodel.ModelType.Name + "Grid")
              .Filter("." + js)
              .ShowOn(TooltipShowOnEvent.Click)
              .ContentHandler(jsFunctions)
              .Position(TooltipPosition.Center)
              .AutoHide(false);
        }
        public static string TooltipSpan(string propertyName,string spanName)
        {
            //("#if (" + property.Name + ") {#" +
            //                      GridTooltipHelper.TooltipSpan(property.Name, "forExceptionTooltips") +
            //                      "#}#");
      //      if(propertyName)
            string span = "#if (" + propertyName + ") {#" + "<span class='wordWrapGridColumn " +spanName+"'"+">#=" + propertyName + "#</span>" +
                                    "#} else {#" +
                                    "<span class=''></span>" + "#}#";
            return span;
        }
    }
}
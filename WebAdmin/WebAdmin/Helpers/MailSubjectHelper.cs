﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
   public enum MailSubject
    {
        EventMessage=0,
        DisapprovedMessage=1,
        ApprovedMessage=2
    }
    internal static class MailSubjectHelper
    {
        internal static string GetSubjectCreator(MailSubject mailSubject)
        {
            switch(mailSubject)
            {
                case MailSubject.EventMessage:
                    return "Greeting";
                case MailSubject.DisapprovedMessage:
                    return "Account Disapproved";
                case MailSubject.ApprovedMessage:
                    return "Account Approved";
                default:
                    return string.Empty;
            }
        }
    }
}
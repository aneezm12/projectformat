﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace WebAdmin
{
    public static class SwitchHelper
    {
        public static SwitchBuilder GridSwitch(this HtmlHelper helper, string switchName, string enabledName, string disabledName)
        {
            return helper.Kendo().Switch().Name(switchName).Messages(c => c.Checked(enabledName).Unchecked(disabledName));
        }
    }
}
﻿using Kendo.Mvc.UI.Fluent;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebAdmin.Models;
using System.Reflection;

namespace WebAdmin
{
    public static class ComboHelper
    {
        public static ComboBoxBuilder ComboBox(this HtmlHelper helper,string name, string controllerName, string functionName = "")
        {
            functionName =functionName == "" ? "ComboRead" : functionName;
            return helper.Kendo().ComboBox()
             .Name(name)
             .DataTextField("Text")
             .DataValueField("Value")
             .Placeholder("...Select...")
             .Filter(FilterType.Contains)
             .AutoWidth(true)
             .HtmlAttributes(new { @class = "form-control" })
             .DataSource(
                source =>
                {
                    source.Read(read =>
                    {
                        read.Action(functionName, controllerName);
                    });
                });
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.Mvc.UI.Fluent;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebAdmin.Models;
using System.Reflection;
using Ninject.Infrastructure.Language;


namespace WebAdmin
{

    public enum EditOrDelete
    {
        Edit,
        Delete,
        EditDelete,
        Notedit
    }


    public static class GridHelper
    {
        public static Kendo.Mvc.UI.Fluent.GridBuilder<T> Grid<T>(this HtmlHelper helper, IGridViewModel<T> gModel, string controllerName, EditOrDelete editOrDelete, string templateName = "") where T : class
        {
            if (gModel == null)
                return null;
            try
            {

            

                return helper.Kendo().Grid<T>()
                .Name(gModel.ModelType.Name + "Grid")
                .HtmlAttributes(new { style = "height: 550px;" })
                .Editable(editable => editable
                .DisplayDeleteConfirmation(false)
                .Mode(GridEditMode.PopUp).TemplateName(templateName)
                   )
                .Columns(columns =>
                {
                    if (editOrDelete.Equals(EditOrDelete.Delete))
                    {
                        columns.Command(command => command.Destroy().Text(" ").IconClass("fa fa-trash")).Width(100);
                        //columns.Command(command => command.Destroy().Template("<span style='float:left'></span>").Text(" ").IconClass("fa fa-trash")).Width(50);

                    }
                    else if (editOrDelete.Equals(EditOrDelete.Edit))
                    {

                        columns.Command(x => x.Edit().Template("<a role='button' class='k-button-icontext k-grid-edit k-icon k-i-pencil ob-icon-only' style='cursor:pointer'><span class='k-icon k-i-edit'></span> </a>")).Width(50);
                    }

                    else if (editOrDelete.Equals(EditOrDelete.EditDelete))
                    {
                        columns.Command(command => command.Destroy().Text(" ").IconClass("fa fa-trash")).Width(100);

                        columns.Command(x => x.Edit().Template("<a role='button' class='k-button-icontext k-grid-edit k-icon k-i-pencil ob-icon-only' style='cursor:pointer'><span class='k-icon k-i-edit'></span> </a>")).Width(50);
                    }

                    // you can also access helper.ViewBag to get extra data
                    foreach (var property in gModel.Props)
                    {

                        var displayName = property.Name; //DataHelper.TryGetDisplayName(property);

                        switch (displayName)
                        {
                            case "Status":
                                if (templateName != "User" && templateName != "Inactive")
                                {
                                    columns.Bound(property.Name).Width(200)
                                    .ClientTemplate(DataHelper.CreateStyleForSwitch(property.Name)); 
                                }
                                break;
                            case "Blocked":
                                columns.Bound(property.Name).Width(200)
                                .ClientTemplate(DataHelper.CreateStyleForBlockedSwitch(property.Name));
                                break;
                            case "LogDate":
                                columns.Bound(property.Name).Width(220).ClientTemplate("#=kendo.parseDate(" + property.Name + ",'MM/dd/yyyy')#");
                                break;
                            case "Content":
                                columns.Bound(property.Name).Width(200).ClientTemplate(GridTooltipHelper.TooltipSpan(property.Name, "forContentTooltips"));
                                break;
                            case "Description":
                                columns.Bound(property.Name).Width(300).ClientTemplate(GridTooltipHelper.TooltipSpan(property.Name, "forContentTooltips"));
                                break;
                            case "Message":
                                columns.Bound(property.Name).Width(200).ClientTemplate(GridTooltipHelper.TooltipSpan(property.Name, "forMessageTooltips"));
                                break;
                            case "Exception":
                                columns.Bound(property.Name).Width(200).ClientTemplate(GridTooltipHelper.TooltipSpan(property.Name, "forExceptionTooltips"));
                                break;
                            case "Disabled":
                                columns.Bound(property.Name).Width(200).EditorTemplateName("HardwareStatus")
                               .ClientTemplate(
                                  "#if (" + property.Name + ") {#" +
                                   "<span class='label label-danger'>Inactive</span>" +
                                   "#} else {#" +
                                   "<span class='label label-success'>Active</span>" +
                                   "#}#"
                               );
                                break;
                            case "StartDate":
                                columns.Bound(displayName).Width(220)
                                .ClientTemplate("#= kendo.parseDate(" + property.Name + ",'MM/dd/yyyy')#");
                                break;
                            case "EndDate":
                                columns.Bound(displayName).Width(220)
                                .ClientTemplate("#= kendo.parseDate(" + property.Name + ",'MM/dd/yyyy')#");
                                break;
                            case "EventStart":
                                columns.Bound(displayName).Width(220)
                                .ClientTemplate("#= kendo.parseDate(" + property.Name + ",'MM/dd/yyyy hh:mm:ss')#");
                                break;
                            case "EventEnd":
                                columns.Bound(displayName).Width(220)
                                .ClientTemplate("#= kendo.parseDate(" + property.Name + ",'MM/dd/yyyy hh:mm:ss')#");
                                break;
                           
                            default:
                                if (property.HasAttribute(typeof(HiddenInputAttribute)))
                                {
                                    columns.Bound(property.Name).Hidden(true).IncludeInMenu(false);
                                }
                                else
                                {
                                    columns.Bound(property.Name).Visible(true).Width(180);
                                }
                                break;
                        }

                       

                    }

                   

                })

                .ToolBar(tools =>
                {
                    if (gModel.ModelType.Equals(typeof(AuditLog)))
                    {
                        tools.Excel();
                    }
                }
               )
               .Excel(excel => excel
               .FileName(gModel.ModelType.Name + ".xlsx")

               .AllPages(true)

               )

                .Scrollable()
                .Selectable()
                .Groupable()
                .Filterable()
                .Events(events => events.Remove("afterDelete"))
                .Resizable(resizing => resizing.Columns(true))
                .Reorderable(reordering => reordering.Columns(true))
                .Sortable()
                .ColumnMenu()//pls add this to show the menu
                .Events(e => e.DataBound("grid_initialState"))
                
                .Pageable(pageable => pageable
                        .Refresh(true)
                        .PageSizes(true)
                        .ButtonCount(5)
                    )
                .DataSource(dataSource => dataSource
                .Ajax()
               .Batch(false)
                .Model(
                    model =>
                    {
                        foreach (PropertyInfo prop in gModel.Props)
                        {
                            var displayName = prop.Name; //DataHelper.TryGetDisplayName(prop);

                            if (displayName == "Id" || displayName == "MessageId")
                            {
                                model.Id(displayName);

                            }

                        }
                    }
                )

                
               


                .Read(read => read.Action(gModel.ModelType.Name + "Read", controllerName).Data("getDate"))


               .Update(update => update.Action(gModel.ModelType.Name + "Update", controllerName)
                                        .Data("getData"))
               .Destroy(destroy => destroy.Action(gModel.ModelType.Name, controllerName))

               
            );
            }
            catch (Exception ex)
            {
                return null;
            }



        }
    }

}


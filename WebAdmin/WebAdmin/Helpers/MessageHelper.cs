﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Globalization;
using System.Web.Mvc;
using System.Net.Mail;
using System.Web.Configuration;
using Ninject.Infrastructure.Language;
using System.Configuration;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace WebAdmin
{
    public static class MessageHelper
    {
        /// <summary>
        /// Converting Olson TimeZone format to Windows TimeZone format
        /// </summary>
        /// <param name="olsonTimeZoneId"></param>
        /// <returns></returns>
        public static TimeZoneInfo OlsonTimeZoneToTimeZoneInfo(string olsonTimeZoneId)
        {
            string contentPath = ConfigurationManager.AppSettings["TimeZoneDir"];

            var baseDir = AppDomain.CurrentDomain.BaseDirectory;

            string winTimes = System.IO.File.ReadAllText(baseDir + contentPath);


            var olsonWindowsTimes = JsonConvert.DeserializeObject<Dictionary<string, string>>(winTimes);

          
            var windowsTimeZoneId = default(string);
            var windowsTimeZone = default(TimeZoneInfo);
            if (olsonWindowsTimes.TryGetValue(olsonTimeZoneId, out windowsTimeZoneId))
            {
                try { windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneId); }
                catch (TimeZoneNotFoundException) { }
                catch (InvalidTimeZoneException) { }
            }
            return windowsTimeZone;
        }
        public static List<string> GetLanguage(IEnumerable<Language> languages, string languageCulture)
        {
            var languageList = new List<string>();
            try
            {
                ICultureCodeService cultureCodeService = new CultureCodeService();
                var defaultCode = "";
                string defaultLanguage = Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]);
                foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
                {
                    bool matchLanguage = Regex.IsMatch(defaultLanguage, Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                    if (matchLanguage)
                    {
                        MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                        defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                    }

                }
                if (string.IsNullOrEmpty(languageCulture))
                {
                    MvcApplication.Logger.Info("No culture code found");
                    return null;
                }

                List<Guid> getLanguageId = (from data in languages
                                            where data.CultureCode.Split('-').First() == defaultCode
                                                  && Convert.ToString(data.Enabled) == "True"

                                            select data.Id).ToList();
                if (getLanguageId.Any())
                {
                    foreach (Guid data in getLanguageId)
                    {
                        languageList.Add(Convert.ToString(data));
                    }
                }


            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error("MessageHelper_getLanguage : " + exception.Message, exception);
            }
            return languageList;
        }
        internal static List<SelectListItem> GetMessages(IEnumerable<Message> messages)
        {
            var messageSelectList = new List<SelectListItem>();


            foreach (Message data in messages)
            {
                                  
                        messageSelectList.Add(
                    new SelectListItem
                    {
                        Value = data.MessageId.ToString(),
                        Text = data.MessageTitle
                    });                   
               

            }
            messageSelectList=messageSelectList.GroupBy(x => x.Text).Select(x => x.First()).ToList();
            return messageSelectList;
        }
        internal static List<SelectListItem> GetMessagesList(IEnumerable<Language> languages, IEnumerable<Message> messages, string userLanguageRequest)
        {
            var messageSelectList = new List<SelectListItem>();

            try
            {
                ICultureCodeService cultureCodeService = new CultureCodeService();
                string defaultCode = "";
                string defaultLanguage = Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]);
                foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
                {
                    bool matchLanguage = Regex.IsMatch(defaultLanguage, Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                    if (matchLanguage)
                    {
                        MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                        defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                    }

                }
                List<string> userLanguage = GetLanguage(languages, defaultCode);


                foreach (Message data in messages)
                {
                    foreach (string language in userLanguage)
                    {
                        if (Convert.ToString(data.LanguageId) == language)
                        {
                            messageSelectList.Add(
                        new SelectListItem
                        {
                            Value = data.MessageId.ToString(),
                            Text = data.MessageTitle
                        });

                        }

                    }
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);

            }

            return messageSelectList;
        }      
        public static List<Guid> GetMessageIds(IEnumerable<Language> languages, IEnumerable<Message> messages, string userLanguageRequest)
        {
            List<Guid> messageIdList = new List<Guid>();

            var englishLanguageList = GetLanguage(languages, WebConfigurationManager.AppSettings["DefaultLanguage"]);//Should be "en" 

            var userLanguage = GetLanguage(languages, userLanguageRequest);

            foreach (var data in messages)
            {
                foreach (var language in englishLanguageList)
                {
                    if (Convert.ToString(data.LanguageId) == language)
                    {
                        messageIdList.Add(data.MessageId);                            
                    }
                }
            }
            return messageIdList;
        }


    }
}
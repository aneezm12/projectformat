﻿using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class MenuSettings : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var userRole = HttpContext.Current.Session["UserRole"] != null ? HttpContext.Current.Session["UserRole"] : "";
            var nodes = new List<DynamicNode>();

            DynamicNode hardwarenode = new DynamicNode();
            hardwarenode.Key = "hardwaredata";
            hardwarenode.Title = "Hardware Data";
            hardwarenode.Controller = "Hardware";
            hardwarenode.Action = "HardwareData";
            hardwarenode.Description = "fa fa-database";
            nodes.Add(hardwarenode);

            if((string)userRole != "Marketing")
            {
                DynamicNode versionnode = new DynamicNode();
                versionnode.Key = "sversions";
                versionnode.Title = "Supported Version";
                versionnode.Controller = "Settings";
                versionnode.Action = "ConfigurationSettingIndex";
                versionnode.Description = "fa fa-support";
                nodes.Add(versionnode);

                DynamicNode imgnnode = new DynamicNode();
                imgnnode.Key = "imgresolution";
                imgnnode.Title = "Image Resolutions";
                imgnnode.Controller = "Settings";
                imgnnode.Action = "Image";
                imgnnode.Description = "fa fa-image";
                nodes.Add(imgnnode);

                DynamicNode gtinnode = new DynamicNode();
                gtinnode.Key = "gtin";
                gtinnode.Title = "GTIN Number";
                gtinnode.Controller = "Settings";
                gtinnode.Action = "Gtin";
                gtinnode.Description = "fa fa-list";
                nodes.Add(gtinnode);

                DynamicNode auditlognode = new DynamicNode();
                auditlognode.Key = "auditlog";
                auditlognode.Title = "Audit Log Report";
                auditlognode.Controller = "AuditLog";
                auditlognode.Action = "Index";
                auditlognode.Description = "fa fa-file";
                nodes.Add(auditlognode);

                DynamicNode countrynode = new DynamicNode();
                countrynode.Key = "country";
                countrynode.Title = "Approved Countries";
                countrynode.Controller = "User";
                countrynode.Action = "CountryIndex";
                countrynode.Description = "fa fa-globe";
                nodes.Add(countrynode);

                DynamicNode inactivenode = new DynamicNode();
                inactivenode.Key = "inactive";
                inactivenode.Title = "Inactive Users";
                inactivenode.Controller = "User";
                inactivenode.Action = "InActive";
                inactivenode.Description = "fa fa-user";
                nodes.Add(inactivenode);
          
                DynamicNode browsernode = new DynamicNode();
                browsernode.Key = "browser";
                browsernode.Title = "Browser Details";
                browsernode.Controller = "Software";
                browsernode.Action = "BrowserIndex";
                browsernode.Description = "fa fa-chrome";
                nodes.Add(browsernode);

                DynamicNode softwarenode = new DynamicNode();
                softwarenode.Key = "software";
                softwarenode.Title = "Software Details";
                softwarenode.Controller = "Software";
                softwarenode.Action = "Index";
                softwarenode.Description = "fa fa-list";
                nodes.Add(softwarenode);

                DynamicNode Messagesnode = new DynamicNode();
                Messagesnode.Key = "Messages";
                Messagesnode.Title = "Message";
                Messagesnode.Controller = "Message";
                Messagesnode.Action = "MessageIndex";
                Messagesnode.Description = "fa fa-envelope";
                nodes.Add(Messagesnode);
           
                DynamicNode EventMessagesnode = new DynamicNode();
                EventMessagesnode.Key = "EventMessages";
                EventMessagesnode.Title = "Event Message";
                EventMessagesnode.Controller = "Message";
                EventMessagesnode.Action = "EventMessageIndex";
                EventMessagesnode.Description = "fa fa-envelope";
                nodes.Add(EventMessagesnode);

                DynamicNode blockIPnode = new DynamicNode();
                blockIPnode.Key = "blockIP";
                blockIPnode.Title = "Block IP";
                blockIPnode.Controller = "Software";
                blockIPnode.Action = "BlockIpAddressIndex";
                blockIPnode.Description = "fa fa-ban";
                nodes.Add(blockIPnode);
            }
            DynamicNode contactnode = new DynamicNode();
            contactnode.Key = "contact";
            contactnode.Title = "Contact";
            contactnode.Controller = "User";
            contactnode.Action = "ContactIndex";
            contactnode.Description = "fa fa-address-card";
            nodes.Add(contactnode);
            return nodes;
        }
    }
}
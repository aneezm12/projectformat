﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Globalization;
using System.Web.Mvc;
using System.Net.Mail;
using System.Web.Configuration;
using Ninject.Infrastructure.Language;
using System.ComponentModel;

using WebAdmin.Services;

namespace WebAdmin
{


   public enum LanguageList
    {
        [Description("English")]
        en,
        [Description("German")]
        de,
        [Description("French")]
        fr,
        [Description("Arabic")]
        ar,
        [Description("Spanish")]
        es

    } 

    public static class DataHelper
    {
        public static string TryGetDisplayName(PropertyInfo propertyinfo)
        {
            string result = null;
            try
            {
                var attrs = propertyinfo.GetCustomAttributes(typeof(DisplayAttribute), true);
                if(attrs.Any())
                {
                    result = ((attrs[0]) as DisplayAttribute)?.Name;
                }
            }
            catch(Exception ex)
            {
                result = string.Empty;
            }
            return result;
        }
        /// <summary>
        /// To compare new value with already existing value to note the changes in Auditlog
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns></returns>
        public static List<Variance> Compare<T>(this T val1, T val2)
        {
            var variances = new List<Variance>();
            var Props = typeof(T).GetProperties();

            string[] hiddenFields = {
                //For User table
                    "CreationDate",
                    "LastLoginDate"                    
            };
            foreach (var property in Props)
            {
                if (!property.HasAttribute(typeof(HiddenInputAttribute)) && !hiddenFields.Contains(property.Name))
                {
                   
                    var v = new Variance
                    {
                        PropertyName = property.Name,
                        Old = property.GetValue(val1),
                        New = property.GetValue(val2)
                    };
                    if (property.Name == "Blocked")
                    {
                       v.Old = (bool)property.GetValue(val1) ? "Active" : "Inactive" ;
                       v.New = (bool)property.GetValue(val2) ? "Active" : "Inactive";
                    }
                    if (v.Old == null && v.New == null)
                    {
                        continue;
                    }
                    if (
                        (v.Old == null && v.New != null)
                        ||
                        (v.Old != null && v.New == null)
                    )
                    {
                        variances.Add(v);
                        continue;
                    }
                    if (!v.Old.Equals(v.New))
                    {
                        variances.Add(v);
                    }
                }                
            }
            return variances;
        }
        /// <summary>
        /// To create string to show the changes of data  of user details in Auditlog
        /// </summary>
        /// <param name="note"></param>
        /// <param name="username"></param>
        /// <param name="varianceList"></param>
        /// <returns></returns>
        public static StringBuilder CreateMessageString(string note, String username, List<Variance> varianceList)
        {
            var str = new StringBuilder();
            str.Append(username + "'s </br>");
            foreach (var data in varianceList)
            {
                if(data.PropertyName == "Description")
                {
                    str.Append(data.PropertyName + " changed. ");
                    str.Append("</br>");
                }
                else if(data.PropertyName == "StatusDate")
                {
                    str.Append(" on "+ data.New+" ");
                }
                else
                { 
                str.Append(data.PropertyName + " changed from " + data.Old + " to " + data.New +".");
                str.Append("</br>");
                }
            }
            if (!String.IsNullOrEmpty(note))
                str.Append("Note:" + note);

            return str;
        }
        /// <summary>
        /// To create a string to show the changes in Auditlog
        /// </summary>
        /// <param name="note"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder  CreateMessage(string note, params string[] value)
        {
            var str = new StringBuilder();
            foreach (string message in value)
            {
                str.Append(message);
            }
            str.Append("</br>");
            if (!String.IsNullOrEmpty(note))
                str.Append("Note:" + note);
            return str;
        }
    
        /// <summary>
        /// To show the switch in Grid
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static string CreateStyleForSwitch(string property)
        {
            return "#if(" + property + "){#" +
                "<span class='label label-success'>Active</span>" +
                "#} else {#" + "<span class='label label-danger'>Inactive</span>" +
                "#}#";
        }
        /// <summary>
        /// To get language Name from language code using enum
        /// </summary>
        /// <param name="languagecode"></param>
        /// <returns></returns>
        public  static string GetLanguageName(string languagecode)
        {
            string language = "";
            foreach (LanguageList data in Enum.GetValues(typeof(LanguageList)))
            {
                if (Convert.ToString(data) == languagecode)

                {
                    language = EnumExtensions.GetDescription(data);
                    return language;
                }

            }
            return language;
        }
        internal static string CreateStyleForBlockedSwitch(string property)
        {
            return "#if (" + property + ") {#" +
                               "<span class= 'label blockedYes '>Yes</span>" +
                            "#} else {#" +
                               "<span class='label blockedNo'>No</span>" +
                            "#}#";
        }
        public static List<SelectListItem> GetCountries(List<SelectListItem> selectList)
        {
            List<string> CountryList = new List<string>();
           // var selectList = new List<SelectListItem>();
            CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in CInfoList)
            {
                RegionInfo R = new RegionInfo(CInfo.LCID);
                if (!(CountryList.Contains(R.EnglishName)))
                {
                    CountryList.Add(R.EnglishName);
                    selectList.Add(
                   new SelectListItem
                   {
                       Value = R.EnglishName,
                       Text = R.EnglishName
                   });
                }
            }
            selectList.OrderBy(x => x.Text);
            return selectList;
        }
     

        internal static IList<object> ModelStateErrors(ModelStateDictionary modelStates)
        {
            IList<object> modelStateErrors = new List<object>();
            foreach (var modelState in modelStates)
            {
                if (modelState.Value.Errors.Count > 0)
                {
                    IList<string> ErrorMessages = new List<string>();
                    foreach (var error in modelState.Value.Errors)
                    {
                        ErrorMessages.Add(error.ErrorMessage);
                    }
                    modelStateErrors.Add(new { element = modelState.Key, errorMessage = ErrorMessages });
                }
            }
            return modelStateErrors;
        }
                   

     

    }
    public class Variance
    {
        public string PropertyName { get; set; }
        public dynamic Old { get; set; }
        public dynamic New { get; set; }
    }
    public static class EnumExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum value)
            where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name)
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }
        /// <summary>
        /// To get the language Description which is set as enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String GetDescription(this Enum value)
        {
            var description = GetAttribute<DescriptionAttribute>(value);
            return description != null ? description.Description : null;
        }
    }

}
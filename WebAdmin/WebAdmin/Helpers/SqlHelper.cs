﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace WebAdmin
{
    ///  <summary>
    ///   Provides helper methods for building SQL fragments for existing C# classes
    ///   </summary>

    public static class Sq1Helper
    {
        ///   <summary>
        ///   Builds and returns a fully qualifies name for a table based on model name or Sq1TableAttribute
        ///   </summary>
        ///   <typeparam  name="T">The model’s type</typeparam>

        public static string GetFullTableNameFor<T>()
        {
            string result;
            Type type = typeof(T);
            string schema = ((SqlSchemaAttribute)type.GetCustomAttribute(typeof(SqlSchemaAttribute)))?.Name ?? "dbo";
            string table = ((SqlTableAttribute)type.GetCustomAttribute(typeof(SqlTableAttribute)))?.Name ?? type.Name;
            result = $"[{schema}].[{table}]";
            return result;
        }

        ///   <summary>
        ///   Builds and returns a comma separated List of SQL columns by fully qualified names for use in SQL statements
        ///   </summary>
        ///   <typeparam  name="T">The model’s type</typeparam>
        ///   <param name= "tableAlias">Optional: The table alias used in the join. Optional.If not present,uses the original name of [Schema]

        public static string GetFullColumnListFor<T>(string tableAlias = null)
        {
            string fullTableName = tableAlias == null ? GetFullTableNameFor<T>() : "[" + tableAlias + "]";
            var colums = GetColumnListOf<T>(false, fullTableName);
            string result = string.Join(", ", colums);
            return result;
        }


        ///   <summary>
        ///   Builds and returns a comma separated list of SQL columns names without schema and table prefix for use in SQL statement.
        ///   </summary>
        ///   <typeparam name="T">The model’s type</typeparam>

        public static string GetShortColumnListFor<T>()
        {
            var columns = GetColumnListOf<T>(false);
            string result = string.Join(", ", columns);
            return result;
        }

        ///   <summary>
        ///    Builds and returns a comma separated List of SQL parameters names (with  @  prefix)  that matches all the property names of the model
        ///   </summary>
        ///   <typeparam  name="T">The model’s type</typeparam>

        public static string GetParameterListFor<T>()
        {
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var columns = new List<string>();
            foreach (var prop in properties)
            {
                columns.Add("@" + prop.Name);
            }
            string result = string.Join(", ", columns);
            return result;
        }

        ///   <summary>
        ///   Build and returns a SELECT statement for a complete data query from a table
        ///   </summary>
        ///   <typeparam name="T">The model’s type</typeparam>

        public static string GetFullSelectQueryFor<T>(string tableAlias = null, int top = 0)
        {
            string result = "SELECT ";
            if (top > 0)
            {
                result = result + $"TOP({ top})  ";
            }
            if (tableAlias == null)
            {
                result += GetShortColumnListFor<T>() + "FROM" + GetFullTableNameFor<T>();
            }
            else
            {
                result += GetFullColumnListFor<T>(tableAlias) + " FROM" + GetFullTableNameFor<T>() + $" AS [{tableAlias}]";
            }
            return result;
        }

        ///   <summary>
        ///   Builds and returns a SELECT statement for a complete data query from a table
        ///   </summary>
        ///   <typeparam  name="T">The model’s type</typeparam>

        public static string GetFullUpdateStatementFor<T>()
        {
            var assignments = new List<string>();
            List<string> columns = GetColumnListOf<T>(true);
            List<string> properties = GetPropertyListOf<T>();
            for (int i = 0; i < columns.Count; i++)
            {
                assignments.Add(columns[i] + " = @" + properties[i]);
            }
            string result = "UPDATE " + GetFullTableNameFor<T>() + " SET " + string.Join(", ", assignments);
            return result;
        }

        ///   <summary>
        ///   Builds and returns an INSERT statement with parameters for a complete data insert into a table
        ///   </summary>
        ///   <typeparam  name="T">The model’s type</typeparam>

        public static string GetFullInsertStatementFor<T>()
        {
            var result = "INSERT INTO " + GetFullTableNameFor<T>() + " (" + GetShortColumnListFor<T>() + ")  VALUES ("
                                          + GetParameterListFor<T>() + ")";
            return result;
        }

        public static string GetCheckIfEntryExists<T>(Guid id)
        {
            var result = "SELECT COUNT(1)  FROM " + GetFullTableNameFor<T>() + " WHERE [Id] = @Id";
            return result;
        }

        private static List<string> GetColumnListOf<T>(bool isColumnListForUpdate, string prefix = null)
        {
            Func<PropertyInfo, bool> whereFunc;
            if (isColumnListForUpdate)
            {
                whereFunc = p => p.GetCustomAttribute(typeof(SqlIgnoreAttribute)) == null &&
                                        p.GetCustomAttribute(typeof(SqlPrimaryKeyAttribute)) == null;
            }
            else
            {
                whereFunc = p => p.GetCustomAttribute(typeof(SqlIgnoreAttribute)) == null;
            }
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance  |  BindingFlags.Public);
            List<string> result = properties
                             .Where(whereFunc)
                              .Select(
                                    p =>
                                    {
                                        string name = ((SqlColumnAttribute) p.GetCustomAttribute(typeof(SqlColumnAttribute)))?.Name
                                                                                        ?? p.Name;
                                        return prefix != null ? $"{prefix}.[{name}]" : $"[{name}]";
    
                                                     }).ToList();
            return result;
        }

        private static List<string> GetPropertyListOf<T>()
        {
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance  | BindingFlags.Public);
            List<string> result = properties
                        .Where(p => p.GetCustomAttribute(typeof(SqlIgnoreAttribute)) == null &&
                                                  p.GetCustomAttribute(typeof(SqlPrimaryKeyAttribute)) == null).Select(p => p.Name).ToList();
            return result;
        }
    }
}

﻿using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public static class GridDialogHelper
    {
        public static DialogBuilder GridDialog(this HtmlHelper helper)
        {
            return helper.Kendo().Dialog()
            .Name("dialog")
            .Title("Confirmation")
            .Width(400)
            .Modal(false)
            .Visible(false)
            .Actions(actions =>
            {
                actions.Add()
                .Text("Cancel")
                .Action("onCancel");
                actions.Add().Text("OK")
                .Primary(true)
                .Action("onOK");
            });
        }
    }
}
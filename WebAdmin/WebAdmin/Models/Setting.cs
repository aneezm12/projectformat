﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class Setting
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
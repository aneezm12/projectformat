﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class Software
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Version")]
        public string Version { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Messages")]
        public string Messages { get; set; }

       
    }
}
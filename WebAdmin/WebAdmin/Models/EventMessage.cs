﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;

namespace WebAdmin
{
    public class EventMessage
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required(ErrorMessage = "*Start Date")]
      //  [UIHint("Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "*End Date")]
      //  [UIHint("Date")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "*Event Start Time")]
        [DataType(DataType.DateTime)]
        public DateTime EventStart { get; set; }

        [Required(ErrorMessage = "*Event End Time")]
        [DataType(DataType.DateTime)]
        public DateTime EventEnd { get; set; }
        [Required(ErrorMessage = "*Time Zone")]
        [Display(Name = "Time Zone")]
        public string TimeZone { get; set; }
        [Required(ErrorMessage = "*Message")]       
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Message")]
        [ForeignKey("MessageId")]
        [UIHint("GridForeignKey")]
        public Guid MessageId { get; set; }

        
      //  public virtual  PopUpMessage Messages { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class AuditLog
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
      
        public int Id { get; set; }
        public DateTime LogDate { get; set; }
        public string LogLevel { get; set; }
        public string Logger { get; set; }
        public string UserId { get; set; }
        public string UserAgent { get; set; }
        public string Url { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string ClientIP { get; set; }
    }
}
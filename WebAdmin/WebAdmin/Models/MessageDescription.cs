﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class MessageDescription
    {
        public DateTime EventStart { get; set; }
        public DateTime EventEnd { get; set; }
        public string MessageTitle { get; set; }
        public string Description { get; set; }
    }
}
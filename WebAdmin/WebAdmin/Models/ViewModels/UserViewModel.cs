﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebAdmin
{
    public class UserViewModel
    {
        public IGridViewModel<User> UserGridViewModel { get; set; }
        public User user { get; set; }
        public AuditLog log { get; set; }

        //[Display(Name = "Id")]
        //public Guid Id { get; set; }

        //public string UserName { get; set; }

        //[Display(Name = "Email")]
        //public string EmailAddress { get; set; }

        //public string Role { get; set; }

        //[Display(Name = "Status")]
        //public bool Status { get; set; }

    }
}
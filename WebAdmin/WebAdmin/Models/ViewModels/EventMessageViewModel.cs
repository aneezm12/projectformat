﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;

namespace WebAdmin
{
    public class EventMessageViewModel
    {
        public GridViewModel<EventMessage> EventMessageGridViewModel => new GridViewModel<EventMessage>();
       
        //public PopUpMessage PopUpMessage { get; set; }

        public EventMessage EventMessage { get; set; }
        public IEnumerable<SelectListItem> MessageList { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
namespace WebAdmin
{
    public class SoftwareViewModel
    {
        public GridViewModel<Software> SoftwareGridViewModel => new GridViewModel<Software>();
        public Software software { get; set; }

    }
}
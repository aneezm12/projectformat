﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;
namespace WebAdmin
{
    public class HardwareViewModel
    {
        public IGridViewModel<Ring> RingGridViewModel { get; set; }
        public IGridViewModel<Strut> StrutGridViewModel { get; set; }
    }
}
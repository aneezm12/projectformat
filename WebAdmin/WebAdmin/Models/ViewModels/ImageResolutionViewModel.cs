﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class ImageResolutionViewModel 
    {
        [Required(ErrorMessage = "Minimum width is required")]       
        [RegularExpression("^[1-9][0-9]{0,}?$", ErrorMessage = "Value must be numeric and greater than 0")]
        public int minwidth { get; set; }

        [Required(ErrorMessage = "Maximum width is required")]
        [RegularExpression("^[1-9][0-9]{0,}?$", ErrorMessage = "Value must be numeric and greater than 0")]
        [GreaterThan("minwidth", ErrorMessage = "Max width should be greater than Min width")]
        public int maxwidth { get; set; }

        [Required(ErrorMessage = "Minimum height is required")]
        [RegularExpression("^[1-9][0-9]{0,}?$", ErrorMessage = "Value must be numeric and greater than 0")]
        public int minheight { get; set; }

        [Required(ErrorMessage = "Maximum height is required")]
        [RegularExpression("^[1-9][0-9]{0,}?$", ErrorMessage = "Value must be numeric and greater than 0")]
        [GreaterThan("minheight", ErrorMessage = "Max height should be greater than Min Height")]
        public int maxheight { get; set; }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;

namespace WebAdmin

{
    public class MessageViewModel
    {
        public GridViewModel<Message> messageGridViewmodel => new GridViewModel<Message>();
        public Message MessageDetails { get; set; }

        //public Guid Id { get; set; }
        //public Guid LanguageId { get; set; }
        //public string LanguageString { get; set; }
        //public string Title { get; set; }
        //public string Description { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
    }
}
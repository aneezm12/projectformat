﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class DashBoardViewModel
    {
        public int ActiveUsers { get; set; }
        public int Countries { get; set; }
        public int Structs { get; set; }
        public int Rings { get; set; }
    }
}
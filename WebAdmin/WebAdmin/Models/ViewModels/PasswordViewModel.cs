﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAdmin.Models.ViewModels
{
    public class PasswordViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Password is required")]

        [Display(Name = "Password")]
        [RegularExpression(@"(?=.*\d)(?=.*[A-Za-z]).{5,}", ErrorMessage = "Your password must be at least 5 characters long and contain at least 1 letter and 1 number")]
        public string Pswd { get; set; }

      //  [NotMapped]
        [Required(ErrorMessage = "Confirm Password required")]
        [CompareAttribute("Pswd", ErrorMessage = "The password and confirmation password do not match.")]
      //  [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }

    }
}
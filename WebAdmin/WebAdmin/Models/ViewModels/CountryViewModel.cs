﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{

    //public class BrowserViewModel
    //{
    //    public GridViewModel<BrowserDetails> BrowserGridViewModel => new GridViewModel<BrowserDetails>();
    //    public IEnumerable<SelectListItem> BrowserList { get; set; }
    //    public BrowserDetails BrowserDetails { get; set; }
    //}
    public class CountryViewModel
    {
        public GridViewModel<Country> CountryGridViewmodel => new GridViewModel<Country>();

        public Country Country { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class BlockIpAddressViewModel
    {

        public GridViewModel<BlockIpAddress> IpaddressGridViewmodel => new GridViewModel<BlockIpAddress>();
        [Required(ErrorMessage = "Required *")]
        [Display(Name = "IP Address")]
        public string IpAddress { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;

namespace WebAdmin
{
    public interface IGridViewModel<T>
    {
        Type ModelType { get; }

        PropertyInfo[] Props { get; }
    }
    public class GridViewModel<TResult> :IGridViewModel<TResult>
    {

        public Type ModelType => typeof(TResult);

        public PropertyInfo[] Props => ModelType.GetProperties();
        
    }
   
}
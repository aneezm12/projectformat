﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;


namespace WebAdmin
{
    public class ConfigurationSettingViewModel
    {
        public GridViewModel<ConfigurationSetting> ConfigurationSetinGridViewModel => new GridViewModel<ConfigurationSetting>();
        public ConfigurationSetting ConfigurationSetting {  get;    set; }

        public BrowserDetails BrowserDetails { get; set; }

        public IEnumerable<SelectListItem> BrowserList { get; set; }


     

    }
}
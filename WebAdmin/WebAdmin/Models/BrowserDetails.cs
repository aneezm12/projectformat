﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class BrowserDetails
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Display(Name = "Search Pattern")]
        [Required(ErrorMessage = "Pattern  is required")]
        public string SearchPattern { get; set; }

        [Display(Name = "Browser Name")]
        [Required(ErrorMessage = "BrowserName  is required")]
        public string BrowserName { get; set; }

        public enum browsers
        {
            chrome,
            firefox,
            safari,
            ie
        }
    }
}
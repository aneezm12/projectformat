﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class Country
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required(ErrorMessage = "Select Country")]
        public string CountryName { get; set; }
        [Display(Name = "Status")]
       

        public bool Status { get; set; }
       
    }
}
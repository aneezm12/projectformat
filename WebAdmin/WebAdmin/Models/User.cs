﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
       

    public enum UserRoles
    {
        Administrator = 0,
        Marketing = 1,
        Surgeon = 2
                     
    }
   
    public class User
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required(ErrorMessage = "Required *")]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required *")]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Display(Name = "UserName")]

        public string Username { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Required *")]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        //public string Role { get; set; }
        [Display(Name = "Creation Date")]
        [Required(ErrorMessage = "Required *")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Last Login Date")]
        [Required(ErrorMessage = "Required *")]
        public DateTime LastLoginDate { get; set; }

        [Required(ErrorMessage = "Required *")]
        [Display(Name = "MarketingPerson")]
        public string MarketingPerson { get; set; }

       // [Required(ErrorMessage = "Required *")]
        [Display(Name = "Country")]

        [HiddenInput(DisplayValue = false)]
        [ForeignKey("CountryId")]

        [UIHint("GridForeignKey")]

        public Guid CountryId { get; set; }
        [Required(ErrorMessage = "Required *")]
        [Display(Name = "Language")]

        [HiddenInput(DisplayValue = false)]
        //[ForeignKey("LanguageId")]

        //[UIHint("GridForeignKey")]

        public Guid LanguageId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool IsResetpassword { get; set; }

        //[HiddenInput(DisplayValue = true)]
        [Display(Name = "UserStatus")]
        public UserStatus Status { get; set; }

        public  bool Blocked { get; set; }
        [Display(Name = "Status Updated Date")]
        [Required(ErrorMessage = "Required *")]
        public DateTime StatusDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsDelete { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int InvalidPasswordAttempts { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime SessionExpiration { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? LockOutUntil { get; set; }

    }
    public enum Persons
    {
        Person1,
        Person2,
        Person3
    }

    public enum UserStatus
    {
        NewUser = 0,
        EmailAddressApproved = 1,
        Activated = 2,
        RegistrationFinished = 3
    }
}
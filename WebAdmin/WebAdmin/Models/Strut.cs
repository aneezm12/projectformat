﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class Strut
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]

        public Guid Id { get; set; } = Guid.NewGuid();


        public string PartNumber { get; set; }


        public string Name { get; set; }


        public float MaxLengthInMM { get; set; }

        private bool disabled;
        public bool Disabled
        {
            get
            {
                return disabled ? false : true;
            }
            set
            {
                disabled = value;
            }
        }
    }
}

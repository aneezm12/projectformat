﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin.Models
{
    public class SecurityQuestion
    {
        public Guid Id { get; set; }

        public string Question { get; set; }
        public string Answer { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    
    public class ConfigurationSetting 
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required(ErrorMessage = "Display Name Required")]

        public string DisplayName { get; set; }
        [Required(ErrorMessage = "OS Required")]
        public string OS { get; set; }

        [Display(Name = "OS Bottom Version")]
        [Required(ErrorMessage = "OS Bottom Version Required")]
        [RegularExpression("^[0-9.]*$", ErrorMessage = "Value must be numeric")]
        public float OS_Bottom_Version { get; set; }

        [Display(Name = "OS Top Version")]
        [Required(ErrorMessage = "OS Top Version Required")]
        [GreaterThan("OS_Bottom_Version", ErrorMessage ="Top should be greater than Bottom in OS")]
        [RegularExpression("^[0-9.]*$", ErrorMessage = "Value must be numeric")]
        public float OS_Top_Version { get; set; }

        [Display(Name = "OS 32/64bit")]
        [Required(ErrorMessage = "OS Bit Version Required")]
        public int? OS_32_64Bit { get; set; }
        
        [Display(Name = "Browser")]
        [Required(ErrorMessage = "Browser Required")]
        public string Browser { get; set; }


        [Display(Name = "Browser Bottom Version")]
        [Required(ErrorMessage = "Bottom Version Required")]
        [RegularExpression("^[0-9.]*$", ErrorMessage = "Value must be numeric")]
        public float Browser_Bottom_Version { get; set; }

        [Display(Name = "Browser Top Version")]
        [Required(ErrorMessage = "Top Version Required")]
        [GreaterThan("Browser_Bottom_Version", ErrorMessage ="Top should be greater than Bottom in Browser")]
        [RegularExpression("^[0-9.]*$", ErrorMessage = "Value must be numeric")]
        public float Browser_Top_Version { get; set; }
       
        [Display(Name = "Browser 32/64bit")]
        [Required(ErrorMessage = "Bit Version Required")]
        public int? Browser_32_64Bit { get; set; }


    }

    public enum OS
    {
        Windows,
        Mac
    }

    public enum Browsers
    {
        Chrome,
        Firefox,
        Safari
    }
   

  
}
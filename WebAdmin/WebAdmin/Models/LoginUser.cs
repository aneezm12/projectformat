﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAdmin
{
   
    public class LoginUser
    {
        [Required(ErrorMessage="UserName is Required")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is Required")]

        public string Password { get; set; }
        public string Roles_Login { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{


    public class Message
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        //    public Guid MessageId { get; set; }
        public Guid MessageId { get; set; } = Guid.NewGuid();
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Required *")]
        public string MessageTitle { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Required *")]
        public string Subject { get; set; }


        //[HiddenInput(DisplayValue = false)]
        //[DataType(DataType.DateTime)]
        //public DateTime StartDate { get; set; }



        //[HiddenInput(DisplayValue = false)]
        //[DataType(DataType.DateTime)]
        //public DateTime EndDate { get; set; }
        //  [HiddenInput(DisplayValue = false)]
        // [DataType(DataType.DateTime)]
        //public string TimeZone { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Required *")]
      //  [HiddenInput(DisplayValue = false)]
        public string Description { get; set; }
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Language")]
        public Guid LanguageId { get; set; }



    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class BlockIpAddress
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required(ErrorMessage = "Required *")]
        [Display(Name = "IP Address")]

        public string IPaddress { get; set; }
    }
}
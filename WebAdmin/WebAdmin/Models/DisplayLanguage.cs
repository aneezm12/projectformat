﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class DisplayLanguage
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool Enabled { get; set; }
    }
}
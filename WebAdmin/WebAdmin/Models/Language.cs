﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class Language
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string CultureCode { get; set; }

        public bool Enabled { get; set; }
    }
}
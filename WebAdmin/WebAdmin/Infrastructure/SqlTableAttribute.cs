﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{

    /// <summary>
    /// Specifies the name of the back table (if different from the class name)
    /// </summary>
    public class SqlTableAttribute : Attribute
    {
        public SqlTableAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
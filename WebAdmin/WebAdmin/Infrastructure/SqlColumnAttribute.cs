﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    /// <summary>
    /// Specifies the name of the backing table column (if different from the property name)
    /// </summary>
    public class SqlColumnAttribute: Attribute
    {
        public SqlColumnAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
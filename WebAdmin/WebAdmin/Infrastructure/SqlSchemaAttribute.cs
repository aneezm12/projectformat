﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{

    /// <summary>
    /// Specifies the name of the database schema of the backing table
    /// </summary>
    public class SqlSchemaAttribute : Attribute
    {
        public SqlSchemaAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

    }
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace WebAdmin
{
    public class HardwareController : WebAdminController
    {
        // GET: Hardware
        private readonly IDataRepository<Ring> ringRepository;
        private readonly IDataRepository<Strut> strutRepository;
        private readonly IGridViewModel<Ring> ringGridViewModel;
        private readonly IGridViewModel<Strut> strutGridViewModel;

        public HardwareController()
        {
        }

        public HardwareController(IDataRepository<Ring> ringRepository,
            IDataRepository<Strut> strutRepository,
           IGridViewModel<Ring> ringGridViewModel,
           IGridViewModel<Strut> strutGridViewModel

           )
        {

            this.ringRepository = ringRepository;
            this.strutRepository = strutRepository;
            this.strutGridViewModel = strutGridViewModel;
            this.ringGridViewModel = ringGridViewModel;




        }

        /// <summary>
        /// To load ring and strut view
        /// </summary>
        /// <returns></returns>
        public ActionResult HardwareData()
        {

            try
            {
                var hModel = new HardwareViewModel
                {
                    RingGridViewModel = ringGridViewModel,
                    StrutGridViewModel = strutGridViewModel
                };

                return View(hModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To populate ring grid 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> RingRead([DataSourceRequest] DataSourceRequest request)
        {
            var getAllData = await this.GetDataBaseFunction<Ring>(request, ringRepository);
            var result = getAllData.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To update ring details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ring"></param>
        /// <param name="textParam"></param>
        /// <returns></returns>
        public async Task<ActionResult> RingUpdate([DataSourceRequest] DataSourceRequest request, Ring ring, string textParam)
        {
            object result = null;
            try
            {
                if (ring != null && ModelState.IsValid)
                {
                    var update = await ringRepository.UpdateById(ring, textParam);
                    var ringData = await ringRepository.GetDatas();
                    result = ringData.ToDataSourceResult(request);
                    //await UpdateById(ring, textParam);
                }

            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To populate strut grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> StrutRead([DataSourceRequest] DataSourceRequest request)
        {

            var getAllData = await this.GetDataBaseFunction<Strut>(request, strutRepository);
            var result = getAllData.ToDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To update strut details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="strut"></param>
        /// <param name="textParam"></param>
        /// <returns></returns>
        public async Task<ActionResult> StrutUpdate([DataSourceRequest] DataSourceRequest request, Strut strut, string textParam)
        {
            object result = null;
            try
            {
                if (strut != null && ModelState.IsValid)
                {
                    var update = await strutRepository.UpdateById(strut, textParam);
                    var strutData = await strutRepository.GetDatas();
                    result = strutData.ToDataSourceResult(request);
                    //await UpdateById(strut, textParam);
                }

            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}
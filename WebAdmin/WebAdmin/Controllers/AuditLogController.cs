﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public class AuditLogController : WebAdminController
    {
        private readonly IDataRepository<AuditLog> auditLogRepository;
        private readonly IGridViewModel<AuditLog> auditLogGridViewModel;
        

        // GET: AuditLog

        public AuditLogController(IDataRepository<AuditLog> auditLog_Repository, IGridViewModel<AuditLog> auditLogGridViewModel)
        {
            //var st = new DataRepository<User>(new DatabaseService());
            auditLogRepository = auditLog_Repository;
            this.auditLogGridViewModel = auditLogGridViewModel;

        }
        /// <summary>
        /// To load Audit log view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                return View(auditLogGridViewModel);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error("AuditLog Index : "+exception.Message, exception);
                throw exception;
            }
        }
        /// <summary>
        /// To populate Auditlog grid
        /// </summary>
        /// <param name="request"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
       [HttpPost]
        public async Task<ActionResult> AuditLogRead([DataSourceRequest] DataSourceRequest request, string startDate, string endDate)
        {
            DataSourceResult result = null;
            IEnumerable<AuditLog> userdata = null;
            TempData["startdate"] = startDate;
            TempData["enddate"] = endDate;
            try
            {
                if (String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate))
                {
                     userdata = await auditLogRepository.GetDatas();
                }
                else
                {

                    userdata = null;
                    var getAll = AsyncUtilHelper.AsyncUtil.RunSync(() => auditLogRepository.GetDatas());
                    var date = Convert.ToDateTime(startDate).ToString("yyyy-MM-dd");
                    var _userdata = getAll.Where(x =>
                    Convert.ToDateTime(startDate) != Convert.ToDateTime(endDate)
                    ? x.LogDate > Convert.ToDateTime(startDate) && x.LogDate <= Convert.ToDateTime(endDate).AddDays(1)
                    : x.LogDate.ToString("yyyy-MM-dd") == date).ToList();
                    userdata = _userdata.OrderByDescending(x=>x.LogDate);
                    
                }
            }
            catch(Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
            }
            userdata = userdata.OrderByDescending(x => x.LogDate);
            result = userdata.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To delete the Auditlog details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="auditLog"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> AuditLogDelete([DataSourceRequest] DataSourceRequest request, AuditLog auditLog)
        {
            try
            {
                await DeleteValue(auditLogRepository, auditLog);
             
                return Json(new[] { auditLog }.ToDataSourceResult(request, ModelState));
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw exception;
            }
        }
    }
}
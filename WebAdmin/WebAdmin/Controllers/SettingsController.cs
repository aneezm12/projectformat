﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebAdmin;
using WebAdmin.Models;

namespace WebAdmin
{
    public class SettingsController : WebAdminController
    {
        private readonly IDataRepository<Setting> resolutionRespository;
        private IEnumerable<Setting> settingValue;
        private readonly IDataRepository<ConfigurationSetting> configurationSettingRepository;
        private readonly IDataRepository<BrowserDetails> browserRepository;
        private ImageResolutionViewModel imageViewModel;
        private GtinViewModel gtinViewModel;
        public SettingsController(IDataRepository<Setting> dataRepository, IDataRepository<ConfigurationSetting> configurationSettingRepository, IDataRepository<BrowserDetails> browserRepository)
        {
            this.resolutionRespository = dataRepository;
            this.configurationSettingRepository = configurationSettingRepository; ;
            this.browserRepository = browserRepository;
        }

        // GET: Settings
        #region Image View
       /// <summary>
            /// To set the model for Gtin and Image
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
        private string SetModel(string key)
        {
            return settingValue?.FirstOrDefault(x => x.Key.Equals(key))?.Value ?? string.Empty;
        }
        /// <summary>
        /// To load Image details
        /// </summary>
        /// <returns></returns>
        public ActionResult Image()
        {

            imageViewModel = new ImageResolutionViewModel();
            try
            {
                if (Session["ImageModel"] == null)
                {
                    settingValue = AsyncUtilHelper.AsyncUtil.RunSync(() => resolutionRespository.GetDatas());

                    imageViewModel.minwidth = int.Parse(this.SetModel("minwidth"));
                    imageViewModel.maxwidth = int.Parse(this.SetModel("maxwidth"));
                    imageViewModel.minheight = int.Parse(this.SetModel("minheight"));
                    imageViewModel.maxheight = int.Parse(this.SetModel("maxheight"));
                    Session["ImageModel"] = imageViewModel;
                }
                else
                {
                    imageViewModel = Session["ImageModel"] as ImageResolutionViewModel;
                }

            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
            }
            return View("Image", imageViewModel);
        }
        /// <summary>
        /// To Update image details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateImage(ImageResolutionViewModel model)
        {
            int update = 0;
            string message = "";
            string textParam = "";
            bool isSuccess = false;

            try
            {
                if (ModelState.IsValid)
                {
                    IDictionary<string, int> dict = new Dictionary<string, int>();
                    dict.Add(new KeyValuePair<string, int>("minwidth", model.minwidth));
                    dict.Add(new KeyValuePair<string, int>("maxwidth", model.maxwidth));
                    dict.Add(new KeyValuePair<string, int>("minheight", model.minheight));
                    dict.Add(new KeyValuePair<string, int>("maxheight", model.maxheight));

                    foreach (KeyValuePair<string, int> entry in dict)
                    {
                        var settingmodel = new Setting
                        {
                            Key = entry.Key,
                            Value = entry.Value.ToString()
                        };
                        update = await resolutionRespository.UpdateById(settingmodel, textParam);
                        if (update != 1) { break; }
                    }
                    Func<string> addSession = () =>
                    {
                        Session["ImageModel"] = model;
                        isSuccess = update == 1;
                        return "Added Successfully";
                    };
                    message = update == 1 ? addSession() : "Failed";
                    return Json(new { message = message, status = isSuccess, keepModelValues = true });
                }
                else
                {
                    isSuccess = false;
                    IList<object> modelStateErrors = DataHelper.ModelStateErrors(ModelState);
                    return Json(new { message = message, status = isSuccess, validationErrors = modelStateErrors });
                }
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }
#endregion

        #region GTIN View
        /// <summary>
        /// To get the Gtin value
        /// </summary>
        /// <returns></returns>
        public ActionResult Gtin()
        {
            gtinViewModel = new GtinViewModel();
            try
            {
                if (Session["gtin"] == null)
                {
                    this.settingValue = AsyncUtilHelper.AsyncUtil.RunSync(() => resolutionRespository.GetDatas());
                    gtinViewModel.gtin = this.SetModel("gtin");
                    Session["gtin"] = gtinViewModel.gtin;
                }
                else
                {
                    gtinViewModel.gtin = Session["gtin"].ToString();
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
            }
            return View("Gtin", gtinViewModel);
        }
        /// <summary>
        /// To update the Gtin value
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ActionResult> UpdateGtin(GtinViewModel model)
        {
            int update = 0;
            string msg = "";
            string textParam = "";
            bool isSuccess = false;
            try
            {
                if (ModelState.IsValid)
                {
                    string gtinno = model.gtin;
                    var modelsetting = new Setting
                    {
                        Key = "gtin",
                        Value = gtinno
                    };
                    update = await resolutionRespository.UpdateById(modelsetting, textParam);
                    if (update == 1)
                    {
                        isSuccess = true;
                        msg = "Updated Successfully";
                        TempData["gtin"] = gtinno;
                        Session["gtin"] = gtinno;

                    }
                    else
                    {
                        isSuccess = false;
                        msg = "Updation Failed";
                    }
                    return Json(new { message = msg, status = isSuccess, keepModelValues = true });
                }
                else
                {
                    isSuccess = false;
                    IList<object> modelStateErrors = DataHelper.ModelStateErrors(ModelState);
                    return Json(new { message = msg, status = isSuccess, validationErrors = modelStateErrors });
                }
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }
        #endregion


        #region SupportedVersion
        /// <summary>
        /// To load Supported version view
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ConfigurationSettingIndex()
        {
            var configViewModel = new ConfigurationSettingViewModel();
            var selectList = new List<SelectListItem>();
            try
            {
                
                MvcApplication.Logger.Info("Supported Version Details");
                IEnumerable<BrowserDetails> selectData = await browserRepository.GetDatas();
                
                foreach (BrowserDetails data in selectData)
                {
                    selectList.Add(
                         new SelectListItem
                         {
                             Value = data.Id.ToString(),
                             Text = data.BrowserName // + " From " + data.BrowserMinorVersion + " Version"
                         });

                }
                var BrowserTip = new SelectListItem()
                {
                    Value = null,
                    Text = "--- select Browser ---"
                };
                selectList.Insert(0, BrowserTip);

                configViewModel.BrowserList = new SelectList(selectList, "Value", "Text");

                
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                //throw ex;
            }
            return View(configViewModel);
        }

        /// <summary>
        /// To add new configuration details
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        [HttpPost]        
        public async Task<ActionResult> ConfigurationSettingAdd(ConfigurationSettingViewModel st)
        {
            string message = string.Empty;
           
                    MvcApplication.Logger.Debug("Added Supported Version Details");
            try
            {
                var getAll = AsyncUtilHelper.AsyncUtil.RunSync(() => browserRepository.GetDatas());
                var getBrowser = (from browser in getAll
                                  where Convert.ToString(browser.Id) == st.ConfigurationSetting.Browser
                                  select new { browser.BrowserName }).ToList();
                st.ConfigurationSetting.Browser = getBrowser.Count > 0 ? Convert.ToString(getBrowser.FirstOrDefault().BrowserName) : "";
                await AddValue(configurationSettingRepository, st.ConfigurationSetting);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                Message = exception.Message;
                IsSuccess = false;
                throw;
            }
            return Json(new { message = this.Message, status = IsSuccess, validationErrors = this.ModelStateErrors });

        }
        /// <summary>
        /// To populate supported version grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> ConfigurationSettingRead([DataSourceRequest]DataSourceRequest request)
        {
            var getAllData = await this.GetDataBaseFunction<ConfigurationSetting>(request, configurationSettingRepository);
            var result = getAllData.ToDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To delete supported version details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="supportedVersion"></param>
        /// <returns></returns>
        

        [HttpPost]
        public async Task<JsonResult> ConfigurationSettingDelete(ConfigurationSetting data)
        {
            bool isSuccess = false;
            var msg = "";
            try
            {
                if (!String.IsNullOrEmpty(data.Id.ToString()))
                {
                    ConfigurationSetting supportedVersion = new WebAdmin.ConfigurationSetting();
                    supportedVersion.Id = data.Id;
                    await DeleteValue(configurationSettingRepository, supportedVersion);

                    MvcApplication.Logger.Info(data.DisplayName + " deleted");
                    isSuccess = true;
                    msg = "Deleted successfully !";
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                msg = "Deleted failed !";
            }
            return Json(new { message = msg, status = isSuccess });
        }
        #endregion
    }
}
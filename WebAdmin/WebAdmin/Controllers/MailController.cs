﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace WebAdmin
{
    public class MailController : WebAdminController
    {
        private readonly IDataRepository<Message> messageRepository;

        private readonly IEmailService mailService;
        /// <summary>
        /// The culture code service.
        /// </summary>
        private readonly ICultureCodeService cultureCodeService;

        /// <summary>
        /// The heading value.
        /// </summary>
        private static string headingValue;
        private readonly IDataRepository<Language> languageRepository;
        private readonly IDataRepository<User> userRepository;

        private readonly IDataRepository<EventMessage> eventMessageRepository;
        /// <summary>
        /// Initializes a new instance of the <see cref="MailController"/> class.
        /// </summary>
        /// <param name="messageRepository">
        /// The message repository.
        /// </param>
        /// <param name="mailService">
        /// The mail service.
        /// </param>
        /// <param name="languageRepository">
        /// The language repository.
        /// </param>
        ///   <param name="eventMessageRepository">
        /// The Event Message Repository.
        /// </param>
        ///  ///   <param name="userRepository">
        /// The user repository.
        /// </param>
        /// <param name="cultureCodeService">
        /// The culture code service.
        /// </param>
        /// ///
        public MailController(
            IDataRepository<Message> messageRepository,
            IEmailService mailService,
            IDataRepository<Language> languageRepository,
            IDataRepository<EventMessage> eventMessageRepository,
           IDataRepository<User> userRepository,
            ICultureCodeService cultureCodeService)
        {
            this.messageRepository = messageRepository;
            this.mailService = mailService;
            this.eventMessageRepository = eventMessageRepository;
            this.languageRepository = languageRepository;
            this.cultureCodeService = cultureCodeService;
            this.userRepository = userRepository;
        }
        /// <summary>
        /// To get the description based on the Message title
        /// </summary>
        /// <param name="heading"></param>
        /// <returns></returns>
        public ActionResult GetDescription(string heading)
        {
            string description = "";
            var dateregex = new Regex(@"\d{1,2}\/\d{1,2}\/\d{1,4} \d\d?\:\d\d\:\d\d\s?(A|P)M");
            Match date = dateregex.Match(heading);
            headingValue = heading;
            string defaultCode = "";
            foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
            {
                bool matchLanguage = Regex.IsMatch(Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]), Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                if (matchLanguage)
                {
                    MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                    defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                }

            }
            var getDefaultLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas()).Where(x => x.CultureCode.Split('-').First() ==defaultCode);
            IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            if (!String.IsNullOrEmpty(date.Value))
            {

                string title = Regex.Replace(heading, date.Value, " ");

                title = title.Trim();

                DateTime d = DateTime.ParseExact(date.Value, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                DateTime utcStartDate = d.ToUniversalTime();


                EventMessage getEvent = AsyncUtilHelper.AsyncUtil.RunSync(() => eventMessageRepository.GetDatas()).Where(x => x.EventStart == utcStartDate).FirstOrDefault();

                foreach (var language in getDefaultLanguage)
                {
                 var   EventMessageDes = getAllMessages.Where(x => x.MessageTitle == title && x.LanguageId == language.Id).FirstOrDefault();

                    /////Get message by EventStart and Title////
                    ///

                    //var messageData = messageRepository.GetMessageDescriptionByTitleAndEventStart(utcStartDate, title);

                    if (EventMessageDes != null)
                    {
                        DateTime localDate;

                        // code below helps to get data from DapperRow object
                        DateTime eventStart = getEvent.EventStart;
                        DateTime eventEnd = getEvent.EventEnd;
                        description = EventMessageDes.Description;

                        if (description.Contains("<Startdate>"))
                        {
                            localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventStart), TimeZoneInfo.Local);
                            description = description.Replace("<Startdate>", " " + localDate.ToShortDateString());
                        }
                        if (description.Contains("<Starttime>"))
                        {
                            localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventStart), TimeZoneInfo.Local);
                            description = description.Replace("<Starttime>", " " + localDate.ToLongTimeString());
                        }
                        if (description.Contains("<Enddate>"))
                        {
                            localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventEnd), TimeZoneInfo.Local);
                            description = description.Replace("<Enddate>", " " + localDate.ToShortDateString());
                        }
                        if (description.Contains("<Endtime>"))
                        {
                            localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventEnd), TimeZoneInfo.Local);
                            description = description.Replace("<Endtime>", " " + localDate.ToLongTimeString());
                        }
                        return Json(description);
                    }
                    }

                }
            else
            {

                IEnumerable<Language> getAllLanguages = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());
                var message = new Message();
                var languageId = new Guid();

                heading = heading.Trim();

                IEnumerable<Language> allLanguages = getAllLanguages.ToList();
                IEnumerable<Message> allMessages = getAllMessages.ToList();

                List<string> englishLanguageList = MessageHelper.GetLanguage(getAllLanguages, WebConfigurationManager.AppSettings["DefaultLanguage"]);//Should be English
                List<string> userLanguageList = null;

                if (!string.IsNullOrEmpty(Request?.UserLanguages?.ToString()))
                {
                    string userLanguage = Convert.ToString(Convert.ToString(Request?.UserLanguages?.First().Split('-').First()));
                    userLanguageList = MessageHelper.GetLanguage(allLanguages, userLanguage);
                }
                if (userLanguageList != null)
                {
                    foreach (string data in userLanguageList)
                    {
                        languageId = new Guid(data);
                        message = (from messages in allMessages
                                   where messages.MessageTitle == heading && messages.LanguageId == languageId
                                   select messages).FirstOrDefault();
                        if (message != null)
                            return Json(message.Description);
                    }
                }

                if (message == null)
                {
                    foreach (string data in englishLanguageList)
                    {
                        languageId = new Guid(data);
                        message = (from messagedata in getAllMessages
                                   where messagedata.MessageTitle == heading && messagedata.LanguageId == languageId
                                   select messagedata).FirstOrDefault();
                        if (message != null)
                            return Json(message.Description);
                    }
                }
            }
            return Json(description);
        }
        /// <summary>
        /// To populate Message combobox with the combination of message title and event start date
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSubject(string heading)
        {
            string title = "";
            string subject = "";
            var dateregex = new Regex(@"\d{1,2}\/\d{1,2}\/\d{1,4} \d\d?\:\d\d\:\d\d\s?(A|P)M");
            Match date = dateregex.Match(heading);
            headingValue = heading;
            string defaultCode = "";
            foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
            {
                bool matchLanguage = Regex.IsMatch(Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]), Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                if (matchLanguage)
                {
                    MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                    defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                }

            }
            var getDefaultLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas()).Where(x => x.CultureCode.Split('-').First() == defaultCode);
            IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            //if (date.Length != 0)
            //{
            //    title = Regex.Replace(heading, date.Value, " ");
            //}
            //else
            //{
            //    title = heading;
            //}
            title = date.Length != 0 ? Regex.Replace(heading, date.Value, " ") : heading;
            title = title.Trim();


            foreach (var language in getDefaultLanguage)
                {
                    var EventMessageDes = getAllMessages.Where(x => x.MessageTitle == title && x.LanguageId == language.Id).FirstOrDefault();

                    if (EventMessageDes != null)
                    {                 
                        subject = EventMessageDes.Subject;                       
                    }
                
            }
            return Json(subject);
        }

        public string GetMailSubject(string id, string msgContent,string title)
        {
            string subject = "";
            //var dateregex = new Regex(@"\d{1,2}\/\d{1,2}\/\d{1,4} \d\d?\:\d\d\:\d\d\s?(A|P)M");
            //Match date = dateregex.Match(heading);
            //headingValue = heading;
            IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());
            IEnumerable<User> getAllUsers = AsyncUtilHelper.AsyncUtil.RunSync(() => userRepository.GetDatas());
            IEnumerable<Message> getAllMessage = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());

            string defaultLanguage = Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]);
            var defaultCode = "";
            var userLanguageCode = "";
            Guid languageId = (from data in getAllUsers

                               where Convert.ToString(data.Id) == id
                               select data.LanguageId).SingleOrDefault();

            var languageName = (from data in getAllLanguage
                                where Convert.ToString(data.Id) == Convert.ToString(languageId)
                                select new { data.Name, data.CultureCode }).SingleOrDefault();
            foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
            {
                bool matchDefaultLanguage = Regex.IsMatch(defaultLanguage, Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                if (matchDefaultLanguage)
                {
                    MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                    defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                }
                else
                {
                    if (languageName.Name.ToUpper().Contains(Convert.ToString(language).ToUpper()))
                    {
                        MultiMap<string, string> cultures = this.cultureCodeService.GetCultureCodeList(language);
                        userLanguageCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                    }
                }
            }
            
            if (!userLanguageCode.Contains(defaultCode))
            {
                string headingTitle = CheckHeading(title);
                string messageSubject = (from data in getAllMessage
                                       where data.LanguageId == languageId
                                       && headingTitle.TrimEnd() == data.MessageTitle
                                       select data.Subject).SingleOrDefault();
                try
                {

                    if (messageSubject != null)
                    {
                        subject = messageSubject;
                            
                        
                    }
                }catch(Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                }
                }
            return subject;
        }
        public ActionResult ComboRead()
        {

            IEnumerable<Message> getAllMessage = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            IEnumerable<EventMessage> allEventMessage = AsyncUtilHelper.AsyncUtil.RunSync(() => this.eventMessageRepository.GetDatas());
            IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());

            List<SelectListItem> messageSelectList;
            string userLanguage = Convert.ToString(Convert.ToString(Request?.UserLanguages?.First().Split('-').First()));

            messageSelectList = MessageHelper.GetMessagesList(getAllLanguage, getAllMessage, userLanguage);
            var messageList = new List<Message>();
            foreach (SelectListItem data in messageSelectList)
            {
                var item = new Message { MessageId = new Guid(data.Value), MessageTitle = data.Text };
                messageList.Add(item);
            }



            var dataList = from messages in messageList
                           join events in allEventMessage on messages.MessageId equals events.MessageId into result
                           from events in result.DefaultIfEmpty()
                           select new
                           {
                               prop1 = messages.MessageTitle,
                               prop2 = events == null ? " " : TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(events.EventStart),
                                           TimeZoneInfo.Local).ToString(CultureInfo.CurrentCulture)

                           };
            var selectList = new List<SelectListItem>();
            foreach (var data in dataList)
            {
                string text = data.prop1 + " " + data.prop2;
                selectList.Add(
                     new SelectListItem
                     {
                         Value = Guid.NewGuid().ToString(),
                         Text = text
                     });
            }

            // selectList.OrderBy(x => x.Text);
            ViewData["MessageId"] = selectList;
            IEnumerable<SelectListItem> message = new SelectList(selectList, "Value", "Text").OrderBy(x => x.Text);
            return Json(message, JsonRequestBehavior.AllowGet);
        }




        /// <summary>
        /// To send mail
        /// </summary>
        /// <param name="messageContent">
        /// The messageContent.
        /// </param>
        /// <param name="mailList">
        /// mailList
        /// </param>
        /// <param name="heading">
        /// The heading.
        /// </param>
        /// <returns>
        /// JSON
        /// </returns>

        [HttpPost]
        public JsonResult SendMail(string messageContent, List<User> mailList, string heading)
        {
            int mail;
            bool mailStatus;
            //Dictionary<User, string> mapList = GetEmailMessage(mailList, messageContent, heading);
            Dictionary<User, Tuple<string,string>> mapList = GetEmailMessage(mailList, messageContent, heading);

          //  string mailSubject = GetMailSubject(heading);
            mail = mailService.SendEventMessageMail(mapList);

            mailStatus = mail == 0;

            MvcApplication.Logger.Info(mailStatus ? "Mail Send" : "Mail Send Failed");

            return Json(new { mail, status = mailStatus });



        }
        private string GetDescriptionByLanguage(string id, string msgContent, string title)
        {
            IEnumerable<User> getAllUsers = AsyncUtilHelper.AsyncUtil.RunSync(() => userRepository.GetDatas());
            IEnumerable<Message> getAllMessage = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());

            string defaultLanguage = Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]);
            var defaultCode = "";
            var userLanguageCode = "";
            Guid languageId = (from data in getAllUsers

                               where Convert.ToString(data.Id) == id
                               select data.LanguageId).SingleOrDefault();

            var languageName = (from data in getAllLanguage
                                where Convert.ToString(data.Id) == Convert.ToString(languageId)
                                select new { data.Name, data.CultureCode }).SingleOrDefault();
            foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
            {
                bool matchDefaultLanguage = Regex.IsMatch(defaultLanguage, Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                if (matchDefaultLanguage)
                {
                    MultiMap<string, string> cultures = cultureCodeService.GetCultureCodeList(language);
                    defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                }
                else
                {
                    if (languageName.Name.ToUpper().Contains(Convert.ToString(language).ToUpper()))
                    {
                        MultiMap<string, string> cultures = this.cultureCodeService.GetCultureCodeList(language);
                        userLanguageCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                    }
                }
            }

            if (!userLanguageCode.Contains(defaultCode))

            {
                string headingTitle = CheckHeading(title);
                string messageTitle = (from data in getAllMessage
                                       where data.LanguageId == languageId
                                       && headingTitle.TrimEnd() == data.MessageTitle
                                       select data.Description).SingleOrDefault();
                try
                {

                    if (messageTitle != null)
                    {
                        string messageContent = Convert.ToString(this.GetMessageDescription(headingValue, messageTitle));

                        if (!string.IsNullOrEmpty(messageContent))
                            return messageContent;

                    }

                }
                catch (Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message);
                }

            }
            return msgContent;
        }
        /// <summary>
        /// To send mail
        /// </summary>
        /// <param name="mailList">
        /// The mail list.
        /// </param>
        /// <param name="msgContent">
        /// The msg content.
        /// </param>
        /// <param name="heading">
        /// The heading.
        /// </param>
        /// <returns>Dictionary
        /// </returns>
        //public Dictionary<User, string> GetEmailMessage(List<User> mailList, string msgContent, string heading)
        public Dictionary<User, Tuple<string,string>> GetEmailMessage(List<User> mailList, string msgContent, string heading)

        {
            var userAndMessage = new Dictionary<WebAdmin.User, Tuple<string,string>>();
            foreach (User data in mailList)
            {
                userAndMessage.Add(data, new Tuple<string, string>(this.GetDescriptionByLanguage(Convert.ToString(data.Id), msgContent, heading),GetMailSubject(Convert.ToString(data.Id),msgContent, heading)));
            }

            return userAndMessage;
        }

        /// <summary>
        /// The get message description.
        /// </summary>
        /// <param name="heading">
        /// The heading.
        /// </param>
        /// <param name="messageDescription">
        /// The message description.
        /// </param>
        /// <returns>string
        /// </returns>
        public string GetMessageDescription(string heading, string messageDescription)
        {
            IEnumerable<EventMessage> getAllEvents = AsyncUtilHelper.AsyncUtil.RunSync(() => this.eventMessageRepository.GetDatas());
            IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            var description = "";
            var dataRegex = new Regex(@"\d{1,2}\/\d{1,2}\/\d{1,4} \d\d?\:\d\d\:\d\d\s?(A|P)M");
            Match date = dataRegex.Match(heading);
            if (!string.IsNullOrEmpty(date.Value))
            {

                string title = Regex.Replace(heading, date.Value, " ");
                title = title.Trim();

                DateTime d = DateTime.ParseExact(date.Value, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                DateTime utcStartDate = d.ToUniversalTime();

                // get event list with EventStart eq to startDate in our heading
                IEnumerable<EventMessage> eventList = getAllEvents.Where(x => x.EventStart == utcStartDate);

                // get message list with Title eq to our heading
                IEnumerable<Message> messageList = getAllMessages.Where(y => y.MessageTitle == title);

                // Fetch a Message row with events.MessageId eq messages.MessageId from the above lists
                var messageData = (from events in eventList
                                   join messages in messageList on events.MessageId equals messages.MessageId
                                   select new { events.EventStart, events.EventEnd, messages.Description }).First();
                description = messageDescription;

                if (messageData != null)
                {
                    DateTime localDate;
                    var rgxStartDate = new Regex("<Startdate>", RegexOptions.IgnoreCase);
                    var rgxEndDate = new Regex("<Enddate>", RegexOptions.IgnoreCase);
                    var rgxEndTime = new Regex("<Endtime>", RegexOptions.IgnoreCase);
                    var rgxStartTime = new Regex("<starttime>", RegexOptions.IgnoreCase);
                    // description = messageData.Description;
                    // if (rgxStartDate.Match(messageData.Description).Success)

                    if (rgxStartDate.Match(description).Success)
                    {
                        localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(messageData.EventStart), TimeZoneInfo.Local);
                        description = Regex.Replace(description, "<Startdate>", " " + localDate.ToShortDateString(), RegexOptions.IgnoreCase);
                    }

                    if (rgxStartTime.Match(description).Success)

                    {
                        localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(messageData.EventStart), TimeZoneInfo.Local);
                        description = Regex.Replace(description, "<Starttime>", " " + localDate.ToLongTimeString(), RegexOptions.IgnoreCase);
                    }

                    if (rgxEndDate.Match(description).Success)
                    {
                        localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(messageData.EventEnd), TimeZoneInfo.Local);
                        description = Regex.Replace(description, "<Enddate>", " " + localDate.ToShortDateString(), RegexOptions.IgnoreCase);
                    }

                    if (rgxEndTime.Match(description).Success)
                    {
                        localDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(messageData.EventEnd), TimeZoneInfo.Local);
                        description = Regex.Replace(description, "<Endtime>", " " + localDate.ToLongTimeString(), RegexOptions.IgnoreCase);
                    }
                }
            }
            else
            {

                description = messageDescription;
                var rgxStartDate = new Regex("<Startdate>", RegexOptions.IgnoreCase);
                var rgxEndDate = new Regex("<Enddate>", RegexOptions.IgnoreCase);
                var rgxEndTime = new Regex("<Endtime>", RegexOptions.IgnoreCase);
                var rgxStartTime = new Regex("<starttime>", RegexOptions.IgnoreCase);
                // description = messageData.Description;
                // if (rgxStartDate.Match(messageData.Description).Success)

                if (rgxStartDate.Match(description).Success)
                {
                    description = Regex.Replace(description, "<Startdate>", " ", RegexOptions.IgnoreCase);
                }

                if (rgxStartTime.Match(description).Success)

                {
                    description = Regex.Replace(description, "<Starttime>", " ", RegexOptions.IgnoreCase);
                }

                if (rgxEndDate.Match(description).Success)
                {
                    description = Regex.Replace(description, "<Enddate>", " ", RegexOptions.IgnoreCase);
                }

                if (rgxEndTime.Match(description).Success)
                {
                    description = Regex.Replace(description, "<Endtime>", " ", RegexOptions.IgnoreCase);
                }

            }


            return description;
        }

        private string CheckHeading(string heading)
        {
            var dateRegex = new Regex(@"\d{1,2}\/\d{1,2}\/\d{1,4} \d\d?\:\d\d\:\d\d\s?(A|P)M");
            Match date = dateRegex.Match(heading);
            string title;
            if (!string.IsNullOrEmpty(date.Value))
            {

                title = Regex.Replace(heading, date.Value, " ");
                title = title.Trim();

            }
            else
            {
                title = heading;
            }
            return title;

        }


    }
}
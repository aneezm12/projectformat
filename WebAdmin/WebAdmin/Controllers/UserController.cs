﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebAdmin.Models;

namespace WebAdmin
{
    public class UserController : WebAdminController
    {
        private readonly IDataRepository<User> userRepository;
        private readonly IGridViewModel<User> userGridViewModel;
        private readonly IDataRepository<Country> countryRepository;
        private readonly IDataRepository<Message> messageRepository;
        private readonly IDataRepository<Language> languageRepository;
        private readonly IDataRepository<EventMessage> eventmessageRepository;
        private readonly IDataRepository<Contact> contactRepository;
        private readonly IGridViewModel<Country> countryGridViewModel;
        private readonly IDataRepository<Ring> ringRepository;
        private readonly IDataRepository<Strut> strutRepository;

        public static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
      
        public UserController(IDataRepository<User> userRepository, IDataRepository<Country> countryRepository, IDataRepository<Message> messageRepository,
            IDataRepository<EventMessage> eventmessageRepository, IDataRepository<Language> languageRepository, IGridViewModel<User> userGridViewModel, 
            IGridViewModel<Country> countryGridViewModel, IDataRepository<Contact> contactRepository, IDataRepository<Ring> ringRepository, IDataRepository<Strut> strutRepository)
        {
            this.userRepository = userRepository;
            this.countryRepository = countryRepository;
            this.messageRepository = messageRepository;
            this.languageRepository = languageRepository;
            this.eventmessageRepository = eventmessageRepository;
            this.userGridViewModel = userGridViewModel;
            this.countryGridViewModel = countryGridViewModel;
            this.contactRepository = contactRepository;
            this.ringRepository = ringRepository;
            this.strutRepository = strutRepository;
        }
        #region User

        /// <summary>
        /// To update User details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <param name="textParam"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UserUpdate([DataSourceRequest] DataSourceRequest request, User user, string textParam)
        {
            int mail = 1;
            IEmailService emailService = new EmailService();
            bool status = false;
            int accountapprove = 0;
            if (user != null && ModelState.IsValid)
            {
                try
                {
                    accountapprove = await userRepository.UpdateById(user, textParam);
                    if (accountapprove == 1) //Status changed from "EmailAddressApproved" to "Activated"
                    {
                        mail = emailService.SendUserApproveMail(user);
                        status = mail == 0 ? true : false;
                    }
                }
                catch (Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                    throw exception;
                }
            }
            return Json(new { mail = mail, status = status, accountapprove = accountapprove});
        }

        /// <summary>
        /// To disapprove the user  by Marketing manager
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        //public async Task<JsonResult> UserDisApprove([DataSourceRequest]DataSourceRequest request, User user)
        public  JsonResult UserDisApprove(User user,string disapproveComment)

        {
            int mail = 0;
            IEmailService emailService = new EmailService();
            mail = emailService.SendUserDisapproveMail(user,disapproveComment);
            if (user == null)
                return null;
            try
            {
                 userRepository.DeleteByIdAsync(user);

                MvcApplication.Logger.Info(user.Username + " DisApproved");
               
                if (mail == 0)
                    return Json(new { mail = mail, status = true });
                else
                    return Json(new { mail = mail, status = false });
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw exception;
            }
        }
        /// <summary>
        /// To load user view
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            try
            {
                UserViewModel userViewModel = new UserViewModel();

                Logger.Info("Loaded User Details");
                Session["User"] = "UserIndex";
                //Session["UserRole"] = "Marketing";
                Session["UserRole"] = "SuperAdmin";
                // Session["UserRole"] = "Admin";
                var Msg_selectList = new List<SelectListItem>();
                List<SelectListItem> selectList = await getListCountries();
                ViewBag.CountryList = selectList;
                List<SelectListItem> languguageList = await GetLanguage();
                ViewBag.LanguguageList = languguageList;
                return View(userGridViewModel);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// To load inactive user view
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> InActive()
        {
            Logger.Info("Loaded Inactive User Details");
            Session["User"] = "InActive";
            List<SelectListItem> selectList = await getListCountries();

            return View(userGridViewModel);
        }

        /// <summary>
        /// To populate language combobox in User EditorTemplate
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<List<SelectListItem>> GetLanguage()
        {
            var languageselectList = new List<SelectListItem>();

            try
            {
                var lang = await languageRepository.GetDatas();

                foreach (var data in lang)
                {
                    if (Convert.ToString(data.Enabled) == "True")
                    {
                        languageselectList.Add(
                             new SelectListItem
                             {
                                 Value = data.Id.ToString(),
                                 Text = data.CultureCode + "(" + data.Name + ")"
                             });
                    }
                }
                IEnumerable<SelectListItem> language = new SelectList(languageselectList, "Value", "Text");
                ViewBag.LanguguageList = language;
            
           
        }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw exception;
            }
            return languageselectList;
        }
        /// <summary>
        /// To get the list of countries
        /// </summary>
        /// <returns></returns>
        private async Task<List<SelectListItem>> getListCountries()
        {
          
            var countryData = await countryRepository.GetDatas();
            if (countryData == null)
                throw new ArgumentNullException(nameof(countryData));
            IEnumerable<Country> countriesLst = countryData.ToList();
            var selectList = new List<SelectListItem>();
            if (countriesLst.Any())
            {
                var countryByStatus = from data in countriesLst
                                      where Convert.ToString(data.Status) == "True"
                                      select data;
                List<string> ExCountryList = new List<string>();
                List<string> CountryList = new List<string>();

                foreach (var existingdb in countryByStatus)
                {
                    CountryList.Add(existingdb.CountryName);
                    selectList.Add(
                       new SelectListItem
                       {
                           Value = Convert.ToString(existingdb.Id),
                           Text = existingdb.CountryName
                       });
                }
                selectList.OrderBy(x => x.Text);
                IEnumerable<SelectListItem> country = new SelectList(selectList, "Value", "Text");
                ViewBag.CountryList = country;
            }
            return selectList;
        }

       
        /// <summary>
        /// To populate the user grid and the inactive user grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]


        public async Task<ActionResult> UserRead([DataSourceRequest] DataSourceRequest request)

        {
            DataSourceResult result = null;

            //  var userdata = AsyncUtilHelper.AsyncUtil.RunSync(() => userRepository.GetDatas());
            var getAllData = await this.GetDataBaseFunction<User>(request, this.userRepository);
            getAllData = getAllData.Where(x => !x.IsDelete);
            if (Convert.ToString(Session["User"]) == "UserIndex")
            {
                if (getAllData != null)
                {
                    var notApproved = (from data in getAllData
                                       where DateTime.Now.Date.Subtract(data.CreationDate).Days > 30 && data.Status == 0
                                       select data ).ToList();
                   
                    if (notApproved != null)
                    {
                        foreach (User data in notApproved)
                        {
                            DeleteNotApprovedUser(data);
                        }
                    }
                  
                }
                var getUsers = await this.GetDataBaseFunction<User>(request, this.userRepository);
                getUsers = getUsers.Where(x => !x.IsDelete);
                result = getUsers.ToDataSourceResult(request);
            }
            else
            {
                result = (from data in getAllData
                          where DateTime.Now.Date.Subtract(data.LastLoginDate).Days > 400 && data.Status > 0
                          select new
                          {
                              data.Id,
                              data.FirstName,
                              data.LastName,
                              data.Username,
                              data.EmailAddress,
                              data.LastLoginDate,
                              data.CreationDate,
                              data.MarketingPerson,
                              data.CountryId,
                              data.Status,
                              data.Blocked,
                              data.StatusDate
                          }).ToDataSourceResult(request);

            }//Inactive Users

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public async void DeleteNotApprovedUser(User user)
        {
            try
            {

                if (user != null)
                    await userRepository.DeleteByIdAsync(user);
            }
            catch (Exception ex)
            {

            }
        }

        public ActionResult Edit()
        {
            return View();
        }

        #endregion 
        #region Country
        /// <summary>
        /// To load country view
        /// </summary>
        /// <returns></returns>
        public ActionResult CountryIndex()
        {
            MvcApplication.Logger.Info("Country Details");
            var countryViewModel = new CountryViewModel();
            return View(countryViewModel);

        }
        /// <summary>
        /// To populate the Country grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> CountryRead([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                // var countrydata = await countryRepository.GetDatas();
                var getAllData = await this.GetDataBaseFunction<Country>(request, this.countryRepository);
                var result = getAllData.ToDataSourceResult(request);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw exception;
            }
        }
        /// <summary>
        /// To populate the country combobox in country 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<JsonResult> ComboRead([DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<Country> countries = await countryRepository.GetDatas();

            var countryList = new List<string>();
            var selectList = new List<SelectListItem>();

            foreach (Country existingDb in countries)
            {
                countryList.Add(existingDb.CountryName);
                selectList.Add(
                   new SelectListItem
                   {
                       Value = Convert.ToString(existingDb.CountryName),
                       Text = existingDb.CountryName
                   });
            }

            CultureInfo[] cultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in cultureInfo)
            {
                var regionInfo = new RegionInfo(CInfo.LCID);
                if (!(countryList.Contains(regionInfo.EnglishName)))
                {
                    countryList.Add(regionInfo.EnglishName);
                    selectList.Add(
                   new SelectListItem
                   {
                       Value = regionInfo.EnglishName,
                       Text = regionInfo.EnglishName
                   });
                }
            }

            selectList.OrderBy(x => x.Text);
            IEnumerable<SelectListItem> countriesList = new SelectList(selectList, "Value", "Text").OrderBy(x => x.Text);
            return Json(countriesList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To add new country
        /// </summary>
        /// <param name="countryViewModel"></param>
        /// <returns></returns>
        public async Task<ActionResult> CountryAdd(CountryViewModel countryViewModel)
        {
            try { 
            countryViewModel.Country.Status = false;
            if (countryViewModel == null)
                throw new ArgumentNullException(nameof(countryViewModel));
            try
            {
                await AddValue(countryRepository, countryViewModel.Country);


            }
            catch (SqlException exception)
            {
                Message = exception.Message.Contains("CountryDuplicateKey") ? "Country already exists" : "Failed";
                IsSuccess = false;
            }

            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                IsSuccess = false;
            }
            return Json(new { message =this.Message, status = IsSuccess , validationErrors = this.ModelStateErrors });
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }
        /// <summary>
        /// To update existing country
        /// </summary>
        /// <param name="request"></param>
        /// <param name="country"></param>
        /// <param name="textParam"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CountryUpdate([DataSourceRequest] DataSourceRequest request, Country country, string textParam)
        {
            try
            {
                if (country != null && ModelState.IsValid)
                {
                    await countryRepository.UpdateById(country, textParam);
                }

                return Json(new[] { country }.ToDataSourceResult(request, ModelState));
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }
        /// <summary>
        /// To delete country
        /// </summary>
        /// <param name="request"></param>
        /// <param name="country"></param>
        /// <returns></returns>

        #endregion

        #region Contact
        public async Task<ActionResult> ContactIndex()
        {
            Contact data = new Contact();
            try
            {
                var contact = await contactRepository.GetDatas();
                
                return View("ContactIndex", contact.FirstOrDefault());
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddContact(Contact contact)
        {
            try
            {
                IsSuccess = false;
                string msg = string.Empty;

                if (contact != null)
                {
                    var currentContact = await contactRepository.GetDatas();

                    if (currentContact.Count() == 0)
                    {
                        var status = await contactRepository.InsertById(contact);
                        IsSuccess = true;
                        Message = status == 1 ? "Added Succesfully" : "Added Failed";
                    }
                    else
                    {
                        if (contact.Email == currentContact.FirstOrDefault().Email&& contact.Phone == currentContact.FirstOrDefault().Phone)
                        {
                            Message = "This Contact data is same as old data";
                        }
                        else
                        {
                            contact.Id = currentContact.FirstOrDefault().Id;
                            var status = await contactRepository.UpdateById(contact);
                            IsSuccess = true;
                            Message = status == 1 ? "Updated Succesfully" : "Update Failed";
                        }
                        
                    }
                  
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false ;
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
            return Json(new { message = this.Message, status = IsSuccess, validationErrors = this.ModelStateErrors });
        }

        //[HttpPost]
        //public async Task<ActionResult> UpdateContact(Contact contact)
        //{
        //    try
        //    {
        //        if (contact != null)
        //        {
        //            var status = await contactRepository.UpdateById(contact);
        //            return Json(status);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MvcApplication.Logger.Error(ex.Message, ex);
        //        throw ex;
        //    }
        //    return null;
        //}
        #endregion

        #region DashBoard
        public async Task<JsonResult> GetDashBoardValues()
        {
            IDashBoardService dashBoardService = new DashBoardService();
            try
            {

                var userData = await userRepository.GetDatas();
                
                var getRings = await ringRepository.GetDatas();
                var getStructs = await strutRepository.GetDatas();
                var getCountries = await countryRepository.GetDatas();
                var ringCount = getRings.Count();
                var structCount = getStructs.Count();
                var countryCount = getCountries.Where(x => x.Status == true).Count();
                var userCounts = dashBoardService.GetDashBoradCounts(userData, ringCount, structCount, countryCount);
                return Json(userCounts, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }
        #endregion

    }

}
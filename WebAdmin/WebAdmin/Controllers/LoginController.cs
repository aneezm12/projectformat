﻿using MvcSiteMapProvider.Web.Mvc.Filters;
using System.Web.Mvc;

namespace WebAdmin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
          
        }

        [SiteMapCacheRelease]
        public  ActionResult LoginValidate(LoginUser loginuser)
        {
            if (ModelState.IsValid)
            {
                //bool isAdminAuthorized = true;
                //if (isAdminAuthorized)
                //{
                    Session["UserId"] = loginuser.UserName;
                Session["UserRole"] = loginuser.Roles_Login.ToString();
           
                    return RedirectToAction("Index", "User");

                    //   return View("Index", new UserViewModel());
                }
                else
                {
                    return View("Index", loginuser);
                }           
           
        }
    }
}
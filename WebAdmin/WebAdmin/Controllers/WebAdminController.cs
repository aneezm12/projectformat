﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin
{
    public abstract class WebAdminController : Controller
    {
        // GET: WebAdmin
        protected IList<object> ModelStateErrors { get; set; }
        protected string Message { get; set; }
        protected bool IsSuccess { get; set; }

        public ActionResult Error()
        {
            return View();
        }
        /// <summary>
        /// Base class function to add data to the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataRepository"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task AddValue<T>(IDataRepository<T> dataRepository, T data)where T:class, new()
        {
            IsSuccess = false;
          
                if (ModelState.IsValid)
                {
                    int insert = await dataRepository.InsertById(data);
                    Message = insert == 1 ? "Added Successfully" : "Failed";
                    IsSuccess = insert == 1;

                }
                else
                {
                    ModelStateErrors = DataHelper.ModelStateErrors(ModelState);
                    IsSuccess = false;
                }
          
        }
        /// <summary>
        /// Base class function to delete value from the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataRepository"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task DeleteValue<T>(IDataRepository<T> dataRepository, T data) where T : class, new()
        {
            
           
                if (data == null)
                    throw new ArgumentNullException(nameof(data));
               
                await dataRepository.DeleteByIdAsync(data);
                IsSuccess = true;
                Message = "Deleted";
           
        }

        /// <summary>
        /// Get all the data from the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="dataRepository"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetDataBaseFunction<T>(DataSourceRequest request, IDataRepository<T> dataRepository) where T : class, new()

        {
            try
            {
                if (dataRepository == null)
                    throw new ArgumentNullException(nameof(dataRepository));
                IEnumerable<T> tkDatas = await dataRepository.GetDatas();
                return tkDatas;//.ToDataSourceResult(request);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }
        /// <summary>
        /// To handle database related exception 
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
        }
        /// <summary>
        /// To handle page not found error
        /// </summary>
        /// <returns></returns>
        protected ViewResult PageNotFound()
        {
            Response.StatusCode = 404;
            return View("PageNotFound");
        }
    }
}
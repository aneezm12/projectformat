﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebAdmin.Models;

using WebAdmin.Services;


namespace WebAdmin.Controllers
{
 //   [PasswordAttribute]
    public class PasswordController : Controller
    {
        // GET: Password
        private readonly IDataRepository<Password> pswdRepository;


        

        public PasswordController(IDataRepository<Password> pswdRepository)
        {
            this.pswdRepository = pswdRepository;
        }

        // GET: Image Resolution Details
        public async Task<ActionResult> Index()
        {
          try
            { 
            IEnumerable<Password> passwordlst = await pswdRepository.GetDatas();
            if (passwordlst == null)
                throw new ArgumentNullException();

            var singlePassword = passwordlst.FirstOrDefault();

           

            //WebAdmin.Models.Password pModel = new WebAdmin.Models.Password();
            //PasswordViewModel pModel = new PasswordViewModel();
                    
            //    pModel.Id = singlePassword.Id;


            //pModel.Pswd=
           // ModelState.Clear();
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        [HttpPost]
        public ActionResult Compare()
        {
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> update(Password model)
        {
            int update = 0;
            string msg = "";
            bool errorStatus = false;
            try
            { 
            if (ModelState.IsValid)
            {
                var pswdmodel = new Password
                {
                    Id=model.Id,
                    Pswd = model.Pswd

                };
            
                update = await pswdRepository.UpdateById(pswdmodel);
                if (update == 1)
                {
                    msg = "Updated Successfully.";
                    errorStatus = false;
                   
                   // ViewBag.Message(msg);
                }
                else
                {
                    msg = "Updation Failed.";
                    errorStatus = true;
                   // ViewBag.Message(msg);
                }
             
            }
           
            
            return Json(new { message = msg, status = errorStatus ,model=model});
                //  return Json(new { status = errorStatus });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace WebAdmin
{
    public class MessageController : WebAdminController
    {
        private readonly IDataRepository<Message> messageRepository;
        private readonly IDataRepository<Language> languageRepository;
        private readonly IDataRepository<EventMessage> eventMessageRepository;
        private readonly ICultureCodeService cultureCodeService;


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageController"/> class.
        /// </summary>
        /// <param name="messageRepository">
        /// The message repository.
        /// </param>
        /// <param name="eventMessageRepository">
        /// The event message repository.
        /// </param>
        /// <param name="languageRepository">
        /// The language repository.
        /// </param>
        ///  /// <param name="cultureCodeService">
        /// The cultureCodeService.
        /// </param>
        public MessageController(IDataRepository<Message> messageRepository,
                                 IDataRepository<EventMessage> eventMessageRepository,
                                 IDataRepository<Language> languageRepository,
                                 ICultureCodeService cultureCodeService)
        {
            this.messageRepository = messageRepository;
            this.eventMessageRepository = eventMessageRepository;
            this.languageRepository = languageRepository;
            this.cultureCodeService = cultureCodeService;


        }
        /// <summary>
        /// To View Message Details and Populate Language ComboBox
        /// </summary>
        /// <returns>ActionResult</returns>
        public async Task<ActionResult> MessageIndex()
        {
            var messageViewModel = new MessageViewModel();

            try
            {
                var languageSelectList = new List<SelectListItem>();
                IEnumerable<Language> language = await languageRepository.GetDatas();
                IEnumerable<Language> enumerable = language.ToList();

                if (enumerable.Any())
                {
                    foreach (Language data in enumerable)
                    {
                        if (Convert.ToString(data.Enabled) == "True")
                        {
                            languageSelectList.Add(
                                new SelectListItem
                                {
                                    Value = data.Id.ToString(),
                                    Text = CreateLanguageWithCultureCode(data)
                                });
                        }
                    }
                    ViewBag.languages = languageSelectList.OrderBy(x => x.Text);

                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);

            }

            return View(messageViewModel);
        }



        /// <summary>
        /// To View Event Message Details and populate Message and TimeZone ComboBox
        /// </summary>
        /// <returns>ActionResult</returns>
        public async Task<ActionResult> EventMessageIndex()
        {
            var eventMsgViewModel = new EventMessageViewModel();
            var messageSelectList = new List<SelectListItem>();
            ViewData["MessageId"] = messageSelectList;
            try
            {


                IEnumerable<Message> messages = await messageRepository.GetDatas();
                IEnumerable<Language> languages = await languageRepository.GetDatas();

                string[] requestUserLanguages = Request.UserLanguages;

                if (requestUserLanguages != null)
                {
                    string userLanguage = Convert.ToString(Convert.ToString(requestUserLanguages.First().Split('-').First()));

                    IEnumerable<Language> languageLst = languages.ToList();

                    IEnumerable<Message> messageLst = messages.ToList();

                    if (languageLst.Any() && messageLst.Any())
                    {
                        ViewData["MessageId"] = MessageHelper.GetMessagesList(languageLst, messageLst, userLanguage).OrderBy(x => x.Text);
                        var timezoneSelectList = new List<SelectListItem>();
                        var timeZone = TimeZoneInfo.GetSystemTimeZones();
                        foreach (TimeZoneInfo data in timeZone)
                        {
                            timezoneSelectList.Add(
                                new SelectListItem
                                {
                                    Value = data.DisplayName,
                                    Text = data.DisplayName
                                });

                        }
                        // timezoneSelectList.OrderBy(x => x.Text);
                        ViewBag.timezone = timezoneSelectList.OrderBy(x => x.Text);
                    }
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);

            }


            return View(eventMsgViewModel);
        }




        /// <summary>
        /// To populate grid with Message details
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public async Task<ActionResult> MessageRead([DataSourceRequest] DataSourceRequest request)
        {
            try
            {

                IEnumerable<Message> messagesLst = await this.GetDataBaseFunction<Message>(request, this.messageRepository);

                IEnumerable<Message> enumerable = messagesLst.ToList();

                foreach (Message message in enumerable)
                {

                    message.Description = Regex.Replace(message.Description, "<startdate>", "&lt;Startdate&gt;", RegexOptions.IgnoreCase);
                    message.Description = Regex.Replace(message.Description, "<starttime>", "&lt;Starttime&gt;", RegexOptions.IgnoreCase);
                    message.Description = Regex.Replace(message.Description, "<enddate>", "&lt;Enddate&gt;", RegexOptions.IgnoreCase);
                    message.Description = Regex.Replace(message.Description, "<endtime>", "&lt;Endtime&gt;", RegexOptions.IgnoreCase);

                    message.Description = message.Description.Replace("?", " ");
                }
                DataSourceResult result = enumerable.ToDataSourceResult(request);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }



        /// <summary>
        /// To get Message description of a message in a grid edit popup window
        /// </summary>
        /// <param name="messageId">messageId</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult GetMessageDescriptionById(string messageId)
        {
            string description = string.Empty;
            IEnumerable<Message> getAll = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            Message getById = getAll.Where(x => x.MessageId == Guid.Parse(messageId)).FirstOrDefault();
            if (getById != null)
            {
                description = getById.Description;
            }
            description = Regex.Replace(description, "<startdate>", "&lt;Startdate&gt;", RegexOptions.IgnoreCase);
            description = Regex.Replace(description, "<Enddate>", "&lt;Enddate&gt;", RegexOptions.IgnoreCase);
            description = Regex.Replace(description, "<Starttime>", "&lt;Starttime&gt;", RegexOptions.IgnoreCase);
            description = Regex.Replace(description, "<Endtime>", "&lt;Endtime&gt;", RegexOptions.IgnoreCase);


            description = description.Replace("?", " ");
            return Json(description);
        }

        /// <summary>
        ///  To populate grid with event message details
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>ActionResult</returns>

        [HttpPost]
        public async Task<ActionResult> EventMessageRead([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var eventMessageCollection = new List<EventMessage>();
                IEnumerable<EventMessage> getAllEventMessages = await this.GetDataBaseFunction<EventMessage>(request, eventMessageRepository);

                IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
                IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());
                // ReSharper disable once StyleCop.SA1305
                var isAdded = false;
                if (Request.Cookies["TimeZoneClient"] != null)
                {
                    string timezone = Convert.ToString(this.Request.Cookies["TimeZoneClient"].Value);
                    string[] requestUserLanguages = this.Request.UserLanguages;
                    if (requestUserLanguages != null)
                    {
                        var defaultCode = "";
                        string defaultLanguage = Convert.ToString(WebConfigurationManager.AppSettings["DefaultLanguage"]);
                        foreach (LanguageName language in Enum.GetValues(typeof(LanguageName)))
                        {
                            bool matchLanguage = Regex.IsMatch(defaultLanguage, Regex.Escape(Convert.ToString(language)), RegexOptions.IgnoreCase);
                            if (matchLanguage)
                            {
                                var cultures = cultureCodeService.GetCultureCodeList(language);
                                defaultCode = Convert.ToString(cultures.Values.First().First()).Split('-').First();
                            }

                        }

                        List<Guid> languages = (from data in getAllLanguage
                                                where data.CultureCode.Split('-').First().Equals(defaultCode)
                                                // where data.Name == WebConfigurationManager.AppSettings["DefaultLanguage"]
                                                // where data.CultureCode.Equals(WebConfigurationManager.AppSettings["DefaultLanguage"], StringComparison.OrdinalIgnoreCase)

                                                // || data.CultureCode.Equals(DataHelper.GetLanguageName(userLanguage), StringComparison.OrdinalIgnoreCase)
                                                select data.Id).ToList();
                        foreach (Guid language in languages)
                        {
                            if (getAllMessages != null)
                            {
                                List<Guid> messages = (from data in getAllMessages
                                                       where data.LanguageId == language
                                                       select data.MessageId).ToList();
                                foreach (Guid message in messages)
                                {
                                    List<EventMessage> eventMessagesList = (from data in getAllEventMessages
                                                                            where data.MessageId == message
                                                                            select data).ToList();
                                    foreach (EventMessage eventData in eventMessagesList)
                                    {
                                        eventData.StartDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventData.StartDate), MessageHelper.OlsonTimeZoneToTimeZoneInfo(timezone));
                                        eventData.EndDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventData.EndDate), MessageHelper.OlsonTimeZoneToTimeZoneInfo(timezone));
                                        eventData.EventStart = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventData.EventStart), MessageHelper.OlsonTimeZoneToTimeZoneInfo(timezone));
                                        eventData.EventEnd = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(eventData.EventEnd), MessageHelper.OlsonTimeZoneToTimeZoneInfo(timezone));
                                        isAdded = true;
                                    }
                                    if (isAdded)
                                    {
                                        eventMessageCollection.AddRange(eventMessagesList);
                                        isAdded = false;
                                    }
                                }
                            }

                        }
                    }

                }
                DataSourceResult result = eventMessageCollection.ToDataSourceResult(request);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }


        /// <summary>
        /// To populate Message ComboBox in Message  
        /// </summary>
        /// <returns>JSON</returns>
        public async Task<JsonResult> MessageComboRead()
        {
            IOrderedEnumerable<SelectListItem> messagesLst;
            List<SelectListItem> messageSelectList;
            IEnumerable<Message> messages = await messageRepository.GetDatas();
            messageSelectList = MessageHelper.GetMessages(messages);
            messagesLst = messageSelectList.OrderBy(x => x.Text);
            ViewData["Messages"] = messagesLst;
            return Json(messagesLst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To populate Message ComboBox in EventMessage
        /// </summary>
        /// <returns>JSON</returns>
        public async Task<JsonResult> EventMessageComboRead()
        {
            IOrderedEnumerable<SelectListItem> messagesLst;
            List<SelectListItem> messageSelectList;
            IEnumerable<Message> messages = await messageRepository.GetDatas();
            MvcApplication.Logger.Error("messages list" + messages.Count());
            IEnumerable<Language> languages = await languageRepository.GetDatas();
            string userLanguage = Convert.ToString(Convert.ToString(Request?.UserLanguages?.First().Split('-').First()));
            messageSelectList = MessageHelper.GetMessagesList(languages, messages, userLanguage);

            messagesLst = messageSelectList.OrderBy(x => x.Text);
            ViewData["Messages"] = messagesLst;
            return Json(messagesLst, JsonRequestBehavior.AllowGet);
        }






        /// <summary>
        /// To add a new message
        /// </summary>
        /// <param name="messageViewModel">messageViewModel</param>
        /// <returns>ActionResult</returns>
        public async Task<ActionResult> MessageDetailsAdd(MessageViewModel messageViewModel)
        {


            if (messageViewModel == null)
                throw new ArgumentNullException(nameof(messageViewModel));
            if (ModelState.IsValid && messageViewModel.MessageDetails != null)
            {
                string startTag = messageViewModel.MessageDetails.Description.Replace("&amp;lt;", "<");
                string endTag = startTag.Replace("&amp;gt;", ">");

                string tempHtml = HttpUtility.HtmlDecode(endTag);
                messageViewModel.MessageDetails.Description = tempHtml;


                try
                {
                    IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
                    IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());

                    List<string> languageByTitle = (from msg in getAllMessages
                                                    join lang in getAllLanguage
                                                    on msg.LanguageId equals lang.Id
                                                    where msg.MessageTitle == messageViewModel.MessageDetails.MessageTitle
                                                    select lang.CultureCode).ToList();

                    foreach (string data in languageByTitle)
                    {
                        string language = Convert.ToString(Convert.ToString(data.Split('-').First()));
                        List<string> currentCulture = (from current in getAllLanguage
                                                       where current.Id == messageViewModel.MessageDetails.LanguageId
                                                       select current.CultureCode).ToList();
                        string currentLanguage = Convert.ToString(currentCulture.First()).Split('-').First();
                        if (currentLanguage == language)
                        {
                            Message = "Message already exists in this language ";
                            IsSuccess = false;
                            return Json(new { message = Message, status = IsSuccess });
                        }

                    }

                    await AddValue(messageRepository, messageViewModel.MessageDetails);
                }
                catch (Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                }

                return Json(new { message = Message, status = IsSuccess });
            }
            else
            {
                IList<object> modelStateErrors = DataHelper.ModelStateErrors(ModelState);
                return Json(new { message = this.Message, status = IsSuccess, validationErrors = modelStateErrors });
            }
        }
        /// <summary>
        /// To add new event message
        /// </summary>
        /// <param name="eventMessageViewModel">eventMessageViewModel</param>
        /// <returns>ActionResult</returns>
        public async Task<ActionResult> EventMessageAdd(EventMessageViewModel eventMessageViewModel)
        {


            try
            {
                if (ModelState.IsValid)
                {
                    IEnumerable<EventMessage> getAllEvents = AsyncUtilHelper.AsyncUtil.RunSync(() => eventMessageRepository.GetDatas());
                    IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());

                    List<string> messageTitle = (from data in getAllMessages
                                                 where data.MessageId == eventMessageViewModel.EventMessage.MessageId
                                                 select data.MessageTitle).ToList();

                    List<Guid> messageIdList = (from data in getAllMessages
                                                where data.MessageTitle == messageTitle.First()
                                                select data.MessageId).ToList();

                    foreach (Guid messageId in messageIdList)
                    {
                        foreach (EventMessage data in getAllEvents)
                        {

                            DateTime modelEventDate = eventMessageViewModel.EventMessage.EventStart.ToUniversalTime();
                            if (messageId == data.MessageId && data.EventStart.Date == modelEventDate.Date)
                            {

                                Message = "This Event is Already Exists in Same Date";
                                IsSuccess = false;
                                return Json(new { message = Message, status = IsSuccess });
                            }
                        }

                    }


                    DateTime startDate = eventMessageViewModel.EventMessage.StartDate;
                    DateTime utcStartDate = startDate.ToUniversalTime();
                    eventMessageViewModel.EventMessage.StartDate = utcStartDate;
                    DateTime endDate = eventMessageViewModel.EventMessage.EndDate;
                    DateTime utcEndDate = endDate.ToUniversalTime();
                    eventMessageViewModel.EventMessage.EndDate = utcEndDate;
                    DateTime eventStart = eventMessageViewModel.EventMessage.EventStart;
                    DateTime utcEventStart = eventStart.ToUniversalTime();
                    eventMessageViewModel.EventMessage.EventStart = utcEventStart;
                    DateTime eventEnd = eventMessageViewModel.EventMessage.EventEnd;
                    DateTime utcEventEnd = eventEnd.ToUniversalTime();
                    eventMessageViewModel.EventMessage.EventEnd = utcEventEnd;
                    await AddValue(eventMessageRepository, eventMessageViewModel.EventMessage);

                    return Json(new { message = this.Message, status = IsSuccess });

                }
                else
                {
                    IList<object> modelStateErrors = DataHelper.ModelStateErrors(ModelState);
                    return Json(new { message = this.Message, status = IsSuccess, validationErrors = modelStateErrors });
                }


            }

            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw;
            }
        }

        /// <summary>
        /// To update existing message
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="textParam">textParam</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public async Task<ActionResult> MessageUpdate(Message message, string textParam)
        {
            var update = 0;
            try
            {
                string startTag = message.Description.Replace("&amp;lt;", "<");
                string endTag = startTag.Replace("&amp;gt;", ">");
                string tempHtml = HttpUtility.HtmlDecode(endTag);
                message.Description = tempHtml;
                if (ModelState.IsValid)
                {
                    try
                    {
                        update = await messageRepository.UpdateById(message, textParam);
                    }
                    catch (Exception exception)
                    {
                        MvcApplication.Logger.Error(exception.Message, exception);
                        throw;
                    }

                }


                return Json(new { status = update == 1, gridName = "Message" });

            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw;
            }
        }


        /// <summary>
        /// To update existing event message
        /// </summary>

        /// <param name="eventMessage">eventMessage</param>
        /// <param name="textParam">textParam</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public async Task<ActionResult> EventMessageUpdate(EventMessage eventMessage, string textParam)
        {
            var update = 0;
            try
            {


                if (eventMessage != null && ModelState.IsValid)
                {


                    DateTime startDate = eventMessage.StartDate;
                    DateTime utcStartDate = startDate.ToUniversalTime();
                    eventMessage.StartDate = utcStartDate;

                    DateTime endDate = eventMessage.EndDate;
                    DateTime utcEndDate = endDate.ToUniversalTime();
                    eventMessage.EndDate = utcEndDate;

                    DateTime eventStart = eventMessage.EventStart;
                    DateTime utcEventStart = eventStart.ToUniversalTime();
                    eventMessage.EventStart = utcEventStart;

                    DateTime eventEnd = eventMessage.EventEnd;
                    DateTime utcEventEnd = eventEnd.ToUniversalTime();
                    eventMessage.EventEnd = utcEventEnd;
                    update = await eventMessageRepository.UpdateById(eventMessage, textParam);
                }
                return Json(new { status = update == 1, gridName = "EventMessage" });
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }

        /// <summary>
        /// To check the duplication of a message in a same language
        /// </summary>
        /// <param name="language">language</param>
        /// <param name="title">title</param>
        /// <returns>integer</returns>
        public int CheckExistence(Guid language, string title)
        {

            var count = 0;

            IEnumerable<Message> getAllMessages = AsyncUtilHelper.AsyncUtil.RunSync(() => messageRepository.GetDatas());
            IEnumerable<Language> getAllLanguage = AsyncUtilHelper.AsyncUtil.RunSync(() => languageRepository.GetDatas());

            List<string> languageByTitle = (from msg in getAllMessages
                                            join lang in getAllLanguage
                                                on msg.LanguageId equals lang.Id
                                            where msg.MessageTitle == title
                                            select lang.CultureCode).ToList();

            foreach (string data in languageByTitle)
            {
                string cultureLanguage = Convert.ToString(Convert.ToString(data.Split('-').First()));
                List<string> currentCulture = (from current in getAllLanguage
                                               where current.Id == language
                                               select current.CultureCode).ToList();
                string currentLanguage = Convert.ToString(currentCulture.First()).Split('-').First();
                if (DataHelper.GetLanguageName(currentLanguage) == DataHelper.GetLanguageName(cultureLanguage))
                {
                    count = 1;
                    return count;
                }
            }

            return count;
        }




        /// <summary>
        /// To populate language combo box in Message
        /// </summary>
        /// <returns>JSON</returns>
        public async Task<JsonResult> LanguageComboRead()
        {
            var languageSelectList = new List<SelectListItem>();
            try
            {
                IEnumerable<Language> lang = await languageRepository.GetDatas();

                foreach (Language data in lang)
                {
                    if (Convert.ToString(data.Enabled) == "True")
                    {
                        languageSelectList.Add(
                            new SelectListItem
                            {
                                Value = data.Id.ToString(),
                                Text = CreateLanguageWithCultureCode(data)
                            });
                    }
                }
                return Json(languageSelectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }

        /// <summary>
        /// To populate TimeZone Combo box in EventMessage
        /// </summary>
        /// <returns>JSON</returns>
        public JsonResult GetTimezone([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                var timezoneSelectList = new List<SelectListItem>();
                // ReadOnlyCollection<TimeZoneInfo> timeZone = TimeZoneInfo.GetSystemTimeZones();
                var timeZone = TimeZoneInfo.GetSystemTimeZones();

                foreach (var data in timeZone)
                {
                    timezoneSelectList.Add(
                        new SelectListItem
                        {
                            Value = data.DisplayName,
                            Text = data.DisplayName
                        });

                }

                ViewBag.timezone = timezoneSelectList.OrderBy(x => x.Text);
                IEnumerable<SelectListItem> timezoneList = new SelectList(timezoneSelectList, "Value", "Text").OrderBy(x => x.Text);
                return Json(timezoneList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw;
            }
        }
        /// <summary>
        /// To delete a Message from the  grid
        /// </summary>
        /// <param name="data">data</param>
        /// <returns>JSON</returns>
        [HttpPost]
        public async Task<JsonResult> MessageDelete(Message data)
        {


            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data.MessageId)))
                {
                    var message = new Message { MessageId = data.MessageId };
                    await DeleteValue(messageRepository, message);
                    MvcApplication.Logger.Info("Message deleted");

                }
            }
            catch (Exception exception)
            {
                IsSuccess = false;
                Message = "Deletion Failed !";
                MvcApplication.Logger.Error(exception);
            }
            return Json(new { message = Message, status = IsSuccess });
        }

        /// <summary>
        /// Delete the event message settings
        /// </summary>
        /// <param name="data">data</param>
        /// <returns>JSON</returns>
        [HttpPost]
        public async Task<JsonResult> EventMessageDelete(EventMessage data)
        {


            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data.Id)))
                {
                    var eventMessage = new EventMessage { Id = data.Id };
                    await DeleteValue(eventMessageRepository, eventMessage);
                    MvcApplication.Logger.Info("EventMessage deleted");

                }
            }
            catch (Exception exception)
            {
                IsSuccess = false;
                Message = "Deletion failed !";
                MvcApplication.Logger.Error(exception);
            }

            return Json(new { message = Message, status = IsSuccess });
        }


        private static string CreateLanguageWithCultureCode(Language data)
        {
            return data.CultureCode + "(" + data.Name + ")";
        }



    }

}

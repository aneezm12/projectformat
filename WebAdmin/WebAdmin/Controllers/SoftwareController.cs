﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;

namespace WebAdmin
{

    public sealed class SoftwareController : WebAdminController
    {

        private readonly IDataRepository<BlockIpAddress> ipAddressRepository;
        private readonly IDataRepository<BrowserDetails> browserRepository;
        private readonly IGridViewModel<Software> softwareGridViewModel;
        private readonly ISoftwareService softwareService;

        public SoftwareController(IDataRepository<BlockIpAddress> ipAddressRepository, IDataRepository<BrowserDetails> browserRepository, IGridViewModel<Software> softwareGridViewModel, ISoftwareService softwareService)
        {
            this.ipAddressRepository = ipAddressRepository;
            this.browserRepository = browserRepository;
            this.softwareGridViewModel = softwareGridViewModel;
            this.softwareService = softwareService;
        }
        /// <summary>
        /// To load software view
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            MvcApplication.Logger.Info("Software Details");
            return View(softwareGridViewModel);
        }
        /// <summary>
        /// To load BlockIPaddress view
        /// </summary>
        /// <returns></returns>
        public ActionResult BlockIpAddressIndex()
        {
            MvcApplication.Logger.Info("Block IP Details");
            var ipViewModel = new BlockIpAddressViewModel();
            return View(ipViewModel);
        }
        /// <summary>
        /// To load Browser view
        /// </summary>
        /// <returns></returns>
        public ActionResult BrowserIndex()
        {
            MvcApplication.Logger.Info("Browser Details");
            var browserViewModel = new BrowserViewModel();
            return View(browserViewModel);
        }
        /// <summary>
        /// To populate blockIPaddress Grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> BlockIPAddressRead([DataSourceRequest] DataSourceRequest request)
        {
            var getAllData = await this.GetDataBaseFunction<BlockIpAddress>(request, ipAddressRepository);
            var result = getAllData.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To populate Browser grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> BrowserDetailsRead([DataSourceRequest]DataSourceRequest request)
        {
            var getAllData = await this.GetDataBaseFunction<BrowserDetails>(request, browserRepository);
            var result = getAllData.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// To add new Block IP address
        /// </summary>
        /// <param name="blockIpAddressViewModel"></param>
        /// <returns></returns>
        public async Task<ActionResult> BlockIPAddressAdd(BlockIpAddressViewModel blockIpAddressViewModel)
        {
           
            var isValidIp = false;

            if (blockIpAddressViewModel == null)
                throw new ArgumentNullException(nameof(blockIpAddressViewModel));


            IPAddress ipAddress;
            if (IPAddress.TryParse(blockIpAddressViewModel.IpAddress, out ipAddress))
            {
                switch (ipAddress.AddressFamily)
                {
                    case AddressFamily.InterNetwork:
                        //We have IPv4
                        Match match = Regex.Match(blockIpAddressViewModel.IpAddress, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
                        isValidIp = match.Success;
                        break;
                    case AddressFamily.InterNetworkV6:
                        //We have IPV6
                        isValidIp = true;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            if (isValidIp)
            {
                var blockIpAddress = new BlockIpAddress { IPaddress = blockIpAddressViewModel.IpAddress };
                try
                {
                    await AddValue(ipAddressRepository, blockIpAddress);
                }
                catch (SqlException exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                    Message = exception.Message.Contains("BlockIPDuplicateKey") ?
                            "IP Address Already Blocked" : "Failed";
                    IsSuccess = false;
                }
                catch (Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                    Message = exception.Message;
                    IsSuccess = false;
                    throw;
                }
            }
            else
            {
                Message = "Not a valid IPAddress";
            }


            return Json(new { message = this.Message, status = IsSuccess, validationErrors = this.ModelStateErrors });

        }
        /// <summary>
        /// To add new Browser
        /// </summary>
        /// <param name="browserViewModel"></param>
        /// <returns></returns>
        public async Task<ActionResult> BrowserAdd(BrowserViewModel browserViewModel)
        {
            string message = string.Empty;
            try
            {

                if (browserViewModel == null)
                    throw new ArgumentNullException(nameof(browserViewModel));


                try
                {
                    await AddValue(browserRepository, browserViewModel.BrowserDetails);
                }
                

                catch (SqlException exception)
                {
                    Message = exception.Message.Contains("BrowserDetailsDuplicateKey") ?
                            "Search Pattern Already Exists" : "Failed";
                   IsSuccess = false;
                }

                catch (Exception exception)
                {
                    MvcApplication.Logger.Error(exception.Message, exception);
                    Message = exception.Message;
                    IsSuccess = false;
                    throw;
                }

                return Json(new { message = this.Message, status = IsSuccess, validationErrors = this.ModelStateErrors });

            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }

        }
        /// <summary>
        /// To delete Block IP address
        /// </summary>
        /// <param name="request"></param>
        /// <param name="blockIpAddress"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> BlockIPAddressDelete(BlockIpAddress data)
        {
            bool isSuccess = false;
            var msg = "";
            try
            {
                if (!String.IsNullOrEmpty(data.Id.ToString()))
                {
                    BlockIpAddress blockIpAddress = new WebAdmin.BlockIpAddress();
                    blockIpAddress.Id = data.Id;
                    await DeleteValue(ipAddressRepository, blockIpAddress);

                    MvcApplication.Logger.Info("BlockIpAddress deleted");
                    isSuccess = true;
                    msg = "Deleted successfully !";
                }

            }
            catch (Exception ex)
            {
                isSuccess = false;
                msg = "Deleted failed !";
            }
            return Json(new { message = msg, status = isSuccess });
        }
        /// <summary>
        /// To delete browser details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="browserdetails"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<JsonResult> BrowserDetailsDelete(BrowserDetails data)
        {
            bool isSuccess = false;
            var msg = "";
            try
            {
                if (!String.IsNullOrEmpty(data.Id.ToString()))
                {
                    BrowserDetails browserdetails = new WebAdmin.BrowserDetails();
                    browserdetails.Id = data.Id;
                    await DeleteValue(browserRepository, browserdetails);

                    MvcApplication.Logger.Info("Browser deleted");
                    isSuccess = true;
                    msg = "Deleted successfully !";
                }

            }
            catch (Exception ex)
            {
                isSuccess = false;
                msg = "Deleted failed !";
            }
            return Json(new { message = msg, status = isSuccess });
        }
        /// <summary>
        /// To populate browser combobox in browser
        /// </summary>
        /// <returns></returns>
        public JsonResult ComboRead()
        {
            try
            {
                if (browserRepository == null)
                    throw new ArgumentNullException(nameof(browserRepository));
                var getAll = AsyncUtilHelper.AsyncUtil.RunSync(() => browserRepository.GetDatas());
                var getDistinctData = getAll.Select(b => b.BrowserName).Distinct().ToList();
                var data = new List<BrowserDetails>();
                var selectList = new List<SelectListItem>();

                foreach (var item in getDistinctData)
                {
                   selectList.Add(
                   new SelectListItem
                   {
                       Value = item,
                       Text = item
                   });
                }
                return Json(selectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw;
            }
        }

        [HttpPost]
        public JsonResult SoftwareRead([DataSourceRequest] DataSourceRequest request)
        {
            DataSourceResult result = null;
            try
            {
                string warning = string.Empty;


                List<Software> softwares = new List<Software>();
                string assemblyName = ConfigurationManager.AppSettings["AssemblyName"];

                var latestVer = softwareService.GetSoftwareVersion();
                softwares.AddRange(latestVer);
                var selectCommon = latestVer.Where(x => x.Name == "HexapodCommon").FirstOrDefault();
                var version = selectCommon.Version;



                var currentVersion = softwareService.GetAssemblyVersion();

                var webProjectVersion = softwareService.GetCurrentProjectVersion();
                softwares.AddRange(webProjectVersion);


                var compareResult = currentVersion.Where(x => x.Name == assemblyName).FirstOrDefault().Version;

                if (version != compareResult)
                {
                    warning = "HexapodCommon using different version";
                }
                else
                {
                    warning = null;
                }



                softwares[0].Messages = warning;
                result = softwares.ToDataSourceResult(request);


               
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
} 
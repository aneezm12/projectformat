[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebAdmin.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(WebAdmin.NinjectWebCommon), "Stop")]

namespace WebAdmin
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using WebAdmin.Models;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDatabaseService>().To<DatabaseService>().InSingletonScope();
            kernel.Bind(typeof(IDataRepository<>)).To(typeof(DataRepository<>)).InSingletonScope();
    
            kernel.Bind(typeof(IDataRepository<User>)).To(typeof(UserRepository));
            kernel.Bind(typeof(IDataRepository<Setting>)).To(typeof(SettingRepository));
            kernel.Bind(typeof(IDataRepository<BrowserDetails>)).To(typeof(BrowserRepository));
            kernel.Bind(typeof(IDataRepository<AuditLog>)).To(typeof(AuditLogRepository));
            kernel.Bind(typeof(IDataRepository<Language>)).To(typeof(LanguageRepository));
            kernel.Bind(typeof(IDataRepository<Ring>)).To(typeof(RingRepository));
            kernel.Bind(typeof(IDataRepository<Strut>)).To(typeof(StrutRepository));
            kernel.Bind(typeof(IDataRepository<Message>)).To(typeof(MessageRepository));
            kernel.Bind(typeof(IDataRepository<EventMessage>)).To(typeof(EventMessageRepository));
            kernel.Bind(typeof(IDataRepository<ConfigurationSetting>)).To(typeof(ConfigurationSettingRepository));
            kernel.Bind(typeof(IDataRepository<Country>)).To(typeof(CountryRepository));
            kernel.Bind(typeof(IDataRepository<BlockIpAddress>)).To(typeof(BlockIPRepository));
            kernel.Bind(typeof(IDataRepository<Contact>)).To(typeof(ContactRepository));
            kernel.Bind(typeof(IEmailService)).To(typeof(EmailService));
            kernel.Bind(typeof(ICultureCodeService)).To(typeof(CultureCodeService));
            kernel.Bind(typeof(ISoftwareService)).To(typeof(SoftwareService));


            kernel.Bind(typeof(IGridViewModel<>)).To(typeof(GridViewModel<>)).InSingletonScope();
        }        
    }
}
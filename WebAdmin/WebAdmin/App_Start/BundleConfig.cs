﻿using System.Web;
using System.Web.Optimization;

namespace WebAdmin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*Scripts Section - Start*/
            //layout
            bundles.Add(new ScriptBundle("~/Scripts/layout")
                .Include("~/Scripts/Js/jquery.min.js",
                "~/Scripts/Js/bootstrap.min.js",
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/jszip.min.js",
                "~/Scripts/Js/loader.js",
                "~/Scripts/Js/Grid.js",
                "~/Scripts/Js/service-wrap.js",
                "~/Scripts/Js/utility.js",
                "~/Scripts/Js/main.js",
                "~/Scripts/Js/custom-fun.js",
                "~/Scripts/Js/toastr.min.js",
                 "~/Scripts/Js/sunlight-min.js",
                "~/Scripts/Js/jquery.dialog.js"));

            bundles.Add(new ScriptBundle("~/Scripts/auditLog")
                .Include("~/Scripts/ViewScripts/AuditLog.js"));

            bundles.Add(new ScriptBundle("~/Scripts/eventMessage")
                .Include("~/Scripts/ViewScripts/EventMessages.js"));

            bundles.Add(new ScriptBundle("~/Scripts/message")
                .Include("~/Scripts/ViewScripts/Message.js"));

            bundles.Add(new ScriptBundle("~/Scripts/configurationSettings")
                .Include("~/Scripts/ViewScripts/ConfigurationSettings.js"));

            bundles.Add(new ScriptBundle("~/Scripts/gtin")
                .Include("~/Scripts/ViewScripts/GTIN.js"));

            bundles.Add(new ScriptBundle("~/Scripts/image")
                .Include("~/Scripts/ViewScripts/Image.js"));

            bundles.Add(new ScriptBundle("~/Scripts/mail")
                .Include("~/Scripts/ViewScripts/Mail.js"));

            bundles.Add(new ScriptBundle("~/Scripts/siteMapMenu")
                .Include("~/Scripts/ViewScripts/SiteMapMenu.js"));

            bundles.Add(new ScriptBundle("~/Scripts/eventMessageEditor")
                .Include("~/Scripts/ViewScripts/EventMessageEditor.js"));

            bundles.Add(new ScriptBundle("~/Scripts/messageEditor")
                .Include("~/Scripts/ViewScripts/MessageEditor.js"));

            bundles.Add(new ScriptBundle("~/Scripts/user")
               .Include("~/Scripts/ViewScripts/User.js"));

            bundles.Add(new ScriptBundle("~/Scripts/blockIPAddress")
               .Include("~/Scripts/ViewScripts/BlockIPAddress.js"));

            bundles.Add(new ScriptBundle("~/Scripts/browser")
               .Include("~/Scripts/ViewScripts/Browser.js"));

            bundles.Add(new ScriptBundle("~/Scripts/software")
               .Include("~/Scripts/ViewScripts/Software.js"));

            bundles.Add(new ScriptBundle("~/Scripts/country")
               .Include("~/Scripts/ViewScripts/Country.js"));

            /*Scripts Section - End*/

            /*Styles Section - Start*/
            //layout
            bundles.Add(new StyleBundle("~/Content/css/layout")
                .Include("~/Content/Css/bootstrap.min.css",
                "~/Content/kendo/kendo.common-bootstrap.min.css",
                "~/Content/kendo/kendoall.css",
                "~/Content/Css/font-awesome.min.css",
                "~/Content/Css/custom.min.css",
                "~/Content/Css/main.css",
                "~/Content/Css/custom-grid.css",
                "~/Content/Css/login.css",
                "~/Content/Css/toastr.css",
                "~/Content/Css/dialog.css",
                "~/Content/Css/sunlight.default.css"));

            bundles.Add(new StyleBundle("~/Content/css/error")
                .Include("~/Content/Css/bootstrap.min.css",
                "~/Content/Css/main.css"));

            bundles.Add(new StyleBundle("~/Content/css/login")
               .Include("~/Content/Css/bootstrap.min.css",
               "~/Content/Css/login.css"));

            /*Styles Section - End*/

            BundleTable.EnableOptimizations = false;

        }
    }
}

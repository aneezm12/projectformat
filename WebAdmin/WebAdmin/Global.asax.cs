﻿using log4net;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MvcApplication()
        {

        }

        protected void Application_Start()
        {
#if DEBUG
            log4net.Config.XmlConfigurator.Configure();

#else
            log4net.Config.BasicConfigurator.Configure();
#endif
            Logger.Info("Application Started");
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            AreaRegistration.RegisterAllAreas();
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
#if DEBUG
            //Prevent any client caching
            Response.AddHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AddHeader("Pragma", "no-cache");//HTTP 1.0
            Response.AddHeader("Expires","0");//Proxies
#endif
            //save the url and user agent for logging
            LogicalThreadContext.Properties["Url"] = Request.Url;
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            try
            {
                //save the session identifier for logging
                LogicalThreadContext.Properties["SessionID"] = Session.SessionID;
            }
            catch (HttpException)
            {
                LogicalThreadContext.Properties["SessionID"] = "";
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Error(exception.GetType().Name, exception);
                if (exception is HttpRequestValidationException)
                {
                    Server.Transfer("~/ErrorPages/500.aspx");
                }
            }

        }

        }
}

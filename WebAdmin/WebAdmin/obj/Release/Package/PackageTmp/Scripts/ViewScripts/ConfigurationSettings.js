﻿//tab - supported versions grid
$('#tbSupportrdVersion').click(function () {
    page_loader(true);
    $("#2a").load();
    page_loader(false);
});
//tab - add new
$('#tbSupportrdVersionGrid').click(function () {
    page_loader(true);
    $("#1a").load();
    page_loader(false);
});

//create new supported version
function createSupportedVersion(url) {
    serviceWrap.submitForm(url, function () {
        var grid = $("#ConfigurationSettingGrid").data("kendoGrid");
        grid.dataSource.read();
    });
}
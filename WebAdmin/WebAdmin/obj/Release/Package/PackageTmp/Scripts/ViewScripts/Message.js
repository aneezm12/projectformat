﻿//tab change - Add New Message
$('#tbNewMessage').click(function () {
    var comboMessageTitle = $("#MessageDetails_MessageTitle").data("kendoComboBox");
    comboMessageTitle.dataSource.read();
});
//get tool tip message
function getTooltipMessage(e) {
    var dataItem = $("#MessageGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
    var content = dataItem.Description;//descFunc(dataItem.Description, dataItem.StartDate, dataItem.EndDate, 'tooltip'); 
    return content;
}

//ready hook
$(document).ready(function () {
    $("#MessageDetails_MessageTitle").width(250).kendoComboBox();
});

//save message details
function saveMessage(url) {
    serviceWrap.submitForm(url, function () {
        var grid = $("#MessageGrid").data("kendoGrid");
        grid.dataSource.read();
        var comboMessageTitle = $("#MessageDetails_MessageTitle").data("kendoComboBox");
        comboMessageTitle.dataSource.read();
        comboMessageTitle.value("");
        var combolanguage = $("#MessageDetails_LanguageId").data("kendoComboBox");
        combolanguage.dataSource.read();
        combolanguage.value("");
    }); 
}
//on edit hook
function onEdit(e) {
    getDescription(e.model.MessageId);
}
//get description
function getDescription(messageId) {
    page_loader(true);
    $.ajax({
        url: 'Message/GetMessageDescriptionById',
        type: 'POST',
        dataType: "json",
        data: { MessageId: messageId },
        success: function (data) {
            var editor = $("#Description").data("kendoEditor");
            editor.value(data.description);
            page_loader(false);
        },
        error: function (error) {
            page_loader(false);
            alert('Sorry An Error Occured !');
        }
    });

}


//Set Tags
function setTags(tag) {
    var editor = $("#MessageDetails_Description").data("kendoEditor");
    var content = editor.value();
    switch (tag) {
        case "Startdate":
            if (!content.includes("&lt;Startdate&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Startdate" + "&gt;" });
            }
            break;
        case "Enddate":
            if (!content.includes("&lt;Enddate&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Enddate" + "&gt;" });
            }
            break;
        case "Starttime":
            if (!content.includes("&lt;Starttime&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Starttime" + "&gt;" });
            }
            break;
        case "Endtime":
            if (!content.includes("&lt;Endtime&gt;")) {
                editor.exec("inserthtml", { value: "&lt;" + "Endtime" + "&gt;" });
            }
            break;
        default: break;
    }
}


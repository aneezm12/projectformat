﻿//Fun to update Grid on "Filter" button Click

function updateGrid() {

    var datePicker = $("#daterangepicker").data("kendoDateRangePicker");
    var range = datePicker.range();
    var start = range.start;
    var end = range.end;
    var startObj = kendo.parseDate(start);
    var endObj = kendo.parseDate(end);
    var startString = kendo.toString(startObj, "yyyy-MM-dd");
    var endString = kendo.toString(endObj, "yyyy-MM-dd");
    $('#startDate').val(startString);
    $('#endDate').val(endString);
    var grid = $("#AuditLogGrid").data("kendoGrid");
    grid.dataSource.read({ startDate: startString, endDate: endString })
}

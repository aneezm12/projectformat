﻿//method for saving Block IP address.
function saveBlockIpAddress(url) {
        serviceWrap.submitForm(url, function () {
            var grid = $("#BlockIpAddressGrid").data("kendoGrid");
            grid.dataSource.read();
            $("#IpAddress").val("");
        });
        $("#IpAddress").val("");
}
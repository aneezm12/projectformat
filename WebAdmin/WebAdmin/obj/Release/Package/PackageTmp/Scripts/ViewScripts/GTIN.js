﻿//clear Gtin details
function clearGtin(url) {
    window.$.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        success: function (data) {
            window.$('#content-area').html(data);
            display_error("Loading old data", false);
        }
    })
}
//save Gtin details
function saveGTIN(url) {
    serviceWrap.submitForm(url, function () {
        $('#cancel').attr('disabled', true);
    });
}
﻿//tab - add new browser details
$('#tbNewBrowser').click(function () {
    var combo = $("#BrowserDetails_BrowserName").data("kendoComboBox");
    combo.dataSource.read();
});
//tab - browserdetails grid
$('#tbBrowserDetails').click(function () {
    page_loader(true);
    $("#1a").load();
    page_loader(false);
});

$(function () {
    $("#browser_form").kendoValidator();
});

//method to save browser details
function saveBrowser(url) {
    serviceWrap.submitForm(url, function () {
        var grid = $("#BrowserDetailsGrid").data("kendoGrid");
        grid.dataSource.read();
        var combo = $("#BrowserDetails_BrowserName").data("kendoComboBox");
        combo.dataSource.read();
        combo.value("");
        $('#BrowserDetails_SearchPattern').val('');
    });
}

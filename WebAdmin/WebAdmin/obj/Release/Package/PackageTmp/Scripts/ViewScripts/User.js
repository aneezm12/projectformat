﻿//user index
userRoles = {
    Marketing: "Marketing",
    Admin: "Admin",
    SuperAdmin: "SuperAdmin"
}

userPage = {
    setComponents: setComponents,
    dialogMail: null,
    messageCombo: null,
    globalStatus: undefined,
    messageContent: null,
    modal: null,
    editorWindow: null,
    userStatus: null,
    userRoles: userRoles,
    sessionUserRole: null,
    approvedButton: null,
    disapprovedButton: null,
    passwordButton: null,
    passwordButton: null,
    txtFirstName: null,
    txtLastName: null,
    txtEmailAddress: null,
    txtMarketingPerson: null,
    txtAreaComment: null,
    countryId: null,
    blocked: null,
    statusDate: null,
    statusBlock: null,
    requestEndHooked: false,
    displaystatus: false,
    userGrid: null,
    kendoGridCancel:null
}

function showDialog() {
    userPage.dialogMail.open();
    userPage.dialogMail.refresh();

    userPage.messageCombo.value("");
    userPage.messageCombo.dataSource.read();
    userPage.messageContent.value(null);
}
function showDisapproveDialog() {
    userPage.dialogMail.open();
    userPage.dialogMail.refresh();

    userPage.messageCombo.value("");
    userPage.messageCombo.dataSource.read();
    userPage.messageContent.value(null);
}

//usereditor
function onEdit(e) {
    setEditorComponents();
    userPage.modal = e.model;
    userPage.editorWindow = null;
    var checkCountry = isGuid($("#CountryId").data("kendoComboBox").text());
    if (checkCountry == true)
        $("#CountryId").data("kendoComboBox").text("");
 
    debugger;
    userPage.editorWindow = e.container.data("kendoWindow");
    var userRole = userPage.sessionUserRole.data('value');
    userPage.userStatus.value = userPage.globalStatus = userPage.modal.Status;
    var isStatus = (parseInt(userPage.userStatus.value) != 1);
    switch (userRole) {
        case userPage.userRoles.Marketing:
            userPage.passwordButton.attr("disabled", true);
            userPage.disapprovedButton.attr("disabled", isStatus);
            userPage.approvedButton.attr("disabled", isStatus);
            break;
        case userPage.userRoles.Admin:
            userPage.passwordButton.attr("disabled", true);
            userPage.disapprovedButton.attr("disabled", true);
            userPage.approvedButton.attr("disabled", false);
            break;
        case userPage.userRoles.SuperAdmin:
            userPage.passwordButton.attr("disabled", false);
            userPage.disapprovedButton.attr("disabled", isStatus);
            userPage.approvedButton.attr("disabled", isStatus);
            break;
        default:
            display_error("User role not defined !", false);
            break;
    }
}
function isGuid(value) {
    var regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
    var match = regex.exec(value);
    debugger
    return match != null;
}
function fnApproved() {
    userPage.userStatus.value = userPage.globalStatus;
    $(userPage.userStatus).trigger("change");
    if (userPage.globalStatus == 1)
        userPage.userStatus.value = 2;
    userPage.statusDate.value = new Date();
    $(userPage.statusDate).trigger("change");
    $(userPage.userStatus).trigger("change");
    setReadOnly(userPage.approvedButton);

};

//Func hook_request_end used to bind requestEnd with GridUpdate.
//This show mail status, when Account status changed
//from "EmailAddressApproved" to "Activated"
function hook_request_end() {
    if (userPage.requestEndHooked == true) {
        return;
    }
    userPage.requestEndHooked = true;
    userPage.userGrid.dataSource.bind("requestEnd", dataSource_requestEnd);
}

function dataSource_requestEnd(e) {
    try {
        if (e.type == "update" && e.response.accountapprove == 1) {
            if (userPage.displaystatus == true) {
                return;
            }
            if (e.response.mail == 0) {
                userPage.displaystatus = true;
                display_error("Account Approved. Mail Sent Successfully !", e.response.status);
            }
            else {
                userPage.displaystatus = true;
                display_error("Account Approved. Mail Sent Failed !", e.response.status);
            }
        }

    }
    catch (e) {
    }
}

function kendoGridCancelClick() {
    var grid = userPage.userGrid;

    if (!grid.dataSource.hasChanges()) {
        grid.dataSource.read();
    }
};
//Delete User 
function disApproveUser() {
    var user = {
        "Id": userPage.modal.Id,
        "Username": userPage.modal.Username,
        "EmailAddress": userPage.modal.EmailAddress,
        "IsDelete": true
    };
    page_loader(true);
    $.ajax({
        url: "/User/UserDisApprove",
        type: "POST",
        dataType: 'json',
        data: { user: user, disapproveComment: userPage.txtAreaComment.value },
        success: function (response) {
            userPage.userGrid.dataSource.read()
            if (response.mail === 0) {
                display_error("Account Disapproved. Mail Sent Successfully !", response.status);
            }
            else {
                display_error("Account Disapproved. Mail Sent Failed !", response.status);
            }
            page_loader(false);
        },
        error: function (error) {
            display_error("Account Disapproval Failed !", response.status);
            page_loader(false);
            userPage.userGrid.dataSource.read()
        }
    });
}

function disapprovedClick() {
    userPage.kendoGridCancel.click();
    $.dialog({
        "body": "Disapproved user will be deleted...!",
        "title": "Confirm Delete",
        "show": true,
        "modal": true,
        "height": '180px',
        "width": '910px',
        "closeOnMaskClick": true,
        "allowScrolling": true,
        "footer": "<label>Reasons for Disapprove: </label> <textarea id='txtareaComment' style='width:650px;height:95px;margin-left:10px'/><button class='saveButton' data-dialog-action=\"hide\" onClick='disApproveUser()'>Yes</button> <button class='cancelButton' data-dialog-action=\"hide\">No</button>"
    });
};
function passWordClick(){
    userPage.statusBlock.check(true);
    userPage.statusBlock.trigger("change");
    userPage.resetPswd.trigger("change");

};
function restPswdChange(e) {
    userPage.resetPswd.check(true);
    userPage.resetPswd.options.checked = true;
}

function setEditorComponents() {
    userPage.userStatus = document.getElementById('Status');
    userPage.approvedButton = $("#btnApproved");
    userPage.disapprovedButton = $("#btnDisApproved");
    userPage.passwordButton = $("#btnPwd");
    userPage.txtFirstName = $('#txtFirstName');
    userPage.txtLastName = $('#txtLastName');
    userPage.txtEmailAddress = $('#txtEmailAddress');
    userPage.txtMarketingPerson = $('#txtMarketingPerson');
    userPage.countryId = $('#CountryId');
    userPage.blocked = $('#Blocked');
    userPage.statusDate = document.getElementById('StatusDate');
    userPage.txtAreaComment = $("#txtareaComment");
    userPage.statusBlock = $("#Blocked").data("kendoSwitch");
    userPage.resetPswd = $("#Resetpassword").data("kendoSwitch");
    userPage.sessionUserRole = $("#SessionUserRole");
    userPage.kendoGridCancel =$(".k-grid-cancel");
    setReadOnly(userPage.disapprovedButton, userPage.passwordButton, userPage.txtFirstName, userPage.txtLastName,
        userPage.txtEmailAddress, userPage.txtMarketingPerson, userPage.countryId, userPage.blocked);
    setEditEvents();
    setComponents();
}

function setComponents() {
    userPage.dialogMail = $('#dialog_mail').data("kendoWindow");
    userPage.messageCombo = $("#message").data("kendoComboBox");
    userPage.messageContent = $('#msg_content').data("kendoEditor");
    userPage.userGrid = $("#UserGrid").data("kendoGrid");
}

function setEditEvents() {
    $(userPage.approvedButton).click(fnApproved);
    $(userPage.passwordButton).click(passWordClick);
    $(userPage.disapprovedButton).click(disapprovedClick);
    $(userPage.kendoGridCancel).click(kendoGridCancelClick);
}
﻿//ready hook og images
$(document).ready(function () {
    $('#cancel').attr('disabled', true);

});

//event listeners
$('#image_res_form :input').on('keyup change', function () {
    $('#cancel').removeAttr('disabled');
});

//clear image dimensions
function clearImageDimensions(url) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        success: function (data) {
            var errorstatus = false;
            $('#content-area').html(data);
            display_error("Loading old data", errorstatus);
        }
    });
}

//save image details
function saveImage(url) {
    serviceWrap.submitForm(url, function () {
    });
}


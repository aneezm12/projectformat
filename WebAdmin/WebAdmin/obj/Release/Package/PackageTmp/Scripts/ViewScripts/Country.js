﻿//save country details
function saveCountry(url) {
    serviceWrap.submitForm(url, function () {
        var grid = $("#CountryGrid").data("kendoGrid");
        grid.dataSource.read();
        var combo = $("#Country_CountryName").data("kendoComboBox");
        combo.dataSource.read();
        combo.value("");
    });
}
//tab - add new country
$('#tbNewCountry').click(function () {  
    var combo = $("#Country_CountryName").data("kendoComboBox");
    combo.dataSource.read();
});
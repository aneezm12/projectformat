﻿//initiateLoader(document.readyState);
var responseData = null;

//debugger;


function messagefunction(e) {
    debugger;
    console.log(e);
    if (e.items[0].Message != null) {

        $("#dialog").kendoDialog({
            width: "400px",
            title: "Software Update",
            closable: false,
            modal: false,
            content: e.items[0].Message,
            actions: [

                { text: 'OK', primary: true }
            ]
        });
    }
}

$.ajax({
    url: "/Software/SoftwareRead",
    type: "GET",
    success: function (response) {
        console.log(response)
        responseData = response;
        if (response.warningR != null) {

            $("#dialog").kendoDialog({
                width: "400px",
                title: "Software Update",
                closable: false,
                modal: false,
                content: "<center>A new version is available. Please update asap<center>",
                actions: [
                   
                    { text: 'OK', primary: true }
                ]
            });

            //$.dialog({
            //    "body": response.warningR,
            //    "title": "Alert",
            //    "show": true,
            //    "modal": true,
            //    "height": 50,
            //    "closeOnMaskClick": false,
            //    "allowScrolling": true,
            //    "footer": "<button name='saveButton' data-dialog-action=\"hide\" >Ok</button> <button name='cancelButton' data-dialog-action=\"hide\">Cancel</button>"
            //});
        }
        $("#grid").kendoGrid({
            dataSource: response.softwaredata,
            scrollable: false,
            columns: [
                {
                    field: "Name",
                    title: "Name"
                },
                {
                    field: "Version",
                    title: "Version",
                }
            ]
        });


        $("#grid2").kendoGrid({
            dataSource: response.latestVersion,
            scrollable: false,
            columns: [
                {
                    field: "Name",
                    title: "Name"
                },
                {
                    field: "Version",
                    title: "Version",
                }
            ]
        });
    }
});


//var sharedDataSource = [{ "Software": "Windows 10", "Version": "10.0.0.8" }];


﻿//file to keep all ajax calls , act as a service wrapper
serviceWrap = {
    submitForm: submitForm,
    getPage: getPage
}
//service submit form
function submitForm(inputUrl, callBack) {
    debugger
    var data = $('form').serialize() ;
    $.ajax({
        url: inputUrl,
        type: 'POST',
        data: $('form').serialize(),
        success: function (response) {
            utility.clearValidationMessage(true);
            if (response.status == true) {
                if (callBack) {
                    callBack();
                }
                if (!response.keepModelValues) {
                    //clear all controls
                    utility.clearAllControls();
                }
            }
            else {
                $.each(response.validationErrors, function (index, error) {
                    var validationMessage = error.errorMessage[0];
                    if (validationMessage) {
                        //highlight the element
                        var element = $("[name='" + error.element + "']");
                        element.addClass('highlight');
                        // add each error
                        var validationElement = $("[data-valmsg-for='" + error.element + "']");
                        validationElement.text(validationMessage);
                    }
                });
            }
            display_error(response.message, response.status);
        },
        error: function () {//write the logic here when the submit fails
        }
    });
}

//function to get pages
function getPage(inputUrl, successCallBack, failCallBack) {
    $.ajax({
        url: inputUrl,
        type: 'POST',
        dataType: "html",
        success: function (data) {
            if (successCallBack) {
                successCallBack(data);
            }
        },
        error: function (error) {
            if (failCallBack) {
                failCallBack();
            }
        }
    });
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;



namespace WebAdmin
{

    /// <summary>
    /// Abstract base class for SQL responsibilities
    /// </summary>
    public abstract class SqlRepositoryBase
    {


        protected IDatabaseService db;

        protected SqlRepositoryBase(IDatabaseService db)
        {
            this.db = db;
        }
        /// <summary>
        /// Stores  a  model  to  the  database
        ///  </summary>

        protected Task StoreAsync<T>(T data) where T : ISqlEntity
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            string sql = Sq1Helper.GetFullUpdateStatementFor<T>() + " WHERE [Id] = @Id; IF @@ROWCOUNT = 0 " +
                     Sq1Helper.GetFullInsertStatementFor<T>();
            return db.ExecuteAsync(sql, data);
        }


        ///  <summary>
        ///  Loads  a  model  from  the  database  by it’s  primary  id
        ///  </summary>

        protected Task<T> FetchAsync<T>(Guid id) where T : ISqlEntity
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            return db.QuerySingleAsync<T>(Sq1Helper.GetFullSelectQueryFor<T>() + " WHERE [Id] = @Id", new { Id = id });
        }


        /// <summary>
        /// Deletes a model from the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>

        protected Task DeleteAsync<T>(T data) where T : ISqlEntity
        {
            return DeleteAsync<T>(data.Id);
        }

        /// <summary>
        /// Deletes a model from the database by its id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        protected Task DeleteAsync<T>(Guid id) where T : ISqlEntity
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            return db.ExecuteAsync(
             "DELETE FROM " + Sq1Helper.GetFullTableNameFor<T>() + " WHERE [Id] = @Id",
                  new
                  {
                      Id = id
                  });
        }


        ///   <summary>
        ///   Updates  the  model  data  in  the  database
        ///  </summary>
        ///  <param  name=”id”>The  primary  key  id  of  the  model</param>
        ///  <param  name=”data”>The  data  to  update</param>

        protected Task UpdateAsync<T>(Guid id, dynamic data)
        {
            List<string> columns = new List<string>();
            foreach (PropertyInfo prop in data.GetType().GetProperties())
            {
                string columnName = prop.GetCustomAttribute<SqlColumnAttribute>()?.Name ?? prop.Name;
                columns.Add($"[{columnName}] = @{columnName}");
            }
            return db.ExecuteAsync(
                  "UPDATE " + Sq1Helper.GetFullTableNameFor<T>()
                   + " SET " + string.Join(",", columns)
                   + " WHERE [Id] = '" + id + "'",
                       data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        protected async Task<bool> ExistsAsync<T>(Guid id) where T : ISqlEntity
        {
            int count =
                   await
                           db.ExecuteScalarAsync<int>(
                                  "SELECT COUNT(1) FROM "   + Sq1Helper.GetFullTableNameFor<T>() +  " WHERE[Id] = @Id" ,
                          new   { Id = id   } ) ;
            return count >= 1;
        }




    }
}
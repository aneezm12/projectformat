﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using Dapper;
using System.Configuration;
using System.Diagnostics;

using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace WebAdmin
{



    public interface IDatabaseService
    {
        Task<int> ExecuteAsync(string sq1, object parameters);

        Task<IEnumerable<TResult>> QueryAsync<TResult>(string sq1, object parameters = null);

        Task<IEnumerable<TResult>> QueryAsync<T1, T2, TResult>(
          string sq1,
          Func<T1, T2, TResult> mappingFunction,
         object parameters = null);


        Task<IEnumerable<TResult>> QueryAsync<T1, T2, T3, TResult>(
          string sq1,
          Func<T1, T2, T3, TResult> mappingFunction,
          object parameters = null);

        Task<IEnumerable<TResult>> QueryAsync<T1, T2, T3, T4, TResult>(
              string sql,
              Func<T1, T2, T3, T4, TResult> mappingFunction,
              object parameters = null);

        Task<TResult> QueryFirstAsync<TResult>(string sq1, object parameters);

        Task<TResult> QueryFirstOrDefaultAsync<TResult>(string sq1, object parameters);

        Task<TResult> QuerySingleAsync<TResult>(string sq1, object parameters);

        Task<TResult> QuerySingleOrDefaultAsync<TResult>(string sq1, object parameters);

        Task<TResult> ExecuteScalarAsync<TResult>(string sq1, object parameters);

    }

    ///   <summary>
    ///   This class is a façade object  to Dapper   (see  http://dapper-tutorial.net)  which  allows  it to mock database access,
    ///  add security  assertion,  log  SQL   queries  and perform  automatic  encryption  etc.
    ///  </summary>

    public class DatabaseService : IDatabaseService
    {
        private string connectionString;

        public DatabaseService()
        {
            
                connectionString = ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString;
            
        }
        ///  <summary>
        ///  Executes an INSERT/UPDATE/DELETE  statement on the database.
        ///   </summary>
        ///   <returns>The number of affected rows by the statement</returns>

        public async Task<int> ExecuteAsync(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsInsertUpdateOrDeleteStatement(sq1);
            int result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.ExecuteAsync(sq1, parameters).ConfigureAwait(false);
            }
            return result;
        }
        ///  <summary>
        ///  Executes  a query  and  maps  the  result  to  an  object  of  type  TResult.
        ///  </summary>

        public async Task<IEnumerable<TResult>> QueryAsync<TResult>(string sq1, object parameters = null)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            IEnumerable<TResult> result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryAsync<TResult>(sq1, param: parameters).ConfigureAwait(false);
            }
            return result;
        }

        ///  <summary>
        ///  Executes a query and maps the result to an object of type TResult by using multi-mapping
        ///  (see  Dapper’s documentation for more details)
        ///  </summary>

        public async Task<IEnumerable<TResult>> QueryAsync<T1, T2, TResult>(
                  string sq1,
                  Func<T1, T2, TResult> mappingFunction,
                 object parameters = null)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            IEnumerable<TResult> result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryAsync<T1, T2, TResult>(sq1, mappingFunction, parameters).ConfigureAwait(false);
            }

            return result;
        }
        ///  <summary>
        ///  Executes a query and maps the result to an object of type TResult by using multi-mapping
        ///  (see  Dapper’s documentation for more details)
        ///  </summary>

        public async Task<IEnumerable<TResult>> QueryAsync<T1, T2, T3, TResult>(
                  string sq1,
                  Func<T1, T2, T3, TResult> mappingFunction,
                  object parameters = null)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            IEnumerable<TResult> result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryAsync<T1, T2, T3, TResult>(sq1, mappingFunction, parameters).ConfigureAwait(false);
            }
            return result;
        }
        ///  <summary>
        ///  <Executes a query and maps the result to an object of type TResult by using multi-mapping
        ///  (see Dapper’s documentation for more details)
        ///  </summary>

        public async Task<IEnumerable<TResult>> QueryAsync<T1, T2, T3, T4, TResult>(
                      string sql,
                      Func<T1, T2, T3, T4, TResult> mappingFunction,
                      object parameters = null)
        {
            Debug.WriteLine(sql);
            AssertThatSq1IsSelectStatement(sql);
            IEnumerable<TResult> result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryAsync<T1, T2, T3, T4, TResult>(sql, mappingFunction, parameters).ConfigureAwait(false);
            }
            return result;
        }

        ///   <summary>
        ///  Executes a query and maps the result to an object of type TResult.
        ///  Only returns the first element found or null.
        /// Note: Use QuerySingleAsync()  if only one result is expected I
        ///   </summary>

        public async Task<TResult> QueryFirstAsync<TResult>(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            TResult result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryFirstAsync<TResult>(sq1, parameters).ConfigureAwait(false);
            }
            return result;
        }
        ///  <summary>
        ///   Executes a query and maps the result to an object of type TResult.
        ///  Only returns the first element found or the default object of the type if nothing was found.
        ///   Note:  Use QuerySingleAsync()  if only one result is expected I
        ///   </summary>

        public async Task<TResult> QueryFirstOrDefaultAsync<TResult>(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            TResult result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QueryFirstOrDefaultAsync<TResult>(sq1, parameters).ConfigureAwait(false);
            }
            return result;
        }
        ///   <summary>
        ///   Executes a query and maps the result to an object of type TResult.
        ///   Expects exactly one result or else will throw an exception.
        ///   </summary>
        public async Task<TResult> QuerySingleAsync<TResult>(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            TResult result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QuerySingleAsync<TResult>(sq1, parameters).ConfigureAwait(false);
            }
            return result;
        }

        ///   <summary>
        ///   Executes a query and maps the result to an object of type TResult.
        ///   Expects exactly one result.
        ///   If none was found,  returns the default value of TResult.
        ///   If several were found,throws an exception.
        ///    </summary>

        public async Task<TResult> QuerySingleOrDefaultAsync<TResult>(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            TResult result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.QuerySingleOrDefaultAsync<TResult>(sq1, parameters).ConfigureAwait(false);
            }
            return result;
        }


        ///   <summary>
        ///   Executes a query that queries a single value and returns it
        ///   </summary>

        public async Task<TResult> ExecuteScalarAsync<TResult>(string sq1, object parameters)
        {
            Debug.WriteLine(sq1);
            AssertThatSq1IsSelectStatement(sq1);
            TResult result;
            using (var db = new SqlConnection(connectionString))
            {
                result = await db.ExecuteScalarAsync<TResult>(sq1, parameters).ConfigureAwait(false);
             
            }
            return result;
        }

        ///   <summary>
        ///   Asserts  that an SQL statement starts with SELECT or throws an exception.
        ///    </summary>

        private void AssertThatSq1IsSelectStatement(string sql)
        {
            if (sql == null || !sql.StartsWith("SELECT ", true, CultureInfo.InvariantCulture))
            {
                throw new InvalidOperationException("A  non - SELECT  statement was used in QueryAsync()");
            }
        }


        ///   <summary>
        ///   Asserts than an SQL statement starts with INSERT, UPDATE or DELETE or throws an exception.
        ///   </summary>
        ///   </note>

        private void AssertThatSq1IsInsertUpdateOrDeleteStatement(string sql)
        {
            if(sql == null  || (!sql.StartsWith("INSERT ") && !sql.StartsWith("UPDATE") && !sql.StartsWith("DELETE")))
                              {
                throw new InvalidOperationException("A non-INSERT/UPDATE/DELETE  statement was used In ExecuteAsync()");
            }
        }


    }




}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using System.Configuration;



namespace WebAdmin
{
  
    public class SettingRepository : DataRepository<Setting>
    {
       
      
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<Setting>();
        public SettingRepository(IDatabaseService db): base(db)
        {
        }
        
       
        public override async Task<int> UpdateById(Setting t, string textParam)
        {
            try
            { 
            int isUpdate = await db.ExecuteAsync(
                "UPDATE [Setting] SET Value = " + "@value" + " WHERE [Setting].[Key] = @key",
                new { t.Value, t.Key });
            return isUpdate;
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }
      

    }
    }
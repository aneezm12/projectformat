﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public interface IDashBoardService
    {
        DashBoardViewModel GetDashBoradCounts(IEnumerable<User> user, int ringCount, int structCount, int countryCount);
    }
}
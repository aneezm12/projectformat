﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using System.Web.Mvc;

namespace WebAdmin
{
    public interface IDataRepository<T> where T: class
    {
        Task<IEnumerable<T>> GetDatas();
        Task<int> DeleteByIdAsync(T data);      
        Task<int> UpdateById(T t);
        Task<int> UpdateById(T t, string textParam);
        Task<int> InsertById(T t);
        Task<EventMessage> GetMessageDescriptionByTitleAndEventStart(DateTime EventStart, string MessageTitle);
        IEnumerable<object> GetAllMessagesAndEventStart(List<Guid> data);
    }
}
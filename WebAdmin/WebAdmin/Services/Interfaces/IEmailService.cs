﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAdmin
{
   public  interface IEmailService
    {
        int SendEventMessageMail(Dictionary<User,Tuple<string,string>> mapList);
        int SendUserDisapproveMail(User email,string disapproveComment);
        int SendUserApproveMail(User email);

    }
}

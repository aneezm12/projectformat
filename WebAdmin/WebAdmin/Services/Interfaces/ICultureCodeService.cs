﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAdmin
{
    public enum LanguageName
    {
        English = 0,
        German = 1
    }
   public interface ICultureCodeService
    {
        MultiMap<string, string> GetCultureCodeList(LanguageName Name);

        string GetCultureCode(MultiMap<string, string> culturesLst, string userRequestCultureCode, LanguageName name);
    }
}

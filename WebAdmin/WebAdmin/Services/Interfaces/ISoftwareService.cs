﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAdmin
{
    public interface ISoftwareService
    {
        List<Software> GetSoftwareVersion();
        List<Software> GetAssemblyVersion();

        List<Software> GetCurrentProjectVersion();
    }
}

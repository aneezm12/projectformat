﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
  //  [AuditLog]
    public class AuditLogRepository : DataRepository<AuditLog>
    {
        private readonly string fullAuditTableName = Sq1Helper.GetFullTableNameFor<AuditLog>();
        public AuditLogRepository(IDatabaseService db) : base(db)
        {
        }

        public override Task<int> DeleteByIdAsync(AuditLog data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullAuditTableName + " WHERE [AuditLog].[Id]  = @Id",
                    new { Id = data.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
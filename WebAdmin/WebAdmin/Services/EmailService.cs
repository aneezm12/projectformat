﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace WebAdmin
{
    public class EmailService : IEmailService
    {


        private readonly string SMTPServerHost = WebConfigurationManager.AppSettings["SMTPServerHost"];

        private readonly int SMTPPort = int.Parse(!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["SMTPPort"])
                                               ? WebConfigurationManager.AppSettings["SMTPPort"] : "0");

        private int SendEmailMessage(string subject, string message, IEnumerable<User> receiver, string sender)
        {
          
            var mail = new MailMessage();
            mail.From = new MailAddress(sender);
            mail.IsBodyHtml = true;

            SmtpClient client = new SmtpClient(SMTPServerHost, SMTPPort);
            client.EnableSsl = true;
            if (WebConfigurationManager.AppSettings["UserEmailAddress"] != null)
            {
                client.Credentials = new NetworkCredential(WebConfigurationManager.AppSettings["UserEmailAddress"], WebConfigurationManager.AppSettings["UserEmailPassword"]);

            }
            else
            {
                client.UseDefaultCredentials = false;

            }
            mail.Subject = subject;
            try
            {
                foreach (User r in receiver)
                {
                    mail.Body = "UserName: <b>" + r.Username + "</b><br> " + message;
                    mail.To.Add(new MailAddress(r.EmailAddress));
                    client.Send(mail);
                    mail.To.Clear();
                }

                return 0;
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error("Mail Failed" + exception.Message);
                return 1;
            }
        }

        /// <summary>
        /// The send message email.
        /// </summary>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <param name="mapList">
        /// The map list.
        /// </param>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <returns>int
        /// </returns>
        private int SendMessageEmail( Dictionary<User, Tuple<string,string>> mapList, string sender)

        {
            if (string.IsNullOrEmpty(sender))
            {
                MvcApplication.Logger.Error("Sender Email is empty");
                return 1;
            }

            var mail = new MailMessage { From = new MailAddress(sender), IsBodyHtml = true };

            var client = new SmtpClient(SMTPServerHost, SMTPPort);
            client.EnableSsl = true ;
            if (WebConfigurationManager.AppSettings["UserEmailAddress"] != null)
            {
                client.Credentials = new NetworkCredential(WebConfigurationManager.AppSettings["UserEmailAddress"], WebConfigurationManager.AppSettings["UserEmailPassword"]);

            }
            else
            {
                client.UseDefaultCredentials = false;

            }
            //mail.Subject = subject;
            try
            {
                foreach (var data in mapList)
                {
                    mail.Subject = data.Value.Item2;
                    mail.Body = "UserName: <b>" + data.Key.Username + "</b><br> " + data.Value.Item1;
                    mail.To.Add(new MailAddress(data.Key.EmailAddress));
                    client.Send(mail);
                    mail.To.Clear();
            }           
                            
                return 0;
            }
            catch(Exception exception)
            {
                MvcApplication.Logger.Error("Mail Failed");
                return 1;
            }
        }
        /// <summary>
        /// Function to send event message mails
        /// </summary>
        /// <param name="mapList">
        /// The map List.
        /// </param>
        /// <returns>
        /// int
        /// </returns>
        public int SendEventMessageMail(Dictionary<User, Tuple<string,string>> mapList)
        {
            string fromAddress = WebConfigurationManager.AppSettings["UserEmailAddress"];

            //string subject = MailSubjectHelper.GetSubjectCreator(MailSubject.EventMessage);
            int mail = SendMessageEmail(mapList, fromAddress);
            return mail;
        }
        public int SendUserApproveMail(User email)
        {
            List<User> mailList = new List<User>();
            mailList.Add(email);
            string FromAddress = WebConfigurationManager.AppSettings["UserEmailAddress"];
            string Subject = MailSubjectHelper.GetSubjectCreator(MailSubject.ApprovedMessage);

            string message = "The account has been Approved. ";
            int mail = SendEmailMessage(Subject, message, mailList, FromAddress);
            return mail;
        }
        public int SendUserDisapproveMail(User email,string DisapproveComment)
        {
            List<User> mailList = new List<User>();
            mailList.Add(email);
            string FromAddress = WebConfigurationManager.AppSettings["UserEmailAddress"];
            string Subject = MailSubjectHelper.GetSubjectCreator(MailSubject.DisapprovedMessage);
            string message = "The account has been disapproved. "+ "<br>"+DisapproveComment;
            int mail = SendEmailMessage(Subject, message, mailList, FromAddress);
            return mail;
        }

    }
}
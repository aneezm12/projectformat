﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace WebAdmin
{
    public class DataRepository<TData> : IDataRepository<TData> where TData : class
    {
        protected IDatabaseService db;

        public DataRepository(IDatabaseService db)
        {
            this.db = db;
        }   
        public  async Task<IEnumerable<TData>> GetDatas()
        {
            IEnumerable<TData> data;
            try
            { 
             data =  await db.QueryAsync<TData>(Sq1Helper.GetFullSelectQueryFor<TData>())??null;
            
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception.Message, exception);
                throw exception;
            }
            return data;
        }

       
        public virtual Task<int> DeleteByIdAsync(TData data)
        {
            return null;
        }
        public virtual Task<int> UpdateById(TData t)
        {
            return null;
        }

        public virtual Task<int> UpdateById(TData t, string textParam)
        {
            return null;
        }
        
     
        public virtual async Task<int> InsertById(TData t)
        {
            try
            { 
            var update = 0;
            update = await db.ExecuteAsync(Sq1Helper.GetFullInsertStatementFor<TData>(), t);
            return update;
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        public virtual dynamic GetValues()
        {
            return null;
        }

        public virtual Task<EventMessage> GetMessageDescriptionByTitleAndEventStart(DateTime EventStart, string MessageTitle)
        {
            return null;
        } 
        public virtual IEnumerable<object> GetAllMessagesAndEventStart(List<Guid> data)
        {
            return null;
        }  


        //public virtual IEnumerable<TData> GetSoftwareDetails() {
        //    return null;
        //}

    }
}
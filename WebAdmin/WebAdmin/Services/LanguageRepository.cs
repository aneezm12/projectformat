﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
  //  [Language]
    public class LanguageRepository : DataRepository<Language>
    {
        public LanguageRepository(IDatabaseService db) : base(db)
        {
        }
    }
}
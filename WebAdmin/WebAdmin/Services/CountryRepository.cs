﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using WebAdmin.Models;
using System.Text;

namespace WebAdmin
{
 // [UserAttribute]
    

    public class CountryRepository :DataRepository<Country>
    {
        private readonly string fullCountryTableName = Sq1Helper.GetFullTableNameFor<Country>();
        public CountryRepository(IDatabaseService db) : base(db)
        {
        }

        public override async Task<int> UpdateById(Country t, string textParam)
        {
            try
            { 
            var update = 0;
            int existingCountry = await db.ExecuteScalarAsync<int>(
                "SELECT count(*) FROM " + fullCountryTableName + "WHERE [Country].[Id] = @Id",

                new { t.Id });

            if (existingCountry != 0)

            {
                update = await db.ExecuteAsync("UPDATE [Country]"
                    + "SET Status =" + "@Status" +
                    " WHERE [Country].[Id] = @Id", new { t.Id, t.Status });

            }
                string st = t.Status ? "active" : "deactive";
                StringBuilder stringBuilder = update == 1 ? DataHelper.CreateMessage(textParam, t.CountryName + " Status changed to " + st) : null;
                if (stringBuilder != null)
                {
                    MvcApplication.Logger.Info(stringBuilder);
                }
                return update;
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }



        public override Task<int> DeleteByIdAsync(Country data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullCountryTableName + " WHERE [Country].[Id]  = @Id",
                    new { Id = data.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class GreaterThanAttribute : ValidationAttribute
    {

        public GreaterThanAttribute(string otherProperty)
            : base("{0} must be greater than {1}")
        {
            OtherProperty = otherProperty;
        }

        public string OtherProperty { get; set; }

        public string FormatErrorMessage(string name, string otherName)
        {
            return string.Format(ErrorMessageString, name, otherName);
        }

        protected override ValidationResult IsValid(object firstValue, ValidationContext validationContext)
        {
            var firstComparable = firstValue as IComparable;
            var secondComparable = GetSecondComparable(validationContext);

            if (firstComparable != null && secondComparable != null)
            {
                if (validationContext.MemberName == "maxwidth" || validationContext.MemberName == "minwidth" || validationContext.MemberName == "minheight" || validationContext.MemberName == "maxheight")
                {
                    if (Convert.ToInt32(firstComparable.ToString()) <= Convert.ToInt32(secondComparable.ToString()))
                    {
                        string errorMessage = FormatErrorMessage(ErrorMessageString);

                        object obj = validationContext.ObjectInstance;
                        var thing = obj.GetType().GetProperty(OtherProperty);
                        var displayName = (DisplayAttribute)Attribute.GetCustomAttribute(thing, typeof(DisplayAttribute));

                        return new ValidationResult(ErrorMessage);

                    }

                }
                if (float.Parse(firstComparable.ToString()) <= float.Parse(secondComparable.ToString()))
                {
                    string errorMessage = FormatErrorMessage(ErrorMessageString);

                    object obj = validationContext.ObjectInstance;
                    var thing = obj.GetType().GetProperty(OtherProperty);
                    var displayName = (DisplayAttribute)Attribute.GetCustomAttribute(thing, typeof(DisplayAttribute));

                    return new ValidationResult(ErrorMessage);

                }

            }

            return ValidationResult.Success;
        }

        protected IComparable GetSecondComparable(ValidationContext validationContext)
        {
            var propertyInfo = validationContext
                                  .ObjectType
                                  .GetProperty(OtherProperty);
            if (propertyInfo != null)
            {
                var secondValue = propertyInfo.GetValue(
                    validationContext.ObjectInstance, null);
                return secondValue as IComparable;
            }
            return null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
    public class ContactRepository : DataRepository<Contact>
    {
        private readonly string contactTableName = Sq1Helper.GetFullTableNameFor<Contact>();
        
        public ContactRepository(IDatabaseService db) : base(db)
        {
        }

        public async override Task<int> UpdateById(Contact t)
        {
            try
            {
             
                var update = await db.ExecuteAsync("UPDATE [Contact]"
                        + "SET MobileNo =" + "@MobileNo,  Email = " + "@Email" +
                        " WHERE [Contact].[Id] = @Id", new { t.Phone, t.Email, t.Id });
               
            }
            catch (Exception ex)
            {

                throw;
            }
            return 1;
        }

     
    }       
}
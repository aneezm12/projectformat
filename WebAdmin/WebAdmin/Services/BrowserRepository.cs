﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
    public class BrowserDetailsAttribute : Attribute
    {

    }

  //  [BrowserDetailsAttribute]
    public class BrowserRepository : DataRepository<BrowserDetails>
    {
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<BrowserDetails>();
        public BrowserRepository(IDatabaseService db) : base(db)
        {

        }
       
      

        public override Task<int> DeleteByIdAsync(BrowserDetails data)
        {
            try
            { 
            return db.ExecuteAsync("DELETE FROM " + fullImageTableName + " WHERE [BrowserDetails].[Id]  = @Id",
                new { Id = data.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using WebAdmin.Models;

namespace WebAdmin
{
//    [SettingAttribute]
    public class ConfigurationSettingRepository : DataRepository<ConfigurationSetting>
    {
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<ConfigurationSetting>();
        private readonly string BrowserTable = Sq1Helper.GetFullTableNameFor<BrowserDetails>();

        public ConfigurationSettingRepository(IDatabaseService db) : base(db)
        {

        }
        public override Task<int> DeleteByIdAsync(ConfigurationSetting data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullImageTableName + "WHERE [ConfigurationSetting].[Id]  = @Id",
                    new { Id = data.Id });
            }
            catch(Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                return null;
            }
        }
       
        public override Task<int> UpdateById(ConfigurationSetting t, string textParam)
        {
            throw  new NotImplementedException();

        }
    }
}
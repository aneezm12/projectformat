﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebAdmin
{
 //   [BlockIPAttribute]
    public class BlockIPRepository : DataRepository<BlockIpAddress>
    {
        private readonly string fullIPaddressTableName = Sq1Helper.GetFullTableNameFor<BlockIpAddress>();

        public BlockIPRepository(IDatabaseService db) : base(db)
        {
        }
        public override Task<int> DeleteByIdAsync(BlockIpAddress data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullIPaddressTableName + " WHERE [BlockIPAddress].[Id]  = @Id",
                    new { Id = data.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
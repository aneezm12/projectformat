﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
  //  [HardwareAttribute]
    public class StrutRepository : DataRepository<Strut>
    {
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<Strut>();
        public StrutRepository(IDatabaseService db) : base(db)
        {
        }

        public override async Task<int> UpdateById(Strut t, string textParam)
        {
            var update = 0;
            try
            {
                int existingImages = await db.ExecuteScalarAsync<int>(
                    "SELECT count(*) FROM " + fullImageTableName + "WHERE [Strut].[Id] = @Id",
                    new { t.Id });
                if (existingImages != 0)

                {
                    update = await db.ExecuteAsync("UPDATE [Strut]"
                        + "SET Disabled =" + "@Disabled " +
                        " WHERE [Strut].[Id] = @Id", new { t.Id, t.Disabled });

                }
                string st = t.Disabled ? "deactive" : "active";
                StringBuilder stringBuilder = update == 1 ? DataHelper.CreateMessage(textParam, t.PartNumber + " Disabled changed to " + st) : null;
                if (stringBuilder != null)
                {
                    MvcApplication.Logger.Info(stringBuilder);
                }
                return update;
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }

        }


    }
}
﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;

namespace WebAdmin
{
    public class SoftwareService : ISoftwareService
    {
        public List<Software> GetAssemblyVersion()
        {
            List<Software> softwares = new List<Software>();
            try
            {
                var baseDir = AppDomain.CurrentDomain.BaseDirectory;
                string assemblyPath = ConfigurationManager.AppSettings["AssemblyPath"];
                string dllDir = baseDir + assemblyPath;

                var assemblyDetails = Assembly.GetAssembly(typeof(MvcApplication)).GetName();


                Assembly assembly = Assembly.LoadFrom(dllDir);

              
                Software software = new Software()
                {
                    Id = new Guid(),
                    Name = assembly.GetName().Name,
                    Version = assembly.GetName().Version.ToString(),
                };

                
                softwares.Add(software);


                return softwares;
            }
            catch (Exception ex)
            {
               
                throw;
            }
        }

        public List<Software> GetCurrentProjectVersion()
        {
            List<Software> softwares = new List<Software>();
            try
            {
                var assemblyDetails = Assembly.GetAssembly(typeof(MvcApplication)).GetName();

                Software software = new Software()
                {
                    Id = new Guid(),
                    Name = assemblyDetails.Name,
                    Version = assemblyDetails.Version.ToString()
                };


                softwares.Add(software);

                return softwares;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<Software> GetSoftwareVersion()
        {
           

            List<Software> softwares = new List<Software>();
            try
            {
                string url = ConfigurationManager.AppSettings["AppVersionURL"];
                string strResult = string.Empty;

                HttpWebRequest requests = (HttpWebRequest)WebRequest.Create(url);


                using (HttpWebResponse response = (HttpWebResponse)requests.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    strResult = reader.ReadToEnd();
                }

                List<VersionDetails> versionDetails = JsonConvert.DeserializeObject<List<VersionDetails>>(strResult);

                foreach (var item in versionDetails)
                {
                    Software software = new Software()
                    {
                        Id = new Guid(),
                        Name = item.name,
                        Version = item.version

                    };

                    softwares.Add(software);


                }
                var assemblyVersion = Assembly.GetAssembly(typeof(MvcApplication));//.GetName().Version.ToString();
                return softwares;

            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }


}
﻿    using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
    //  [Message]
    public class MessageRepository : DataRepository<Message>
    {
        private readonly string fullMessageTableName = Sq1Helper.GetFullTableNameFor<Message>();
        private readonly string fullEventMessageTableName = Sq1Helper.GetFullTableNameFor<EventMessage>();
        private readonly string LanguageTable = Sq1Helper.GetFullTableNameFor<Language>();

        public MessageRepository(IDatabaseService db) : base(db)
        {
        }
        public override async Task<int> UpdateById(Message t, string textParam)
        {
            var update = 0;
            StringBuilder auditLogMessage = new StringBuilder("");
            try
            {

                int count = await db.ExecuteScalarAsync<int>(
                "SELECT count(*) FROM " + fullMessageTableName + "WHERE [Message].[MessageId] = @MessageId",
                new { t.MessageId });

                if (count != 0)
                {
                    var messagedata = await db.QuerySingleOrDefaultAsync<Message>("SELECT LanguageId, MessageTitle, Description,Subject FROM " + fullMessageTableName +
                        " WHERE [Message].[MessageId] = @MessageId", new { t.MessageId, t.Description, t.LanguageId,t.Subject });


                    update = await db.ExecuteAsync("UPDATE [Message]"
                        + "SET Description =" + "@Description,  LanguageId = " + "@LanguageId, Subject = " + "@Subject" +
                        " WHERE [Message].[MessageId] = @MessageId", new { t.MessageId, t.Description, t.LanguageId, t.MessageTitle ,t.Subject});


                    //code below to record changes to AuditLog
                    var varianceList = messagedata.Compare(t);

                    if (messagedata.LanguageId != t.LanguageId)
                    {
                        var updatedlang = await db.QueryFirstAsync<Language>("SELECT [Language].Name from " + LanguageTable + " WHERE [Language].[Id] = '"
                            + @t.LanguageId + "'", new { t.LanguageId });
                        var oldlang = await db.QueryFirstAsync<Language>("SELECT [Language].Name from " + LanguageTable + " WHERE [Language].[Id] = '"
                            + @messagedata.LanguageId + "'", new { @messagedata.LanguageId });

                        var v = new Variance
                        {
                            PropertyName = "Language",
                            Old = oldlang.Name,
                            New = updatedlang.Name
                        };
                        varianceList.Add(v);
                    }

                    if (varianceList != null)
                        auditLogMessage = DataHelper.CreateMessageString(textParam, t.MessageTitle, varianceList);

                    if (update == 1 && auditLogMessage != null)
                    {
                        MvcApplication.Logger.Info(auditLogMessage);
                    }

                }

                return update;
            }


            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        public override Task<int> DeleteByIdAsync(Message data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullMessageTableName + " WHERE [Message].[MessageId]  = @MessageId",
                    new { MessageId = data.MessageId });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     
        public  override async Task<EventMessage> GetMessageDescriptionByTitleAndEventStart(DateTime EventStart, string MessageTitle)
        {

           
            var messagedata = await db.QuerySingleOrDefaultAsync<Message>("SELECT LanguageId, MessageTitle, Description FROM " + fullEventMessageTableName
                        , null);

            var ress = db.QueryAsync<Message>(Sq1Helper.GetFullSelectQueryFor<Message>()) ?? null;
          


            var df = await db.QueryAsync<EventMessage>(Sq1Helper.GetFullSelectQueryFor<EventMessage>()) ?? null;


            EventMessage eventMessage = new EventMessage();
            var qry = "Select * from dbo.[EventMessage]";

            var res = db.QuerySingleOrDefaultAsync<EventMessage>(qry,null);

            var qrys = "SELECT EventStart, EventEnd, B.MessageTitle, Description FROM EVENTMESSAGE A JOIN MESSAGE B  ON  A.MessageId = b.MessageId WHERE A.EventStart = ' "
                + EventStart + " ' AND B.MessageTitle = '" + MessageTitle + "'";
            var result = db.QueryFirstAsync<MessageDescription>(qry,null);

            //ress =  Task.Run (() => ress);
            return df.FirstOrDefault();
        }       
        public override IEnumerable<dynamic> GetAllMessagesAndEventStart(List<Guid> data)
        {
            string sql = "SELECT MessageTitle, EventStart from MESSAGE A LEFT JOIN EVENTMESSAGE B ON A.MessageId = b.MessageId WHERE A.MessageId IN @IdArray";
            Guid[] IdArray = data.ToArray();    
            var result = db.QueryAsync<dynamic>(sql, new { @IdArray });
            return result.Result;
        }        
    }
}
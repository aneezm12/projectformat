﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebAdmin
{
    
    public class UserRepository : DataRepository<User>
    {
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<User>();
        private readonly string countryTableName = Sq1Helper.GetFullTableNameFor<Country>();
        private readonly string userTableName = Sq1Helper.GetFullTableNameFor<User>();

        public UserRepository(IDatabaseService db) : base(db)
        {
        }
        
        public override async Task<int> UpdateById(User t, string textParam)
        {

            t.LockOutUntil = DateTime.Now;
            var update = 0;
            var status = 0;
            StringBuilder auditLogMessage = new StringBuilder("");
            try
            {
                int count = await db.ExecuteScalarAsync<int>(
                "SELECT count(*) FROM " + fullImageTableName + "WHERE [User].[Id] = @Id",new  { t.Id });
                if (count != 0)
                {
                    var userdata = await db.QuerySingleOrDefaultAsync<User>("SELECT FirstName , LastName , EmailAddress, MarketingPerson, CountryId, LanguageId, Username, Status, LastLoginDate,StatusDate FROM " + fullImageTableName + " WHERE [User].[Id]  = @Id",
                         new { t.Id, t.FirstName, t.LastName, t.EmailAddress, t.MarketingPerson, t.CountryId, t.LanguageId, t.Username, t.Status, t.LastLoginDate, t.StatusDate});


                   

                    List<Variance> varianceList = userdata.Compare(t);

                 
                    if (userdata.CountryId != t.CountryId)
                    {
                        var updatedCountry = await db.QueryFirstAsync<Country>("SELECT [Country].CountryName from " + countryTableName + " WHERE [Country].[Id] = '"
                            + @t.CountryId + "'", new { t.CountryId });
                        var oldCountry = await db.QueryFirstAsync<Country>("SELECT [Country].CountryName from " + countryTableName + " WHERE [Country].[Id] = '"
                            + @userdata.CountryId + "'", new { userdata.CountryId });

                        var v = new Variance
                        {
                            PropertyName = "CountryId",
                            Old = oldCountry.CountryName,
                            New = updatedCountry.CountryName
                        };
                        varianceList.Add(v);
                    }

                    //var compateLockout = Nullable.Compare(t.LockOutUntil, userdata.LockOutUntil);

                    //if (compateLockout != 0)
                    //{
                    //    var variance = new Variance
                    //    {
                    //        PropertyName = "CountryId",
                    //        Old = t.LockOutUntil,
                    //        New = userdata.LockOutUntil
                    //    };
                    //    varianceList.Add(variance);
                    //}
                    



                    foreach (var item in varianceList)
                    {
                        Variance updateList = new Variance();
                        string qry = "UPDATE [User] " + "SET " + item.PropertyName + "= @" + "New" + " WHERE [User].[Id] = @Id";

                       
                            switch (item.PropertyName)
                            {
                                case "Blocked":
                                    updateList.New = (item.New == "Active") ? true : false;
                                    item.New = (item.New == "Active") ? "Yes" : "No";
                                    break;

                                case "CountryId":
                                    updateList.New = t.CountryId;
                                    break;

                                case "Status":
                                    if (item.New.Equals(UserStatus.Activated) && item.Old.Equals(UserStatus.EmailAddressApproved))
                                    {
                                        status = 1;
                                    }
                                    break;
                               
                                default:
                                    updateList.New = item.New;
                                    break;
                            }

                            update = await db.ExecuteAsync(qry, new { updateList.New, t.Id });
                      
                       
                    }


                    if (varianceList!=null)
                        
                        auditLogMessage = DataHelper.CreateMessageString(textParam, t.Username, varianceList);


                    if (t.IsResetpassword)
                    {
                        string qry = "UPDATE [User] " + "SET " + "InvalidPasswordAttempts = 0," + "LockOutUntil = null," + "SessionExpiration = @date" + " WHERE [User].[Id] = @Id";
                        DateTime date = DateTime.Now;
                        await db.ExecuteAsync(qry, new { date, t.Id });

                        auditLogMessage.Append("Admin Has Reset Password");
                    }

                    if (update == 1 && auditLogMessage!=null)
                     {                 
                            MvcApplication.Logger.Info(auditLogMessage);
                     }
           }
             return status;
         }
        catch (Exception exception)
        {
            MvcApplication.Logger.Error(exception.Message, exception);
            throw exception;
        }
    }
        public override Task<int> DeleteByIdAsync(User user)
        {
            try
            {
                return db.ExecuteAsync("UPDATE [User]"
                       + "SET IsDelete =" + "@IsDelete" +
                       " WHERE [User].[Id] = @Id", new { user.Id, user.IsDelete });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdmin
{
    public class DashBoardService : IDashBoardService
    {
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<User>();
       

        public DashBoardViewModel GetDashBoradCounts(IEnumerable<User> user, int ringCount, int structCount, int countryCount)
        {
            
            try
            {
                var activeUserCount = user.Select(x => x.Status = UserStatus.Activated).Count();
                var inActiveUserCount = user.Count() - activeUserCount;

                DashBoardViewModel dashBoardValues = new DashBoardViewModel()
                {
                    ActiveUsers = activeUserCount,
                    Countries = countryCount,
                    Rings = ringCount,
                    Structs = structCount
                };

                return dashBoardValues;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
    }
}
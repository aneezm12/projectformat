﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using System.Configuration;
using WebAdmin.Models;

namespace WebAdmin.Services
{
    public class PasswordAttribute : Attribute
    {

    }

   // [PasswordAttribute]
    public class PasswordRepository :DataRepository<Password>
    {
        
        private readonly string fullImageTableName = Sq1Helper.GetFullTableNameFor<Password>();
        public PasswordRepository(IDatabaseService db): base(db)
        {

        }
     
     
        public override async Task<int> UpdateById(Password t, string textParam)
        {
            int isUpdate = 0;
            try
            { 
             isUpdate = await db.ExecuteAsync(
                "UPDATE [Password] SET Pswd = " + "@value" + " WHERE [Password].[Id] = @id",
                new { value =t.Pswd, id =t.Id });
            return isUpdate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        


    }
}
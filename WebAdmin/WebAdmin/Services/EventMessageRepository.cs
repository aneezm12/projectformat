﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAdmin.Models;

namespace WebAdmin
{
    //public class EventMessageAttribute : Attribute
    //{

    //}
 //   [Message]
    public class EventMessageRepository : DataRepository<EventMessage>
    {
        private readonly string fullMessageTableName = Sq1Helper.GetFullTableNameFor<EventMessage>();
        //private readonly string MessageTable = Sq1Helper.GetFullTableNameFor<Message>();
        private readonly string MessageTable = Sq1Helper.GetFullTableNameFor<Message>();

        public EventMessageRepository(IDatabaseService db) : base(db)
        {
        }
       

        public override async Task<int> UpdateById(EventMessage t, string textParam)
        {
            var update = 0;
            StringBuilder auditLogMessage = new StringBuilder("");

            try
            {

                int count = await db.ExecuteScalarAsync<int>(
                "SELECT count(*) FROM " + fullMessageTableName + "WHERE [EventMessage].[Id] = @Id",

                new { t.Id });

                if (count != 0)

                {
                    var eventdata = await db.QuerySingleOrDefaultAsync<EventMessage>("SELECT MessageId, StartDate, EventStart, EndDate, EventEnd,TimeZone FROM " 
                        + fullMessageTableName +
                       " WHERE [EventMessage].[Id] = @Id", new { t.MessageId, t.StartDate, t.EventStart,t.EndDate,t.EventEnd,t.Id,t.TimeZone });

                    //var Id = t.Id;
                    //var StartDate = t.StartDate;
                    //var EndDate = t.EndDate;
                    //var EventStart = t.EventStart;
                    //var EventEnd = t.EventEnd;
                    //var MessageId = t.MessageId;
                    //update = await db.ExecuteAsync("UPDATE [EventMessage]"
                    //    + "SET StartDate =" + "@StartDate" + ",EndDate =" + "@EndDate" + ",Message =" + "@Message" + ",EventStart =" + "@EventStart" + ",EventEnd =" + "@EventEnd" + " WHERE [EventMessage].[Id] = @Id", new { t.Id, t.StartDate, t.EndDate, t.PopUpMessage, t.EventStart,t.EventEnd });
                    update = await db.ExecuteAsync("UPDATE [EventMessage]"
                    + "SET StartDate =@inputStartDate" 
                    + ",EndDate =@inputEndDate"  
                    + ",EventStart =@inputEventStart" 
                    + ",EventEnd =@inputEventEnd" 
                    + ",MessageId =@inputMessageId"
                     + ",TimeZone =@inputTimeZone"
                    + " WHERE [EventMessage].[Id] = @inputId", 
                    new { inputStartDate=t.StartDate, inputEndDate=t.EndDate, inputEventStart=t.EventStart,
                        inputEventEnd =t.EventEnd, inputMessageId=t.MessageId,
                        inputTimeZone = t.TimeZone,
                        inputId =t.Id });

                    //code below to record changes to AuditLog
                    var varianceList = eventdata.Compare(t);

                    if (eventdata.MessageId != t.MessageId)
                    {
                        var updatedmssg = await db.QueryFirstAsync<Message>("SELECT [Message].MessageTitle from " + MessageTable + " WHERE [Message].[MessageId] = '"
                            + @t.MessageId + "'", new { t.MessageId });
                        var oldlang = await db.QueryFirstAsync<Message>("SELECT [Message].MessageTitle from " + MessageTable + " WHERE [Message].[MessageId] = '"
                            + @eventdata.MessageId + "'", new { eventdata.MessageId });

                        var v = new Variance
                        {
                            PropertyName = "MessageTitle",
                            Old = oldlang.MessageTitle,
                            New = updatedmssg.MessageTitle
                        };
                        varianceList.Add(v);
                    }

                    if (varianceList != null)
                        auditLogMessage = DataHelper.CreateMessageString(textParam, "Event", varianceList);

                    if (update == 1 && auditLogMessage != null)
                    {
                        MvcApplication.Logger.Info(auditLogMessage);
                    }

                }

                return update;
            }
            catch (Exception ex)
            {
                MvcApplication.Logger.Error(ex.Message, ex);
                throw ex;
            }
        }

        public override Task<int> DeleteByIdAsync(EventMessage data)
        {
            try
            {
                return db.ExecuteAsync("DELETE FROM " + fullMessageTableName + " WHERE [EventMessage].[Id]  = @Id",
                    new { Id = data.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}